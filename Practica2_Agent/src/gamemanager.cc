/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica3_Movimiento
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "gamemanager.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "soldier.h"



/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

Game& Game::instance() {
  static Game* singleton = new Game();
  return *singleton;
}

Game::Game() {
  end_program = false;
  time_step = 16.6;
  window_width = 800;
  window_height = 600;
}




Game::~Game() {

}





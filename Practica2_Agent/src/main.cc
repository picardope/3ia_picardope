#include "ESAT/window.h"
#include "ESAT/input.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "EDK3/scoped_ptr.h"
#include "agent.h"
#include "soldier.h"
#include "gamemanager.h"
#include <string>


/**
* @brief Render a text with the life of the victim.
* @param victim Victim to render its life.
* @param victim_num Victim id.
*/
void drawVictimLifeText(Victim& victim, int victim_num) {
  ESAT::DrawSetStrokeColor(255,0,0,255);
  ESAT::DrawSetFillColor(255,0,0,255);
  std::string life = std::to_string(victim.life);
  ESAT::DrawText(victim.position.x - 20, victim.position.y - 30, life.data());
  life = std::to_string(victim_num + 1);
  ESAT::DrawSetStrokeColor(255, 255, 255, 255);
  ESAT::DrawSetFillColor(255, 255, 255, 255);
  ESAT::DrawText(victim.position.x - 50, victim.position.y, life.data());
}

/**
* @brief Render a square.
* @param position Vector where the center of the square will be rendered.
* @param r Red color component of the square.
* @param g Green color component of the square.
* @param b Blue color component of the square.
* @param size Side length of the square.
*/
void DrawSquare(Vec2 position, unsigned char r, unsigned char g, unsigned char b, float size = 20.0f) {
  float vertices[10] = { position.x - size, position.y - size,     
                         position.x - size, position.y + size,     
                         position.x + size, position.y + size,     
                         position.x + size, position.y - size,     
                         position.x - size, position.y - size };    
  ESAT::DrawSetStrokeColor(255, 255, 255, 255);   
  ESAT::DrawSetFillColor(r, g, b, 255);   
  ESAT::DrawSolidPath(vertices, 5, true);
}

/// @brief Input function of the application.
void Input() {
  auto& game = Game::instance();
  game.end_program = !ESAT::WindowIsOpened() || 
                  ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
  if (ESAT::IsSpecialKeyDown(ESAT::SpecialKey::kSpecialKey_Keypad_1)) {
    game.victim_list[0].life -= 1;
  }
  if (ESAT::IsSpecialKeyDown(ESAT::SpecialKey::kSpecialKey_Keypad_2)) {
    game.victim_list[1].life -= 1;
  }
  if (ESAT::IsSpecialKeyDown(ESAT::SpecialKey::kSpecialKey_Keypad_3)) {
    game.victim_list[2].life -= 1;
  }
}

/**
* @brief Update function of the application.
* @param delta_time Time step.
*/
void Update(double delta_time) {
  auto& game = Game::instance();
  printf("\n %d , %d , %d ", game.victim_list[0].life, game.victim_list[1].life, game.victim_list[2].life);
  
  game.soldier.update(delta_time);
}

/// @brief Render function of the application.
void Draw() {
  auto& game = Game::instance();

  ESAT::DrawBegin();
  ESAT::DrawClear(60, 60, 60);

  for (unsigned int i = 0; i < game.victim_list.size(); i++) {
    DrawSquare(game.victim_list[i].position, 255, 134, 134);
    drawVictimLifeText(game.victim_list[i], i);
  }
  DrawSquare(game.soldier.position_, 0, 200, 0);

  ESAT::DrawSetStrokeColor(255, 255, 255, 255);
  ESAT::DrawSetFillColor(255, 255, 255, 255);
  ESAT::DrawText(20, game.window_height - 30, "Input: teclado numerico del 1 al 3");

  ESAT::DrawSetStrokeColor(255, 134, 134, 255);
  ESAT::DrawSetFillColor(255, 134, 134, 255);
  ESAT::DrawText(20, 35, "ENFERMOS");

  ESAT::DrawSetStrokeColor(0, 200, 0, 255);
  ESAT::DrawSetFillColor(0, 200, 0, 255);
  ESAT::DrawText(20, game.soldier.position_.y + 10, "DOCTOR");




  ESAT::DrawEnd();
  ESAT::WindowFrame();
}

/// @brief Init function of the application.
void Init() {
  auto& game = Game::instance();
  game.window_height = 800;
  game.window_width = 600;
  ESAT::WindowInit(game.window_width, game.window_height);
  ESAT::WindowSetMouseVisibility(true);
  ESAT::DrawSetTextFont("../data/fonts/DigitFont.TTF");
  ESAT::DrawSetTextSize(30);
  ESAT::DrawSetTextBlur(true);

  game.victim_list.resize(3);
  game.victim_list[0] = { 100, 100, 100.0f , 100.0f };
  game.victim_list[1] = { 100, 100, game.window_width - 100.0f , 100.0f };
  game.victim_list[2] = { 100, 100, 100.0f , game.window_height - 100.0f };
  game.soldier.alive_ = true;
  game.soldier.base_ = { (float)game.window_width / 2, (float)game.window_height / 2 };
  game.soldier.position_ = game.soldier.base_;
  game.soldier.health_ = 100;
  game.soldier.id_ = 1;
  game.soldier.state_ = kState_Thinking;
  game.soldier.velocity_ = 0.1f;
  game.soldier.victim_ = nullptr;
}

/// @brief Start function of the application.
void Start() {

  auto& game = Game::instance();

  double current_time = ESAT::Time(); 

  while (!game.end_program) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= game.time_step) {

      Update(game.time_step);

      current_time += game.time_step;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }


}

/// @brief Finish function of the application.
void Finish() {
  ESAT::WindowDestroy();
}

/// @brief Main function of the application.
int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

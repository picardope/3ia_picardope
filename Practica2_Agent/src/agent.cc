/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica2_Agent
*  @brief IA agent class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "agent.h"



float Vec2Magnitude(const Vec2& vector) {

  float magnitude;

  magnitude = pow(vector.x, 2) + pow(vector.y, 2);
  magnitude = sqrtf(magnitude);

  return magnitude;
}



Vec2 Vec2Normalize(const Vec2& vector) {

  Vec2 normalized;
  float inverse_magnitude = 1.0f / Vec2Magnitude(vector);

  normalized = { vector.x * inverse_magnitude, vector.y * inverse_magnitude };

  return normalized;
}


Agent::Agent() {
  id_ = 0;
  alive_ = false;
  velocity_ = 0.0f;
  speed_ = { 0.0f, 0.0f };
  position_ = { 0.0f, 0.0f };
}

Agent::~Agent() {}
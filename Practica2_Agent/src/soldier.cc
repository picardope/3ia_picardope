/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica2_Agent
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "soldier.h"
#include "gamemanager.h"

Soldier::Soldier() : Agent() {
  state_ = kState_Thinking;
  health_ = 0;
  base_ = { 0.0f, 0.0f };
  destiny_ = { 0.0f, 0.0f };
  victim_ = nullptr;
  delta_ = 0.0;
  velocity_ = 0.0f;
}

Soldier::~Soldier() {

}

void Soldier::update(const double delta_time) {
  delta_ = delta_time;
  mindUpdate();
  bodyUpdate();
}

void Soldier::mindUpdate() {
  auto& game = Game::instance();

  switch (state_) {
  case kState_Thinking: {
    movement_ = kMovementType_None;
    // comprueba si hay alguna victima que necesite ayuda
    victim_ = nullptr;
    for (unsigned int i = 0; i < game.victim_list.size(); i++) {
      if (game.victim_list[i].life < game.victim_list[i].max_life) {
    // en caso de haber varios, se va a por el que menos vida tiene. Asi no es respuesta
    // directa, el soldado tiene autonomia de curar a quien cree que lo necesita mas
        if (!victim_) {
          victim_ = &game.victim_list[i];
        }
        else if (victim_->life > game.victim_list[i].life) {
          victim_ = &game.victim_list[i];
        }
      }
    }
    if (victim_) {
      state_ = kState_GoingToDestiny;
      // establecemos el vector director hacia el que se movera.
      destiny_ = victim_->position;
      lookAt(destiny_);
    }
  }break;
  case kState_GoingToDestiny: {
    movement_ = kMovementType_Forward;
    // Chequeamos si ha llegado al destino.
    if (isNextToDestiny(destiny_)) {
      if (destiny_.x == base_.x && destiny_.y == base_.y) {
        state_ = kState_Thinking;
      }
      else {
        state_ = kState_HealingVictim;
      }
    }
  }break;
  case kState_HealingVictim: {
    movement_ = kMovementType_None;
    if (victim_->life >= victim_->max_life) {
      // una vez curado, volvemos a la base a por medicinas.
      state_ = kState_GoingToDestiny;
      destiny_ = base_;
      lookAt(destiny_);
    }
    healVictim();
  }break;
  }
  
}

void Soldier::bodyUpdate() {
  if (movement_ == kMovementType_Forward) {
    moveForward();
  }
}


void Soldier::moveForward() {
  position_ = { position_.x + speed_.x * ((float)delta_ * 0.001f) * velocity_,
                position_.y + speed_.y * ((float)delta_ * 0.001f) * velocity_ };
}
void Soldier::lookAt(Vec2 destiny) {
  speed_ = { destiny.x - position_.x,destiny.y - position_.y };
  speed_ = Vec2Normalize(speed_);
}

void Soldier::healVictim() {
  victim_->life += 1;
  if (victim_->life >= victim_->max_life) {
    victim_->life = victim_->max_life;
  }
}

bool Soldier::isNextToDestiny(Vec2 destiny) {
  if (position_.x < destiny.x + 3.0f &&
      position_.x > destiny.x - 3.0f &&
      position_.y < destiny.y + 3.0f &&
      position_.y > destiny.y - 3.0f) {
    return true;
  }
  return false;
}
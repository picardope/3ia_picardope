/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica2_Agent
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_SOLDIER_H__
#define __IA_SOLDIER_H__ 1

#include "agent.h"

enum State {
  kState_Thinking,
  kState_GoingToDestiny,
  kState_HealingVictim,
};

enum MovementType {
  kMovementType_None,
  kMovementType_Forward
};

class Soldier : public Agent {
public:

  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  Soldier();
  ~Soldier();

  /*******************************************************************************
  ***                            Soldier methods                               ***
  *******************************************************************************/

  virtual void bodyUpdate() override;
  virtual void mindUpdate() override;
  virtual void update(const double delta_time) override;

  /// @brief Moves the agent.
  void moveForward();

  /**
  * @brief Modifies the agent velocity vector.
  * @param destiny Vector director of the velocity.
  */
  void lookAt(Vec2 destiny);
  /// @brief Heals a victim.
  void healVictim();

  /**
  * @brief Checks the distance between the agent and the destiny point.
  * @param destiny Vector to calculate its distance.
  * @returns True if the destiny is very near.
  */
  bool isNextToDestiny(Vec2 destiny);


  State state_;
  int health_;
  Vec2 base_;
  Vec2 destiny_;
  Victim* victim_;
  double delta_;
  MovementType movement_;



  /*******************************************************************************
  ***                         Private Copy Constructor                         ***
  *******************************************************************************/
private:
  Soldier(const Soldier& copy);
  Soldier& operator=(const Soldier& copy);

};


#endif
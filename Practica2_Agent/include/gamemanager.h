/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica3_Movimiento
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__ 1

#include "soldier.h"

const unsigned short int kNumPathNodes = 4;


/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

class Game {

public:

  /// @brief Singleton instance.
  static Game& instance();

  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  /// @brief Constructor of the class.
  Game();
  /// @brief Destructor of the class.
  ~Game();

  /*******************************************************************************
  ***                              Attributes                                  ***
  *******************************************************************************/

  // INPUT
  bool end_program;

  double time_step;
  int window_width;
  int window_height;
  Vec2 path_node[kNumPathNodes];
  std::vector<Victim> victim_list;
  Soldier soldier;

   


/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
 private:

  Game(const Game& copy);
  Game& operator=(const Game& copy);

};


#endif

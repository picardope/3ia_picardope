/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica2_Agent
*  @brief IA agent class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_AGENT_H__
#define __IA_AGENT_H__ 1

#include <vector>

struct Vec2 {
  float x, y;
};

/**
* @brief Calculates de magnitude of a vector.
* @param vector Vector to calculate its magnitude.
* @returns the magnitude of the vector.
*/
float Vec2Magnitude(const Vec2& vector);

/**
* @brief Normalizes the magintude of a vector.
* @param vector Vector to calculate its normalized magnitude.
* @returns the vector normalized.
*/
Vec2 Vec2Normalize(const Vec2& vector);

struct Victim {
  int life;
  int max_life;
  Vec2 position;
};


class Agent {
 public:

/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/
  /// @brief Constructor of the class.
  Agent();
  /// @brief Destructor of the class.
  ~Agent();

/*******************************************************************************
***                              Agent methods                               ***
*******************************************************************************/

  /// @brief Update function of the body.
  virtual void bodyUpdate() = 0;
  /// @brief Update function of the mind.
  virtual void mindUpdate() = 0;
  /// @brief Update function of the agent.
  virtual void update(const double delta_time) = 0;


  int id_;
  Vec2 speed_;
  Vec2 position_;
  float velocity_;
  bool alive_;



/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
 private:
  Agent(const Agent& copy);
  Agent& operator=(const Agent& copy);

};


#endif

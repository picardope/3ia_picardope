#include "ESAT/window.h"
#include "ESAT/input.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "gamemanager.h"
#include "path_finder.h"
#include <string>
#include <time.h>


/**
* @brief Render a text.
* @param text Text to render.
* @param position Vector where the text will be rendered.
* @param size Size of the text.
* @param r Red color component of the text.
* @param g Green color component of the text.
* @param b Blue color component of the text.
*/
void DrawText(const char* text, JMATH::Vec2 position, float size = 30, unsigned char r = 0, unsigned char g = 0, unsigned char b = 0) {
  ESAT::DrawSetStrokeColor(r, g, b, 255);
  ESAT::DrawSetFillColor(r, g, b, 255);
  ESAT::DrawSetTextSize(size);
  ESAT::DrawSetTextBlur(0.0f);
  ESAT::DrawText(position.x, position.y, text);
}


/// @brief Input function of the application.
void Input() {
  Game::instance().end_program = !ESAT::WindowIsOpened() || 
                                 ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
}

/// @brief Update function of the application.
void Update(double delta_time) {
  auto& game = Game::instance();

  if (ESAT::MouseButtonDown(0)) {
    int index = game.path_finder_.getMapVectorIndexByPosition({ (float)ESAT::MousePositionX(),
                                                                (float)ESAT::MousePositionY() });
    game.path_finder_.cost_map_grid_[index].info = kCellInfo_Checked;
  }
}

/// @brief Render of the scene.
void DrawScene() {
  auto& game = Game::instance();
  game.background_image_.draw();
  game.path_finder_.render();
}


/// @brief Render function of the application.
void Draw() {
  auto& game = Game::instance();
  ESAT::DrawBegin();
  ESAT::DrawClear(60, 60, 60);

  DrawScene();

  ESAT::DrawEnd();
  ESAT::WindowFrame();
}

/// @brief Init function of the application.
void Init() {
  auto& game = Game::instance();

  game.window_width = 1200;
  game.window_height = 800;
  ESAT::WindowInit(game.window_width, game.window_height);
  ESAT::WindowSetMouseVisibility(true);
  ESAT::DrawSetTextFont("../data/fonts/DigitFont.TTF");
  //ESAT::DrawSetTextBlur(true);
  /* TESTEO CON MAPA DE COSTES POR TEXTO.
  game.path_finder_.obtainMapInfo("./../data/map_12_cost.txt");
  game.background_image_.init("./../data/map_12_terrain.bmp");
  */
  /* TESTEOS CON MAPA DE COSTES POR IMAGEN.
  game.path_finder_.obtainMapInfo("./../data/map_03_60x44_bw.bmp");
  game.background_image_.init("./../data/map_03_60x44_bw.bmp");
  game.path_finder_.obtainMapInfo("./../data/map_03_960x704_bw.bmp");
  */
  game.background_image_.init("./../data/map_03_960x704_layoutAB.bmp");
  game.path_finder_.obtainMapInfo("./../data/map_03_120x88_bw.bmp");
  game.background_image_.init();
  game.background_image_.set_scale((float)game.window_width / (float)game.background_image_.width(),
                                   (float)game.window_height / (float)game.background_image_.height());
}

/// @brief Start function of the application.
void Start() {
  auto& game = Game::instance();
  srand(time(NULL));
  game.time_step = 16.6;

  double current_time = ESAT::Time(); 

  while (!game.end_program) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= game.time_step) {

      Update(game.time_step);

      current_time += game.time_step;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }

}

/// @brief Finish function of the application.
void Finish() {
  ESAT::WindowDestroy();
}

/// @brief Main function of the application.
int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

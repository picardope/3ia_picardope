/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "gamemanager.h"
#include "jmath.h"
#include <stdio.h>
#include <stdlib.h>
#include "ESAT/draw.h"



void DrawSquare(JMATH::Vec2 position, 
                unsigned char r, 
                unsigned char g, 
                unsigned char b, 
                unsigned char a,
                float size) {
  float half_size = size * 0.5f;
  float vertices[10] = { position.x - half_size, position.y - half_size,
                         position.x - half_size, position.y + half_size,
                         position.x + half_size, position.y + half_size,
                         position.x + half_size, position.y - half_size,
                         position.x - half_size, position.y - half_size };
  ESAT::DrawSetStrokeColor(0, 0, 0, 255);
  ESAT::DrawSetFillColor(r, g, b, a);
  ESAT::DrawSolidPath(vertices, 5, true);
}

void DrawRect(JMATH::Vec2 position, 
              unsigned char r, 
              unsigned char g, 
              unsigned char b,
              unsigned char a, 
              JMATH::Vec2 size) {
  JMATH::Vec2 half_size = { size.x * 0.5f, size.y * 0.5f };
  float vertices[10] = { position.x - half_size.x, position.y - half_size.y,
                         position.x - half_size.x, position.y + half_size.y,
                         position.x + half_size.x, position.y + half_size.y,
                         position.x + half_size.x, position.y - half_size.y,
                         position.x - half_size.x, position.y - half_size.y };
  ESAT::DrawSetStrokeColor(255, 255, 255, 255);
  ESAT::DrawSetFillColor(r, g, b, a);
  ESAT::DrawSolidPath(vertices, 5, true);
}

/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

Game& Game::instance() {
  static Game* singleton = new Game();
  return *singleton;
}

Game::Game() {
  end_program = false;
  time_step = 16.6;
  window_width = 800;
  window_height = 600;
}

Game::~Game() {

}





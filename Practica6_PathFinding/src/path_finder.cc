/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "path_finder.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "gamemanager.h"


/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

PathFinder::PathFinder() {
  grid_size_ = { 0, 0 };
  cell_size_ = { 0.0f, 0.0f };
}


PathFinder::~PathFinder() {
  cost_map_grid_.clear();
}


void PathFinder::obtainMapInfo(const char* cost_map_filename) {

  if (strlen(cost_map_filename) <= 5) {
    printf(" ERROR: Filename \"%s\" is too short.\n", cost_map_filename);
    return;
  }

  const char* file_extension = cost_map_filename + strlen(cost_map_filename) - 4;

  if (!strncmp(file_extension, ".txt", 4)) {
    ParseText(cost_map_filename);
  }
  else if (!strncmp(file_extension, ".png", 4) ||
           !strncmp(file_extension, ".bmp", 4)) {
    ParseImage(cost_map_filename);
  }
  else {
    printf(" ERROR: File \"%s\" extension .png or .txt not found.\n", cost_map_filename);
  }
}

void PathFinder::ParseText(const char* filename) {

  auto& game = Game::instance();
  std::ifstream file(filename);

  if (file.is_open()) {

    grid_size_ = { 0, 0 };
    cell_size_ = { 0.0f, 0.0f };

    std::string text((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    int map_text_length = 0;
    for (unsigned int i = 0; i < text.length(); i++) {
      printf("%c", text.c_str()[i]);
      if (text.c_str()[i] == '*' || text.c_str()[i] == ' ') {
        map_text_length += 1;
      }
      else {
        grid_size_.y += 1;
      }
    }

    grid_size_.x = map_text_length / grid_size_.y;
    printf(" grid size = %d x %d", grid_size_.x, grid_size_.y);
    cell_size_ = { (float)game.window_width / (float)grid_size_.x,
                   (float)game.window_height / (float)grid_size_.y };

    cost_map_grid_.resize(map_text_length);
    JMATH::Vec2 initial_position = { cell_size_.x * 0.5f, cell_size_.y * 0.5f };

    int vector_index = 0;
    for (unsigned int i = 0; i < text.length(); i++) { 
      if (text.c_str()[i] == '*') {
        cost_map_grid_[vector_index].info = kCellInfo_NonWalkable;
        vector_index += 1;
      }
      else if (text.c_str()[i] == ' ') {
        cost_map_grid_[vector_index].info = kCellInfo_Walkable;
        vector_index += 1;
      }
      cost_map_grid_[i].position = { initial_position.x + cell_size_.x * (i % grid_size_.x),
                                     initial_position.y + cell_size_.y * (i / grid_size_.x) };
    }

    file.close();
  }
  else { printf(" ERROR: File \"%s\" doesnt exists.\n", filename); }

}

void PathFinder::ParseImage(const char* filename) {

  auto& game = Game::instance();
  Sprite cost_map_image;
  unsigned char color[4];

  cost_map_image.init(filename);
  grid_size_ = { cost_map_image.width(), cost_map_image.height() };
  cost_map_grid_.resize(grid_size_.x * grid_size_.y);

  printf(" grid size = %d x %d", grid_size_.x, grid_size_.y);
  cell_size_ = { (float)game.window_width / (float)grid_size_.x,
                  (float)game.window_height / (float)grid_size_.y };

  int index = 0;
  JMATH::Vec2 initial_position = { cell_size_.x * 0.5f, cell_size_.y * 0.5f };
  for (int i = 0; i < grid_size_.y; i++) {
    for (int j = 0; j < grid_size_.x; j++) {
      ESAT::SpriteGetPixel(cost_map_image.handle(), j, i, color);
      if (color[0] < 128) {
        cost_map_grid_[index].info = kCellInfo_NonWalkable;
      }
      else {
        cost_map_grid_[index].info = kCellInfo_Walkable;
      }
      cost_map_grid_[index].position = { initial_position.x + cell_size_.x * (index % grid_size_.x),
                                         initial_position.y + cell_size_.y * (index / grid_size_.x) };
      index++;
    }
  }

}

void PathFinder::calculate() {

}

void PathFinder::render() {

  auto& game = Game::instance();

  for (int i = 0; i < grid_size_.y * grid_size_.x; i++) {

    switch (cost_map_grid_[i].info) {
      case kCellInfo_None: {
        DrawRect(cost_map_grid_[i].position, 0, 0, 0, 50, { cell_size_.x, cell_size_.y });
      } break;
      case kCellInfo_Start: {
        DrawRect(cost_map_grid_[i].position, 0, 0, 0, 50, { cell_size_.x, cell_size_.y });
      } break;
      case kCellInfo_Destiny: {
        DrawRect(cost_map_grid_[i].position, 0, 0, 0, 50, { cell_size_.x, cell_size_.y });
      } break;
      case kCellInfo_NonWalkable: {
        DrawRect(cost_map_grid_[i].position, 0, 0, 0, 255, { cell_size_.x, cell_size_.y });
      } break;
      case kCellInfo_Walkable: {
        DrawRect(cost_map_grid_[i].position, 0, 255, 0, 5, { cell_size_.x, cell_size_.y });
      } break;
      case kCellInfo_Checked: {
        DrawRect(cost_map_grid_[i].position, 0, 0, 0, 50, { cell_size_.x, cell_size_.y });
      } break;
      case kCellInfo_Path: {
        DrawRect(cost_map_grid_[i].position, 0, 0, 0, 50, { cell_size_.x, cell_size_.y });
      } break;
    }

  }
}

JMATH::Vec2i PathFinder::getIndicesByPosition(const JMATH::Vec2 position) {
  return { (int)(position.x / cell_size_.x) , (int)(position.y / cell_size_.y) };
}

int PathFinder::getMapVectorIndexByPosition(const JMATH::Vec2 position) {
  JMATH::Vec2i indices = getIndicesByPosition(position);
  return (indices.x + grid_size_.x * indices.y);
}

JMATH::Vec2 PathFinder::getCellCenterPositionByIndex(const JMATH::Vec2i index) {
  return { index.x * cell_size_.x + cell_size_.x * 0.5f,
           index.y * cell_size_.y + cell_size_.y * 0.5f, };
}



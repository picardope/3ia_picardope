/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __PATHFINDER_H__
#define __PATHFINDER_H__ 1

#include "jmath.h"
#include <vector>

enum CellInfo {
  kCellInfo_None = 0,
  kCellInfo_Start,
  kCellInfo_Destiny, 
  kCellInfo_NonWalkable,
  kCellInfo_Walkable,
  kCellInfo_Checked,
  kCellInfo_Path,
};

struct GridCell {
  CellInfo info;
  int f, g, h;
  JMATH::Vec2 position;
};
 
class PathFinder {
public:

/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/
  PathFinder();
  ~PathFinder();

  /**
  * @brief Gets a text or a image file and saves its info into the class map_grid_.
  * @param cost_map_filename filename to parse.
  */
  void obtainMapInfo(const char* cost_map_filename);

  /**
  * @brief Gets a text file and saves its info into the class map_grid_.
  * @param filename Text file to parse.
  */
  void ParseText(const char* filename);

  /**
  * @brief Gets a image file and saves its info into the class map_grid_.
  * @param filename Image file to parse.
  */
  void ParseImage(const char* filename);

  /// @brief Calculates the pathfinding using the AStar algorythm
  void calculate();

  /// @brief Renders the map in the window.
  void render();

  std::vector<GridCell> cost_map_grid_;
  JMATH::Vec2i grid_size_;
  JMATH::Vec2 cell_size_;

  /**
  * @brief Gets the indices in rows and cols from a position in the map.
  * This function returns indices in { x, y } axis, not like rows and cols.
  * 
  * @param position Vector where to calculate its index.
  * @return Vec2i with the index in x and y axis.
  */
  JMATH::Vec2i getIndicesByPosition(const JMATH::Vec2 position);

  /**
  * @brief Gets an index from a position in the map.
  * @param position Vector where to calculate its index.
  * @return int with the index of the map_grid_ vector.
  */
  int getMapVectorIndexByPosition(const JMATH::Vec2 position);

  /**
  * @brief Gets the center position of a cell.
  * @param index Index where to calculate it's cell center position.
  * @return Vec2 with the center position of the cell.
  */
  JMATH::Vec2 getCellCenterPositionByIndex(const JMATH::Vec2i index);


/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
private:
  PathFinder(const PathFinder& copy);
  PathFinder& operator=(const PathFinder& copy);

};


#endif
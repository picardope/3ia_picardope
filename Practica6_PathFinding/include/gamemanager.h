/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__ 1

#include "path_finder.h"
#include "sprite.h"

/**
* @brief Render a square.
* @param position Vector where the center of the square will be rendered.
* @param r Red color component of the square.
* @param g Green color component of the square.
* @param b Blue color component of the square.
* @param size Side length of the square.
*/
void DrawSquare(JMATH::Vec2 position, 
                unsigned char r, 
                unsigned char g, 
                unsigned char b,
                unsigned char a,
                float size = 20.0f);

/**
* @brief Render a rectangle.
* @param position Vector where the center of the rectangle will be rendered.
* @param r Red color component of the rectangle.
* @param g Green color component of the rectangle.
* @param b Blue color component of the rectangle.
* @param size Vector with the width and height of the rectangle.
*/
void DrawRect(JMATH::Vec2 position, 
              unsigned char r, 
              unsigned char g, 
              unsigned char b,
              unsigned char a,
              JMATH::Vec2 size = { 20.0f, 20.0f });

/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

class Game {

public:

  /// @brief Singleton instance.
  static Game& instance();

  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  /// @brief Constructor of the class.
  Game();
  /// @brief Destructor of the class.
  ~Game();

  // INPUT
  bool end_program;

  double time_step;
  int window_width;
  int window_height;

  PathFinder path_finder_;
  Sprite background_image_;

/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
 private:

  Game(const Game& copy);
  Game& operator=(const Game& copy);

};


#endif

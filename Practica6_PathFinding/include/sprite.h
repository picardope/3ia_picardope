/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "ESAT/sprite.h"

#ifndef __Sprite_H__
#define __Sprite_H__ 1

  /****************************************
  ***          ENUMS AND CONSTS         ***
  ****************************************/
  /// Useful predefined origin points.
  static enum Origin{
    kOrigin_UpperLeft = 0,
    kOrigin_UpperRight,
    kOrigin_LowerLeft,
    kOrigin_LowerRight,
    kOrigin_Center,
    kOrigin_Up,
    kOrigin_Down,
    kOrigin_Left,
    kOrigin_Right
  };

  /**
  * @brief Sprite class. Built by a handle and a transform struct.
  */
class Sprite{

 public:

  /****************************************
  ***    CONSTRUCTORS AND DESTRUCTOR    ***
  ****************************************/
  /**
  * @brief Default constructor function of the class.
  */
  Sprite();
  /**
  * @brief Constructor copies data from another object of the same class.
  */
  Sprite(const Sprite& copy);
  /**
  * @brief Destructor function of the class.
  */
  ~Sprite();

  /**
  * @brief Init function of the class.
  *
  * Init by filenames.
  *
  * @param posX X coordinate of the sprite position.
  * @param posY Y coordinate of the sprite position.
  * @param scaleX Scale value in X axis.
  * @param scaleY Scale value in Y axis.
  * @param angle Angle value.
  */
  void init(const char* filename,
            Origin origin = kOrigin_UpperLeft,
            const float posX = 0.0f,
            const float posY = 0.0f,
            const float scaleX = 1.0f,
            const float scaleY = 1.0f,
            const float angle = 0.0f);

  /**
  * @brief Init function of the class.
  *
  * Init by handles.
  *
  * @param posX X coordinate of the sprite position.
  * @param posY Y coordinate of the sprite position.
  * @param scaleX Scale value in X axis.
  * @param scaleY Scale value in Y axis.
  * @param angle Angle value.
  */
  void init(ESAT::SpriteHandle handle,
            Origin origin = kOrigin_UpperLeft,
            const float posX = 0.0f,
            const float posY = 0.0f,
            const float scaleX = 1.0f,
            const float scaleY = 1.0f,
            const float angle = 0.0f);
  /**
  * @brief Init function of the class.
  *
  * Used when the sprite is already initialized.
  *
  * @param posX X coordinate of the sprite position.
  * @param posY Y coordinate of the sprite position.
  * @param scaleX Scale value in X axis.
  * @param scaleY Scale value in Y axis.
  * @param angle Angle value.
  */
  void init(Origin origin = kOrigin_UpperLeft,
            const float posX = 0.0f,
            const float posY = 0.0f,
            const float scaleX = 1.0f,
            const float scaleY = 1.0f,
            const float angle = 0.0f);

  /****************************************
  ***        SETTERS AND GETTERS        ***
  ****************************************/
  ESAT::SpriteHandle handle();
  ESAT::SpriteTransform transform();
  int width();
  int height();
  void set_origin(Origin origin);
  void set_position(const float x, const float y);
  void set_origin(const float x, const float y);
  void set_scale(const float x, const float y);
  ///add angle to the current rotation.
  void rotate(const float angle);

  /****************************************
  ***             SPRITE DRAW           ***
  ****************************************/
  /**
  * @brief Default draw function of the class.
  */
  void draw();
  /**
  * @brief Draws in a concrete position of the screen.
  *
  * @param posY Y coordinate.
  * @param posX X coordinate.
  */
  void draw(const float posX, const float posY);

  /****************************************
  ***          PUBLIC ATRIBUTES         ***
  ****************************************/

  ESAT::SpriteHandle handle_;
  ESAT::SpriteTransform transform_;
  bool init_from_file_;

};


#endif

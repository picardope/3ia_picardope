cls
IF EXIST .vs rmdir /s /q .vs
IF EXIST obj rmdir /s /q obj
IF EXIST ..\compiled\debug rmdir /s /q ..\compiled\debug
IF EXIST ..\compiled\release rmdir /s /q ..\compiled\release
IF EXIST PathFinding.vcxproj del /F PathFinding.vcxproj 
IF EXIST PathFinding.vcxproj.filters del /F PathFinding.vcxproj.filters 
IF EXIST PathFinding.vcxproj.user del /F PathFinding.vcxproj.user 
IF EXIST IA_Practica6_PathFinding.sln del /F IA_Practica6_PathFinding.sln 
IF EXIST IA_Practica6_PathFinding.VC.db del /F IA_Practica6_PathFinding.VC.db 

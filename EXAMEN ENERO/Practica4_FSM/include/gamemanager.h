/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica4_FSM
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__ 1

#include "ESAT/sprite.h"
#include <vector>

class Soldier;

struct Vec2 {
  float x, y;
};

/**
* @brief Calculates the magnitude of a vector.
* @param vector Vector to calculate its Magnitude.
* @returns Magnitude of the vector.
*/
float Vec2Magnitude(const Vec2& vector);

/**
* @brief Normalizes the magintude of a vector.
* @param vector Vector to calculate its normalized magnitude.
* @returns the vector normalized.
*/
Vec2 Vec2Normalize(const Vec2& vector);

/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

class Game {

public:


  /// Singleton instance.
  static Game& instance();
  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  Game();
  ~Game();

  /*******************************************************************************
  ***                              Attributes                                  ***
  *******************************************************************************/

  // INPUT
  bool end_program_;

  double time_step_;
  int window_width_;
  int window_height_;

  std::vector<Soldier> soldier_;

  ESAT::SpriteHandle big_sprite_handle_;
  ESAT::SpriteHandle small_sprite_handle_;
  ESAT::SpriteHandle medium_sprite_handle_;
   


/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
 private:

  Game(const Game& copy);
  Game& operator=(const Game& copy);

};


#endif

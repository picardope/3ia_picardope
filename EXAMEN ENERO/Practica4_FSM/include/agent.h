/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica4_FSM
*  @brief IA agent class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_AGENT_H__
#define __IA_AGENT_H__ 1

#include <vector>
#include "ESAT\sprite.h"
#include "gamemanager.h"


class Agent {
 public:

/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/
  /// @brief Constructor of the class.
  Agent();
  /// @brief Destructor of the class.
  ~Agent();

  /// @brief Constructor of copy needed for the vector of soldiers.
  Agent(const Agent& copy);
  /// @brief Operator of copy needed for the vector of soldiers
  Agent& operator=(const Agent& copy);

/*******************************************************************************
***                              Agent methods                               ***
*******************************************************************************/
  /// @brief Update function of the body.
  virtual void bodyUpdate() = 0;
  /// @brief Update function of the mind.
  virtual void mindUpdate() = 0;
  /// @brief Update function of the agent.
  virtual void update(const double delta_time) = 0;


  int id_;
  Vec2 velocity_;
  Vec2 position_;
  float speed_;
  bool alive_;



};


#endif

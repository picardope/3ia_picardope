/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica4_FSM
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_SOLDIER_H__
#define __IA_SOLDIER_H__ 1

#include "agent.h"

enum SoldierState {
  kSoldierState_Init,
  kSoldierState_Working,
  kSoldierState_Chasing,
  kSoldierState_Fleeing,
  kSoldierState_Resting
};

enum SoldierMovement {
  kSoldierMovement_None,
  kSoldierMovement_Determ,
  kSoldierMovement_Random,
  kSoldierMovement_Patron,
  kSoldierMovement_Move,
};

enum SoldierType {
  kSoldierType_Big,
  kSoldierType_Medium,
  kSoldierType_Small
};

enum SoldierPatron {
  kSoldierPatron_Left,
  kSoldierPatron_Up,
  kSoldierPatron_Down,
  kSoldierPatron_Right,
};

const float k2Pi = 6.28318530718f;
const float kD1 = 100.0f;
const float kD2 = 150.0f;
const float kD3 = 200.0f;
const float kRandomMovementTimeToChange = 500.0f;
const float kPatronMovementTimeToChange = 2000.0f;
const float kFleeingSensorTime = 300.0f;
const float kRestingTime = 4000.0f;

class Soldier : public Agent {
public:


  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  Soldier();
  ~Soldier();
  Soldier(const Soldier& copy);
  Soldier& operator=(const Soldier& copy);

  /*******************************************************************************
  ***                            Soldier methods                               ***
  *******************************************************************************/

  virtual void bodyUpdate() override;
  virtual void mindUpdate() override;
  virtual void update(const double delta_time) override;
  /// @brief Render method of the class.
  void draw();
  /// @brief Init method of the class.
  void init();
  /// @brief change to working transition method of the class.
  void changeToWorking();


  /// @brief Mind update applied to the fleeing soldier state.
  void fleeingUpdate();
  /// @brief Mind update applied to the chasing soldier state.
  void chasingUpdate();
  /// @brief Mind update applied to the resting soldier state.
  void restingUpdate();
  /// @brief Mind update applied to the working soldier state.
  void workingUpdate();

  /// @brief Body's Determinist move method.
  void moveDeterm();
  /// @brief Body's Random move method.
  void moveRandom();
  /// @brief Body's Patron move method.
  void movePatron();
  /// @brief Basic movement.
  void move();

  /**
  * @brief Modifies the agent velocity vector.
  * @param destiny Vector director of the velocity.
  */
  void lookAt(Vec2 destiny);

  /**
  * @brief Checks the distance between the agent and the destiny point.
  * @param destiny Vector to calculate its distance.
  * @returns True if the destiny is very near.
  */
  bool isNextToDestiny(Vec2 destiny);

  /**
  * @brief Calculates the distance between the soldier and a point.
  * @param point Point in scene to calculate its distance to the soldier.
  * @returns Distance between soldier and point.
  */
  float getDistanceToPoint(const Vec2 point);
  /// @brief Checks the sreen resolution and changes the position of the solider.
  void checkScreenLimits();
  /// @brief Render a cirle around the soldier, used to make the colissions visible.
  void drawCollisionRadius();


  SoldierType type_;
  SoldierState state_;
  SoldierMovement movement_;
  SoldierPatron patron_;
  Soldier* chase_objetive_;
  int health_;
  Vec2 destiny_;
  Vec2 last_path_position_;
  Vec2 determ_destiny_[2];
  int determ_index_;
  unsigned int last_path_index_;
  double delta_;
  float speed_factor_;
  float collision_radius_;
  int frame_chronometer_;
  ESAT::SpriteHandle sprite_;
  ESAT::SpriteTransform transform_;
  double movement_timer_;
  double resting_timer_;
  double fleeing_sensor_timer_;



};


#endif
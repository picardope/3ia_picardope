/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __COSTMAP_H__
#define __COSTMAP_H__ 1

#include "jmath.h"
#include <vector>
#include "sprite.h"

const int kDoorAIndices[4] = { 3942, 4062, 4182, 4302 };
const int kDoorBIndices[3] = { 3259, 3260, 3258 };

struct CellInfo {
  CellInfo() {
    position = { 0.0f, 0.0f };
    is_walkable = false;
    f = g = h = 0;
    parent = nullptr;
  }

	JMATH::Vec2 position;
  int f, g, h;
  CellInfo* parent;
	bool is_walkable;
};

class CostMap {
public:

/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/

  CostMap();
  ~CostMap();


  /// In x and y axis
  JMATH::Vec2i grid_size_;
  /// Size in X and Y axis. Size adapted to the screen resolution.
  JMATH::Vec2 cell_size_;
  /// Vector where we will save all the map info.
	std::vector<CellInfo> cost_map_vector_;

  /**
  * @brief Gets a text or a image file and saves its info into the class map_grid_.
  * @param cost_map_filename filename to parse.
  * @return True if success and False if there is any error.
  */
	bool load(const char *cost_map_filename);

  /**
  * @brief Gets a text file and saves its info into the class map_grid_.
  * @param filename Text file to parse.
  * @return True if success and false if there is any error.
  */
  bool parseText(const char* filename);

  /**
  * @brief Gets a image file and saves its info into the class map_grid_.
  * @param filename Image file to parse.
  * @return True if success and false if there is any error.
  */
  bool parseImage(const char* filename);

  // Prints the cells of cost_map.
	void print(); 
  /// @brief Renders the map in the window.
	void draw(); 

  /**
  * @brief Gets the indices in rows and cols from a position in the map.
  * This function returns indices in { x, y } axis, not like rows and cols.
  *
  * @param position Vector where to calculate its index.
  * @return Vec2i with the index in x and y axis.
  */
  JMATH::Vec2i getIndicesByPosition(const JMATH::Vec2 position);

  /**
  * @brief Gets an index from a position in the map.
  * @param position Vector where to calculate its index.
  * @return int with the index of the map_grid_ vector.
  */
  int getMapVectorIndexByPosition(const JMATH::Vec2 position);

  /**
  * @brief Gets the center position of a cell.
  * @param index Index where to calculate it's cell center position.
  * @return Vec2 with the center position of the cell.
  */
  JMATH::Vec2 getCellCenterPositionByIndex(const JMATH::Vec2i index);

  void openDoorA(const bool open);
  void openDoorB(const bool open);

/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
private:

  CostMap(const CostMap& copy);
  CostMap& operator=(const CostMap& copy);

};

#endif
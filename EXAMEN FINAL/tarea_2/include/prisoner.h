/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA prisoner class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_PRISONER_H__
#define __IA_PRISONER_H__ 1

#include "agent.h"

enum PrisonerState {
  kPrisonerState_Init,
  kPrisonerState_Resting,
  kPrisonerState_Working,
  kPrisonerState_RestingToWorking,
  kPrisonerState_WorkingToResting,
  kPrisonerState_Alarm,
  kPrisonerState_Freedom,
  kPrisonerState_End
};

enum PrisonerMovement {
  kPrisonerMovement_None,
  kPrisonerMovement_Determ,
  kPrisonerMovement_Random,
  kPrisonerMovement_Tracker
};


const float kPrisonerNormalSpeed = 50.0f;
const float kSwapBetweenRestingAndWorkingTime = 40000.0f;
// Numero de pixeles que se desviaran para evitar que vayan en fila india.
const int kPrisonerFlockingRadius = 12; 

class Prisoner : public Agent {
public:


/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/
  Prisoner();
  ~Prisoner();
  Prisoner(const Prisoner& copy);
  Prisoner& operator=(const Prisoner& copy);

/*******************************************************************************
***                             Agent methods                                ***
*******************************************************************************/

  virtual void bodyUpdate() override;
  virtual void mindUpdate() override;
  virtual void update(const double delta_time) override;

/*******************************************************************************
***                            Prisoner methods                              ***
*******************************************************************************/
  /// @brief Render method of the class.
  void draw();
  /// @brief Init method of the class.
  void init();

  /// @brief Mind update applied to the resting prisoner state.
  void restingUpdate();
  /// @brief Mind update applied to the working prisoner state.
  void workingUpdate();
  /// @brief Loading package from the working area.
  void loadPackage();
  /// @brief Leaving package from the working area. Also useful for start working.
  void leavePackage();
  /// @brief Mind update applied to the working to resting prisoner state.
  void workingToRestingUpdate();
  /// @brief Mind update applied to the resting to working prisoner state.
  void restingToWorkingUpdate();
  /// @brief Mind update applied to the alarm prisoner state.
  void alarmUpdate();
  /// @brief Mind update applied to the free prisoner state.
  void freedomUpdate();

  /// @brief Body's Determinist move method.
  void moveDeterm();
  /// @brief Body's Random move method.
  void moveRandom();
  /// @brief Body's Tracker move method.
  void moveTracker();

  /// @brief Start resting.
  void startResting();
  /// @brief Start working.
  void startWorking();
  /// @brief Actives the alarm mode.
  void startAlarm();
  /// @brief Deactivates the alarm mode.
  void stopAlarm();

  /// @brief Function to swap between these two states.
  void swapBetweenWorkingAndResting();


  /**
  * @brief Modifies the agent velocity vector.
  * @param destiny Vector director of the velocity.
  */
  void lookAt(JMATH::Vec2 destiny);

  /**
  * @brief Calculates the distance between the soldier and a point.
  * @param point Point in scene to calculate its distance to the soldier.
  * @returns Distance between soldier and point.
  */
  float getDistanceToPoint(const JMATH::Vec2 point);
  
  PrisonerState state_;
  PrisonerMovement movement_;
  int health_;
  JMATH::Vec2 destiny_;
  double delta_;
  float collision_radius_;
  float movement_timer_;
  float random_movement_time_to_change_;
  
  PrisonerState state_before_escaping_;

  // working
  bool has_a_package_;

  // tracking
  int tracking_id_;
  class Path* current_path_;
  bool inverse_path_;

};


#endif
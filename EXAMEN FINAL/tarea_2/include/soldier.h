/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_SOLDIER_H__
#define __IA_SOLDIER_H__ 1

#include "agent.h"

enum SoldierState {
  kSoldierState_Init,
  kSoldierState_SearchingDoors,
  kSoldierState_Alarm,
  kSoldierState_End
};

enum SoldierMovement {
  kSoldierMovement_None,
  kSoldierMovement_Determ,
  kSoldierMovement_Tracker
};


const float kSoldierNormalSpeed = 150.0f;
 

class Soldier : public Agent {
public:


/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/
  Soldier();
  ~Soldier();
  Soldier(const Soldier& copy);
  Soldier& operator=(const Soldier& copy);

/*******************************************************************************
***                             Agent methods                                ***
*******************************************************************************/

  virtual void bodyUpdate() override;
  virtual void mindUpdate() override;
  virtual void update(const double delta_time) override;

/*******************************************************************************
***                            Soldier methods                              ***
*******************************************************************************/
  /// @brief Render method of the class.
  void draw();
  /// @brief Init method of the class.
  void init();


  /// @brief Mind update applied to the searching doors soldier state.
  void searchingDoorsUpdate();
  /// @brief Mind update applied to the alarm soldier state.
  void alarmUpdate();

  /// @brief Body's Determinist move method.
  void moveDeterm();
  /// @brief Body's Tracker move method.
  void moveTracker();

  /// @brief Start alarm.
  void startAlarm();
  /// @brief Deactivates the alarm mode.
  void stopAlarm();


  /**
  * @brief Modifies the agent velocity vector.
  * @param destiny Vector director of the velocity.
  */
  void lookAt(JMATH::Vec2 destiny);

  
  SoldierState state_;
  SoldierMovement movement_;
  int health_;
  JMATH::Vec2 destiny_;
  double delta_;
  float collision_radius_;

  // tracking
  int tracking_id_;
  class Path* current_path_;
  bool inverse_path_;

};


#endif
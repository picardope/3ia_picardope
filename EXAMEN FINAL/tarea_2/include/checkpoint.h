/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA Checkpoint class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __CHECKPOINT_H__ 
#define __CHECKPOINT_H__ 1

#include "jmath.h"
#include <vector>

class CheckPoint {
 public:
  
/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/

  CheckPoint();
  ~CheckPoint();

/*******************************************************************************
***                                  Methods                                 ***
*******************************************************************************/
	
  /**
  * @brief Add a new neighbor to the vector.
  * @param neighbor Neighbor you can go after this.
  */
  void addNeighbor(CheckPoint* neighbor);


  /// @brief Draws squares in all the positions of the route.
	void draw();

  /**
  * @brief Look for a random neighbor in the list.
  * @returns an available checkpoint Nullptr if theres checkpoint available.
  */
  CheckPoint* getRandomNeigbor();

/*******************************************************************************
***                             Public Properties                                 ***
*******************************************************************************/

  /// Saves all the checkpoints where you can go after this.
  std::vector<CheckPoint*> neighbor_list_;
  /// Num neighbors.
  unsigned int num_neighbors_;
  /// Position in the map.
  JMATH::Vec2 position_;


/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
private:

  CheckPoint(const CheckPoint& copy);
  CheckPoint& operator=(const CheckPoint& copy);

};
#endif 
/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA guard class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_GUARD_H__
#define __IA_GUARD_H__ 1

#include "agent.h"
#include "astar.h"
#include "path.h"
#include "checkpoint.h"

enum GuardState {
  kGuardState_Init,
  kGuardState_Normal,
  kGuardState_Suspicious,
  kGuardState_Alert,
  kGuardState_Alarm,
  kGuardState_End
};

enum GuardMovement {
  kGuardMovement_None,
  kGuardMovement_Determ,
  kGuardMovement_Patrol,
  kGuardMovement_Tracker
};


const float kGuardNormalSpeed = 50.0f;
const float kGuardDetectionRadius = 20.0;
const int kGuardFlockingRadius = 1; 

class Guard : public Agent {
public:


/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/
  Guard();
  ~Guard();
  Guard(const Guard& copy);
  Guard& operator=(const Guard& copy);

/*******************************************************************************
***                             Agent methods                                ***
*******************************************************************************/

  virtual void bodyUpdate() override;
  virtual void mindUpdate() override;
  virtual void update(const double delta_time) override;

/*******************************************************************************
***                            Guard methods                              ***
*******************************************************************************/
  /// @brief Render method of the class.
  void draw();
  /// @brief Init method of the class.
  void init();

  /// @brief Mind update applied to the normal guard state.
  void normalUpdate();
  /// @brief Mind update applied to the alert guard state.
  void alertUpdate();
  /// @brief Mind update applied to the suspicious guard state.
  void suspiciousUpdate();
  /// @brief Mind update applied to the alarm guard state.
  void alarmUpdate();

  /// @brief Body's Determinist move method.
  void moveDeterm();
  /// @brief Body's Patrol move method.
  void movePatrol();
  /// @brief Body's Tracker move method.
  void moveTracker();

  /// @brief start suspicing transition method.
  void startSuspicing(class Soldier* soldier);
  /// @brief start alarm transition method.
  void startAlarm();
  /// @brief Deactivates the alarm mode.
  void stopAlarm();


  /**
  * @brief Calculates the distance between the soldier and a point.
  * @param point Point in scene to calculate its distance to the soldier.
  * @returns Distance between soldier and point.
  */
  float getDistanceToPoint(const JMATH::Vec2 point);

  /**
  * @brief Looks for the closer and available agent in scene.
  * @returns Position of this agent.
  */
  JMATH::Vec2 getCloserAgentPosition();

  /**
  * @brief Modifies the agent velocity vector.
  * @param destiny Vector director of the velocity.
  */
  void lookAt(JMATH::Vec2 destiny);

  void drawCollisionRadius();
  
  GuardState state_;
  GuardMovement movement_;
  int health_;
  JMATH::Vec2 destiny_;
  double delta_;
  float collision_radius_;


  // tracking
  int tracking_id_;
  bool inverse_path_;
  Path* current_path_;
  CheckPoint* current_checkpoint_;
  Path my_own_path_;
  AStar a_star_;

};


#endif
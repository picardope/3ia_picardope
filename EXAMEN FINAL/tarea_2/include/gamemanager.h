/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__ 1

#include "sprite.h"
#include "cost_map.h"
#include "jmath.h"
#include "path.h"
#include "astar.h"
#include "prisoner.h"
#include "soldier.h"
#include "guard.h"
#include "checkpoint.h"

const int kNumPrisoners = 20;
const int kNumGuards = 10;
const int kNumSoldiers = 20;
const int kNumCheckPoints = 28;
const float kAlarmDeactivateTime = 20000.0f;

/**
* @brief Render a square.
* @param position Vector where the center of the square will be rendered.
* @param r Red color component of the square.
* @param g Green color component of the square.
* @param b Blue color component of the square.
* @param size Side length of the square.
*/
void DrawSquare(JMATH::Vec2 position, 
                unsigned char r, 
                unsigned char g, 
                unsigned char b,
                unsigned char a,
                float size = 20.0f);

/**
* @brief Render a rectangle.
* @param position Vector where the center of the rectangle will be rendered.
* @param r Red color component of the rectangle.
* @param g Green color component of the rectangle.
* @param b Blue color component of the rectangle.
* @param size Vector with the width and height of the rectangle.
*/
void DrawRect(JMATH::Vec2 position, 
              unsigned char r, 
              unsigned char g, 
              unsigned char b,
              unsigned char a,
              JMATH::Vec2 size = { 20.0f, 20.0f });

/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

class Game {

public:

  /// @brief Singleton instance.
  static Game& instance();

/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/
  /// @brief Constructor of the class.
  Game();
  /// @brief Destructor of the class.
  ~Game();

/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/

  /// @brief Update timers
  void updateTimers(const double delta_time);

  /// @brief Actives the alarm.
  void activeAlarm();
  /// @brief Deactivates the alarm.
  void deactivateAlarm();

  /// @brief Initializes the soldiers, guards and prisoners.
  void initAgents();
  /// @brief Renders the soldiers, guards and prisoners.
  void drawAgents();
  /// @brief Updates the soldiers, guards and prisoners.
  void updateAgents(const double delta_time);

/********************************************************************************
***                       Areas and precalculated paths                       ***
********************************************************************************/
  /// @brief Defines the limits of the game areas.
  void initAreasLimits();

  JMATH::Vec2 min_resting_area_;
  JMATH::Vec2 max_resting_area_;
  JMATH::Vec2 min_packing_area_;
  JMATH::Vec2 max_packing_area_;
  JMATH::Vec2 min_unpacking_area_;
  JMATH::Vec2 max_unpacking_area_;
  JMATH::Vec2 min_working_area_;
  JMATH::Vec2 max_working_area_;
  JMATH::Vec2 min_base_B_area_;
  JMATH::Vec2 max_base_B_area_;
  float door_A_area_[10];
  float door_B_area_[10];

  /// @brief Defines the precalculated paths.
  void initPaths();

  Path resting_to_working_path_;
  Path working_to_resting_path_;
  Path resting_to_door_A_path_;
  Path door_A_to_door_B_path_;
  Path base_A_to_out_door_A_path_;
  Path out_door_B_to_base_B_path_;
  Path out_door_A_to_out_door_B_;

  /// @brief Defines the precalculated checkpoints.
  void initCheckPoints();
  CheckPoint checkpoint_list_[kNumCheckPoints];
  
  /// @brief Method to open the A door.
  void openDoorA(const bool open);
  /// @brief Method to open the B door.
  void openDoorB(const bool open);


/********************************************************************************
***                                 Game data.                                ***
********************************************************************************/

  // INPUT
  bool end_program;

  double time_step;
  float window_width;
  float window_height;

  int num_prisoners_free_;

  Sprite background_image_;
  CostMap cost_map_;
  AStar astar_;
  Path path_;

  JMATH::Vec2 origin;
  JMATH::Vec2 goal;

  bool origin_set;
  bool goal_set;

  bool is_door_A_opened_;
  bool is_door_B_opened_;
  bool is_alarm_mode_on_;

  bool input_mouse_button_up;
  
  Prisoner prisoner_[kNumPrisoners];
  Soldier soldier_[kNumSoldiers];
  Guard guard_[kNumGuards];

  float prisoners_swap_between_resting_working_timer_;
  float alarm_timer_;

  Sprite alarm_on_image_;
  Sprite alarm_off_image_;
  Sprite feedback_image_;

/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
 private:

  Game(const Game& copy);
  Game& operator=(const Game& copy);

};


#endif

-- GENIe solution to build the project.
-- Julio Marcelo Picardo <picardope@esat-alumni.com>
-- Ivan Sancho <sanchomu@esat-alumni.com>

PROJ_DIR = path.getabsolute("./../")

solution "3IA_Tarea2"
  configurations {
    "Debug",
    "Release"
  }
  platforms { "x32", "x64" }

project "3IA_Tarea2"
  kind "ConsoleApp"
  language "C++"

  includedirs {
  	path.join(PROJ_DIR, "./include/"),
  	path.join(PROJ_DIR, "./src/"),
  	path.join(PROJ_DIR, "./extern/include/"),
  }

  files {
  	path.join(PROJ_DIR, "./include/*.h"),
  	path.join(PROJ_DIR, "./src/*.cc"),
  	path.join(PROJ_DIR, "./extern/include/ESAT_SDK/*.h"),
  }

  configuration "Debug"
    targetdir "../bin/debug/"
    flags { "Symbols" }
    links {
      "opengl32",
      "../extern/lib/ESAT_SDK/ESAT_d",
    }
    defines {
      "_DEBUG"
    }

  configuration "Release"
    targetdir "../bin/release/"
    flags { "OptimizeSize" }
    links {
      "opengl32",
      "../extern/lib/ESAT_SDK/ESAT",
    }
    defines {
      "NDEBUG"
    }

cls
IF EXIST .vs rmdir /s /q .vs
IF EXIST obj rmdir /s /q obj
IF EXIST ..\bin\debug rmdir /s /q ..\bin\debug
IF EXIST ..\bin\release rmdir /s /q ..\bin\release
IF EXIST 3IA_Tarea2.vcxproj del /F 3IA_Tarea2.vcxproj
IF EXIST 3IA_Tarea2.vcxproj.filters del /F 3IA_Tarea2.vcxproj.filters 
IF EXIST 3IA_Tarea2.vcxproj.user del /F 3IA_Tarea2.vcxproj.user 
IF EXIST 3IA_Tarea2.sln del /F 3IA_Tarea2.sln 
IF EXIST 3IA_Tarea2.VC.db del /F 3IA_Tarea2.VC.db 

/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA Guard class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "guard.h"
#include "ESAT_SDK/Time.h"
#include "ESAT_SDK/draw.h"
#include "jmath.h"
#include "gamemanager.h"


/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/


Guard::Guard() : Agent() {
  movement_ = kGuardMovement_None;
  state_ = kGuardState_Init;
  
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = 0.0f;
  delta_ = 0.0;
  collision_radius_ = 0.0f;
  tracking_id_ = 0;
  current_path_ = nullptr;
  current_checkpoint_ = nullptr;
  inverse_path_ = false;
}

Guard::Guard(const Guard& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
}
Guard& Guard::operator=(const Guard& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
  return *this;
}

Guard::~Guard() {}


/*******************************************************************************
***                             AGENT METHODS                                ***
*******************************************************************************/

void Guard::init() {
  auto& game = Game::instance();
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = kGuardNormalSpeed + (float)((rand() % 10));
  velocity_ = { 0.0f, 0.0f };
  movement_ = kGuardMovement_Patrol;
  state_ = kGuardState_Normal;
  position_ = current_checkpoint_->getRandomNeigbor()->position_;
  position_ += JMATH::getFlockingVariationRange(5.0f, 5.0f);
  destiny_ = current_checkpoint_->position_;
  lookAt(destiny_);
}

void Guard::draw() {
  
  DrawSquare(position_, 255,0,0,255, 7.0f);
}

void Guard::update(const double delta_time) {
  delta_ = delta_time;
  mindUpdate();
  bodyUpdate();
}


/*******************************************************************************
***                              MIND UPDATES                                   ***
*******************************************************************************/



void Guard::mindUpdate() {
  switch (state_) {
    case kGuardState_Init:
      init();
    break;
    case kGuardState_Normal:
      normalUpdate();
    break;
    case kGuardState_Suspicious:
      suspiciousUpdate();
    break;
    case kGuardState_Alert:
      alertUpdate();
    break;
    case kGuardState_Alarm:
      alarmUpdate();
    break;
  }
  
}


void Guard::normalUpdate() {

  auto& game = Game::instance();
  
  // Compruebo que no tengo soldados cerca.
  for (int i = 0; i < kNumSoldiers; i++) {
    if (getDistanceToPoint(game.soldier_[i].position_) <= kGuardDetectionRadius) {
      startSuspicing(&game.soldier_[i]);
      return;
    }
  }

  // Si pasamos cerca de la puerta A y la vemos abierta, pasamos a alerta intstantaneamoente
  if (isNextToDestiny(game.door_A_to_door_B_path_.path_vector_[game.door_A_to_door_B_path_.length() - 2])) {
    if (game.is_door_B_opened_) {
      state_ = kGuardState_Alert;
      movement_ = kGuardMovement_Tracker;
      current_path_ = &game.door_A_to_door_B_path_;
      inverse_path_ = false;
      tracking_id_ = game.door_A_to_door_B_path_.length() - 1;
      destiny_ = current_path_->path_vector_[tracking_id_];
      lookAt(current_path_->path_vector_[tracking_id_]);
    }
  }
  // Si de lo contrario estamos cerca de la puerta A y la vemos abierta, pasamos a alerta instantenamente.
  else if (isNextToDestiny(game.door_A_to_door_B_path_.path_vector_[0])) {
    if (game.is_door_B_opened_) {
      state_ = kGuardState_Alert;
      movement_ = kGuardMovement_Tracker;
      current_path_ = &game.resting_to_door_A_path_;
      inverse_path_ = false;
      tracking_id_ = game.resting_to_door_A_path_.length() - 1;
      destiny_ = current_path_->path_vector_[tracking_id_];
      lookAt(current_path_->path_vector_[tracking_id_]);
    }
  }

  // Si estamos volviendo de la alarma y pasamos por algun checkpoint, patrullamos.
  else if (movement_ == kGuardMovement_Tracker) {
    // Comprobamos dos checkpoints claves. y si pasamos por ahi, empezamos de nuevo a patrullar.
    if (isNextToDestiny(game.checkpoint_list_[4].position_)) {
      current_checkpoint_ = game.checkpoint_list_[4].getRandomNeigbor();
      movement_ = kGuardMovement_Patrol;
      destiny_ = current_checkpoint_->position_;
      lookAt(destiny_);
    }
    else if (isNextToDestiny(game.checkpoint_list_[12].position_)) {
      current_checkpoint_ = game.checkpoint_list_[12].getRandomNeigbor();
      movement_ = kGuardMovement_Patrol;
      destiny_ = current_checkpoint_->position_;
      lookAt(destiny_);
    }
    else if (getDistanceToPoint(game.door_A_to_door_B_path_.path_vector_[0]) < 10.0f) {
      // Si pasamos cerca de la puerta A , nos vamos directos hacia la puerta B.
      // Porque asi nos encontraremos con un checkpoint de camino y patrullaremos.
      tracking_id_ = 1;
      current_path_ = &game.door_A_to_door_B_path_;
      destiny_ = current_path_->path_vector_[tracking_id_];
      inverse_path_ = false;
      lookAt(destiny_);
    }
    // Tambien comprobamos dos puntos mas para evitar que entre en la zona de trabajo al estar normal.
    else if (getDistanceToPoint(game.door_A_to_door_B_path_.path_vector_[1]) < 10.0f) {
      tracking_id_ = 2;
      current_path_ = &game.door_A_to_door_B_path_;
      destiny_ = current_path_->path_vector_[tracking_id_];
      inverse_path_ = false;
      lookAt(destiny_);
    }
    else if (getDistanceToPoint(game.door_A_to_door_B_path_.path_vector_[2]) < 10.0f) {
      tracking_id_ = 3;
      current_path_ = &game.door_A_to_door_B_path_;
      destiny_ = current_path_->path_vector_[tracking_id_];
      inverse_path_ = false;
      lookAt(destiny_);
    }
  }

  // Por prevenir que nos salgamos comprobamos si estamos frente de las dos puertas
  if (isNextToDestiny(game.resting_to_door_A_path_.path_vector_[game.resting_to_door_A_path_.length() - 1])) {
    // Estamos frente a la puerta A
    if (game.is_door_A_opened_) {
      game.openDoorA(false); 
      game.activeAlarm();
    }
    // pasamos a realizar el recorrido de puerta A a B
    movement_ = kGuardMovement_Tracker;
    current_path_ = &game.door_A_to_door_B_path_;
    tracking_id_ = 0;
    inverse_path_ = false;
    destiny_ = current_path_->path_vector_[tracking_id_];
    lookAt(destiny_);
  }
  else if (isNextToDestiny(game.door_A_to_door_B_path_.path_vector_[game.door_A_to_door_B_path_.length() - 1])) {
    // si Estamos frente a la puerta B 
    if (game.is_door_B_opened_) {
      game.openDoorB(false); 
      game.activeAlarm();
    }
    // pasamos a realizar el recorrido de puerta B a A y viceversa buscando soldados o prisioneros.
    movement_ = kGuardMovement_Tracker;
    current_path_ = &game.door_A_to_door_B_path_;
    tracking_id_ = current_path_->length() - 2;
    inverse_path_ = true;
    destiny_ = current_path_->path_vector_[tracking_id_];
    lookAt(destiny_);
  }



}

void Guard::alertUpdate() {

  auto& game = Game::instance();

  if (isNextToDestiny(game.door_A_to_door_B_path_.path_vector_[game.door_A_to_door_B_path_.length() - 1])) {
    // Estamos frente a la puerta B
    if (game.is_door_B_opened_) {
      game.openDoorB(false);
      game.activeAlarm();
    }
  }
  else if (isNextToDestiny(game.resting_to_door_A_path_.path_vector_[game.resting_to_door_A_path_.length() - 1])) {
    // Estamos frente a la puerta B
    if (game.is_door_A_opened_) {
      game.openDoorA(false);
      game.activeAlarm();
    }
  }
  
}

void Guard::suspiciousUpdate() {
  auto& game = Game::instance();

  // Si pasamos por delante de la puerta B, comprobamos que esta abierta.
  if (current_path_ == &game.door_A_to_door_B_path_) {
    if (tracking_id_ >= game.door_A_to_door_B_path_.length() - 1) {
      if (game.is_door_B_opened_) {
        // Si esta abierta pasamos al modo alerta.
        state_ = kGuardState_Alert;
      }
    }
    else if (tracking_id_ < 0) {
      if (game.is_door_A_opened_) {
        // Si esta abierta pasamos al modo alerta.
        state_ = kGuardState_Alert;
      }
      current_path_ = &game.resting_to_door_A_path_;
      inverse_path_ = false;
      tracking_id_ = game.resting_to_door_A_path_.length() - 1;
      destiny_ = current_path_->path_vector_[tracking_id_];
      lookAt(current_path_->path_vector_[tracking_id_]);
    }
  }

}

void Guard::alarmUpdate() {
  auto& game = Game::instance();

  // comprobamos que no haya soldados cerca, si lo hay lo perseguimos.
  bool there_is_no_soldier_near = true;
  for (int i = 0; i < kNumSoldiers; i++) {
    if (getDistanceToPoint(game.soldier_[i].position_) < kGuardDetectionRadius) {
      there_is_no_soldier_near = false;
      game.alarm_timer_ = kAlarmDeactivateTime;
      if (game.soldier_[i].tracking_id_ >= 0 &&
          game.soldier_[i].tracking_id_ < game.soldier_[i].current_path_->length() &&
          (game.soldier_[i].current_path_ != &game.door_A_to_door_B_path_ ||
          game.soldier_[i].current_path_ != &game.resting_to_door_A_path_)) {
        movement_ = kGuardMovement_Tracker;
        current_path_ = game.soldier_[i].current_path_;
        tracking_id_ = game.soldier_[i].tracking_id_;
        inverse_path_ = game.soldier_[i].inverse_path_;
        if (inverse_path_) { tracking_id_ += 1; }
        else { tracking_id_ -= 1; };
        destiny_ = game.soldier_[i].position_;
        lookAt(destiny_);
        break;
      }
    }
  }

  // Comprobamos tambien que no haya prisioneros cerca. Si lo hay lo perseguimos.
  // le doy prioridad a los soldados ya que son los que abren puertas.
  if (there_is_no_soldier_near) {
    for (int i = 0; i < kNumPrisoners; i++) {
      if (getDistanceToPoint(game.prisoner_[i].position_) < kGuardDetectionRadius) {
        if (game.prisoner_[i].tracking_id_ >= 0 &&
            game.prisoner_[i].tracking_id_ < game.prisoner_[i].current_path_->length() &&
            (game.prisoner_[i].current_path_ == &game.door_A_to_door_B_path_ ||
            game.prisoner_[i].current_path_ == &game.resting_to_door_A_path_)) {
          movement_ = kGuardMovement_Tracker;
          current_path_ = game.prisoner_[i].current_path_;
          tracking_id_ = game.prisoner_[i].tracking_id_;
          inverse_path_ = game.prisoner_[i].inverse_path_;
          if (inverse_path_) { tracking_id_ += 1; }
          else { tracking_id_ -= 1; };
          destiny_ = game.prisoner_[i].position_;
          lookAt(destiny_);
          break;
        }
      }
    }
  }

  if (isNextToDestiny(game.door_A_to_door_B_path_.path_vector_[game.door_A_to_door_B_path_.length() - 1])) {
    // si Estamos frente a la puerta B 
    if (game.is_door_B_opened_) {
      if (there_is_no_soldier_near) { game.openDoorB(false); }
      game.alarm_timer_ = kAlarmDeactivateTime;
    }
    // pasamos a realizar el recorrido de puerta B a A y viceversa buscando soldados o prisioneros.
    movement_ = kGuardMovement_Tracker;
    current_path_ = &game.door_A_to_door_B_path_;
    tracking_id_ = current_path_->length() - 2;
    inverse_path_ = true;
    destiny_ = current_path_->path_vector_[tracking_id_];
    lookAt(destiny_);
  }
  else if (isNextToDestiny(game.door_A_to_door_B_path_.path_vector_[game.door_A_to_door_B_path_.length() - 2])) {
    // si Estamos CERCA frente a la puerta B 
    if (game.is_door_B_opened_) {
      // pasamos a ir a la puerta B para cerrarla.
      movement_ = kGuardMovement_Tracker;
      current_path_ = &game.door_A_to_door_B_path_;
      tracking_id_ = current_path_->length() - 1;
      inverse_path_ = true;
      destiny_ = current_path_->path_vector_[tracking_id_];
      lookAt(destiny_);
    }
  }
  else if (isNextToDestiny(game.resting_to_door_A_path_.path_vector_[game.resting_to_door_A_path_.length() - 1])) {
    // Estamos frente a la puerta A
    if (game.is_door_A_opened_) {
      if (there_is_no_soldier_near) { game.openDoorA(false); }
      game.alarm_timer_ = kAlarmDeactivateTime;
    }
    // pasamos a realizar el recorrido de puerta A a B y viceversa buscando soldados o prisioneros.
    movement_ = kGuardMovement_Tracker;
    current_path_ = &game.door_A_to_door_B_path_;
    tracking_id_ = 0;
    inverse_path_ = false;
    destiny_ = current_path_->path_vector_[tracking_id_];
    lookAt(destiny_);
  }
  else if (isNextToDestiny(game.door_A_to_door_B_path_.path_vector_[0])) {
    // si Estamos CERCA frente a la puerta A 
    if (game.is_door_A_opened_ || (tracking_id_ < 0 && current_path_ == &game.door_A_to_door_B_path_)) {
      // pasamos a ir a la puerta A para cerrarla.
      movement_ = kGuardMovement_Tracker;
      current_path_ = &game.resting_to_door_A_path_;
      tracking_id_ = current_path_->length() - 1;
      inverse_path_ = false;
      destiny_ = current_path_->path_vector_[tracking_id_];
      lookAt(destiny_);
    }

  }

}

/*******************************************************************************
***                             BODY UPDATES                                 ***
*******************************************************************************/

void Guard::bodyUpdate() {

  switch (movement_) {
    case kGuardMovement_Determ:
    moveDeterm();
    break;
    case kGuardMovement_Patrol:
    movePatrol();
    break;
    case kGuardMovement_Tracker:
    moveTracker();
    break;
  }

}

void Guard::lookAt(JMATH::Vec2 destiny) {
  velocity_ = { destiny.x - position_.x,destiny.y - position_.y };
  velocity_ = Vec2Normalize(velocity_);
}

void Guard::drawCollisionRadius() {

  float vertices[100];
  float portion = N_2Pi / 50.0f;
  for (unsigned int i = 0; i < 50; ++i) {
    vertices[i * 2] = position_.x + JMATH::Cos(portion * i) * kGuardDetectionRadius;
    vertices[i * 2 + 1] = position_.y + JMATH::Sin(portion * i) * kGuardDetectionRadius;
  }

  ESAT::DrawSetStrokeColor(0, 0, 0, 255);
  ESAT::DrawSetFillColor(255, 255, 255, 50);

  switch (state_) {
    case kGuardState_Suspicious: { ESAT::DrawSetFillColor(0, 0, 255, 50); } break;
    case kGuardState_Alert: { ESAT::DrawSetFillColor(255, 255, 0, 50); } break;
    case kGuardState_Alarm: { ESAT::DrawSetFillColor(255, 0, 0, 50); } break;
    default:
    break;
  }

  ESAT::DrawSolidPath(vertices, 50, true);

}

void Guard::moveDeterm() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_};
}



void Guard::movePatrol() {
  auto& game = Game::instance();

  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ };

  if (isNextToDestiny(destiny_)) {
    current_checkpoint_ = current_checkpoint_->getRandomNeigbor();
    destiny_ = current_checkpoint_->position_;
    destiny_ += JMATH::getFlockingVariationRange(kGuardFlockingRadius, kGuardFlockingRadius);
    lookAt(destiny_);
  }
}

void Guard::moveTracker() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ };

  if (isNextToDestiny(destiny_)) {
    if (inverse_path_) { tracking_id_--; }
    else { tracking_id_++; }
    if (tracking_id_ < current_path_->length() && tracking_id_ >= 0) {
      destiny_ = current_path_->path_vector_[tracking_id_];
      destiny_ += JMATH::getFlockingVariationRange(kGuardFlockingRadius, kGuardFlockingRadius);
      lookAt(destiny_);
    }
  }

}

void Guard::startSuspicing(Soldier* soldier) {

  auto& game = Game::instance();
  if (soldier->tracking_id_ >= 0 &&
      soldier->tracking_id_ < soldier->current_path_->length() &&
      (soldier->current_path_ == &game.door_A_to_door_B_path_ ||
      soldier->current_path_ == &game.resting_to_door_A_path_)) {
    movement_ = kGuardMovement_Tracker;
    state_ = kGuardState_Suspicious;
    current_path_ = soldier->current_path_;
    tracking_id_ = soldier->tracking_id_;
    inverse_path_ = soldier->inverse_path_;
    if (inverse_path_) { tracking_id_ += 1; }
    else { tracking_id_ -= 1; };
    destiny_ = soldier->position_;
    lookAt(destiny_);
  }
}

void Guard::startAlarm() {
  state_ = kGuardState_Alarm;

}

void Guard::stopAlarm() {
  state_ = kGuardState_Normal;
}


/*******************************************************************************
***                              TRANSITIONS                                 ***
*******************************************************************************/


/*******************************************************************************
***                                 UTILS                                    ***
*******************************************************************************/

float Guard::getDistanceToPoint(const JMATH::Vec2 point) {
  JMATH::Vec2 distance = { point.x - position_.x, point.y - position_.y };
  float result = JMATH::Vec2Magnitude(distance);
  if (result < 0.0f) {
    result *= -1.0f;
  }
  return result;
}

JMATH::Vec2 Guard::getCloserAgentPosition() {
  float min = 99999.0f;
  for (int i = 0; i < kNumSoldiers; i++) {
  }
  return{ 0.0f, 0.0f };
}

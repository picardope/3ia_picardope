#include "ESAT_SDK/window.h"
#include "ESAT_SDK/input.h"
#include "ESAT_SDK/draw.h"
#include "ESAT_SDK/time.h"
#include "gamemanager.h"
#include "jmath.h"
#include "astar.h"
#include "cost_map.h"
#include <string>
#include <time.h>


/**
* @brief Render a text.
* @param text Text to render.
* @param position Vector where the text will be rendered.
* @param size Size of the text.
* @param r Red color component of the text.
* @param g Green color component of the text.
* @param b Blue color component of the text.
*/
void DrawText(const char* text, JMATH::Vec2 position, float size = 30, unsigned char r = 0, unsigned char g = 0, unsigned char b = 0) {
  ESAT::DrawSetStrokeColor(r, g, b, 255);
  ESAT::DrawSetFillColor(r, g, b, 255);
  ESAT::DrawSetTextSize(size);
  ESAT::DrawSetTextBlur(0.0f);
  ESAT::DrawText(position.x, position.y, text);
}

/// @brief Init function of the application.
void Init() {
  auto& game = Game::instance();

  game.window_width = 1400;
  game.window_height = 978;
  ESAT::WindowInit((int)game.window_width, (int)game.window_height);
  
  ESAT::WindowSetMouseVisibility(true);
  ESAT::DrawSetTextFont("../data/fonts/DigitFont.TTF");
  //ESAT::DrawSetTextBlur(true);
  /* TESTEO CON MAPA DE COSTES POR TEXTO.
  game.cost_map_.load("./../data/map_12_cost.txt");
  game.background_image_.init("./../data/map_12_terrain.png");
  */
  /* TESTEOS CON MAPA DE COSTES POR IMAGEN.
  game.cost_map_.load("./../data/map_03_960x704_bw.png");
  game.background_image_.init("./../data/map_03_60x44_bw.png");
  game.background_image_.init("./../data/map_03_960x704_color.png");
  game.background_image_.init("./../data/map_02_terrain.png");
  game.cost_map_.load("./../data/map_02_cost.png");
  game.cost_map_.load("./../data/map_03_60x44_bw.png");
  */
  game.cost_map_.load("./../data/map_03_120x88_bw.png");

  game.background_image_.init("./../data/map_03_960x704_layoutAB.png");
  game.background_image_.set_scale(game.window_width / (float)game.background_image_.width(),
                                   game.window_height / (float)game.background_image_.height());

  
  game.alarm_off_image_.init("./../data/alarmOFF.png");
  game.alarm_on_image_.init("./../data/alarmON.png");
  game.alarm_off_image_.set_scale((game.window_width * 0.12f) / (float)game.alarm_off_image_.width(),
                                  (game.window_height * 0.1f / (float)game.alarm_off_image_.height()));
  game.alarm_on_image_.set_scale((game.window_width * 0.12f) / (float)game.alarm_on_image_.width(),
                                 (game.window_height * 0.1f / (float)game.alarm_on_image_.height()));
  game.feedback_image_.init("./../data/feedback.png");
  game.feedback_image_.set_scale((game.window_width * 0.25f) / (float)game.feedback_image_.width(),
                                 (game.window_height * 0.5f / (float)game.feedback_image_.height()));
  game.feedback_image_.set_position(0.0f, game.window_height * 0.25f);

  game.initAreasLimits();
  game.initCheckPoints();
  game.initPaths();
  game.initAgents();
  

}

/// @brief Input function of the application.
void Input() {
  auto& game = Game::instance();
  game.end_program = !ESAT::WindowIsOpened() ||
                      ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
  game.input_mouse_button_up = ESAT::MouseButtonUp(0);



  // LAS CONSECUENCIAS DE ESTOS INPUTS DEBERIAN REALIZARSE EN EL UPDATE, PERO
  // AL SER ALGO SIMPLEMENTE PARA TESTEO Y DEBUG, LO DEJO TODO JUNTO AQUI.
  if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Space)) {
    if (game.is_alarm_mode_on_) {
      game.deactivateAlarm();
    }
    else {
      game.activeAlarm();
    }
    printf("ALAAAAAAAARM");
  }

  if (ESAT::IsKeyDown('2')) {
    if (game.is_door_A_opened_) {
      game.openDoorA(false);
    }
    else {
      game.openDoorA(true);
    }
  }

  if (ESAT::IsKeyDown('1')) {
    if (game.is_door_B_opened_) {
      game.openDoorB(false);
    }
    else {
      game.openDoorB(true); 
    }
  }
}

/// @brief Update function of the application.
void Update(double delta_time) {
  auto& game = Game::instance();

  game.updateTimers(delta_time);
  game.updateAgents(delta_time);
}

/// @brief Render of the scene.
void DrawScene() {
  auto& game = Game::instance();
  
  // IMAGENES
  game.background_image_.draw();
  game.feedback_image_.draw();

  // PINTAMOS LAS PUERTAS DE UN COLOR U OTRO SEGUN SI ESTAN ABIERTAS O NO.
  ESAT::DrawSetFillColor(255, 255, 255, 255);
  if (game.is_door_A_opened_) {
    ESAT::DrawSetFillColor(0, 255, 0, 255);
  }
  ESAT::DrawSolidPath(game.door_A_area_, 5);
  
  ESAT::DrawSetFillColor(255, 255, 255, 255);
  
  if (game.is_door_B_opened_) {
    ESAT::DrawSetFillColor(0, 255, 0, 255);
  }
  ESAT::DrawSolidPath(game.door_B_area_, 5);

  // PINTAMOS LOS AGENTES EN ESCENA.
  game.drawAgents();


  // PINTAMOS LOS TEXTOS INDICATIVOS.
  char text[30];
  if (game.is_alarm_mode_on_) {
    game.alarm_on_image_.draw();
    sprintf(text, " Alarm Time : %d", (int)(game.alarm_timer_ * 0.001f));
  }
  else {
    game.alarm_off_image_.draw();
    sprintf(text, " shift change : %d", (int)(game.prisoners_swap_between_resting_working_timer_ * 0.001f));
  }
  DrawText(text, { 10.0f, game.window_height * 0.20f }, 30, 255, 255, 255);

  if (game.num_prisoners_free_ >= kNumPrisoners) {
    sprintf(text, "PRISONERS WIN");
    DrawText(text, { game.window_width * 0.38f, game.window_height * 0.42f }, 80, 255, 255, 255);
  }

}


/// @brief Render function of the application.
void Draw() {
  auto& game = Game::instance();
  ESAT::DrawBegin();
  ESAT::DrawClear(60, 60, 60);

  DrawScene();

  ESAT::DrawEnd();
  ESAT::WindowFrame();
}


/// @brief Start function of the application.
void Start() {
  auto& game = Game::instance();
  srand(time(NULL));
  game.time_step = 16.6;

  double current_time = ESAT::Time(); 

  while (!game.end_program) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= game.time_step) {

      Update(game.time_step);

      current_time += game.time_step;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }

}

/// @brief Finish function of the application.
void Finish() {
  ESAT::WindowDestroy();
}

/// @brief Main function of the application.
int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

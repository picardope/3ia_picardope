/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA Soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "soldier.h"
#include "ESAT_SDK/Time.h"
#include "ESAT_SDK/draw.h"
#include "jmath.h"
#include "gamemanager.h"


/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/


Soldier::Soldier() : Agent() {
  movement_ = kSoldierMovement_None;
  state_ = kSoldierState_Init;
  
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = 0.0f;
  delta_ = 0.0;
  collision_radius_ = 0.0f;
  tracking_id_ = 0;
  current_path_ = nullptr;
  inverse_path_ = false;
}

Soldier::Soldier(const Soldier& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
}
Soldier& Soldier::operator=(const Soldier& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
  return *this;
}

Soldier::~Soldier() {}


/*******************************************************************************
***                             AGENT METHODS                                ***
*******************************************************************************/

void Soldier::init() {
  auto& game = Game::instance();
  health_ = 100;
  speed_ = kSoldierNormalSpeed + (float)((rand() % 10));
  state_ = kSoldierState_SearchingDoors;
  movement_ = kSoldierMovement_Tracker;
  current_path_ = &game.out_door_B_to_base_B_path_;
  inverse_path_ = true;
  tracking_id_ = current_path_->length() - 1;
  destiny_ = current_path_->path_vector_[tracking_id_];
  lookAt(destiny_);
}

void Soldier::draw() {
  DrawSquare(position_, 0,255,0,255, 5.0f);
}

void Soldier::update(const double delta_time) {
  delta_ = delta_time;
  mindUpdate();
  bodyUpdate();
}


/*******************************************************************************
***                              MIND UPDATES                                   ***
*******************************************************************************/



void Soldier::mindUpdate() {
  switch (state_) {
    case kSoldierState_Init:
      init();
    break;
    case kSoldierState_SearchingDoors:
      searchingDoorsUpdate();
    break;
    case kSoldierState_Alarm:
      alarmUpdate();
    break;
  }
  
}


void Soldier::searchingDoorsUpdate() {

  bool path_has_changed = false;

  auto& game = Game::instance();
  
  if (current_path_ == &game.out_door_B_to_base_B_path_) {
  // He llegado a la puerta B desde la base B.
    if (tracking_id_ < 0) {
      // Si te encuentras la puerta abierta entras por dentro de la fortaleza.
      if (game.is_door_B_opened_) {
        current_path_ = &game.door_A_to_door_B_path_;
        inverse_path_ = true;
        tracking_id_ = current_path_->length() - 1;
        path_has_changed = true;
      }
      else { // Si te la encuentras cerrada vas a la puerta A por fuera de la fortaleza.
        current_path_ = &game.out_door_A_to_out_door_B_;
        inverse_path_ = true;
        tracking_id_ = current_path_->length() - 1;
        path_has_changed = true;
      }
    }
    else if (tracking_id_ >= current_path_->length()) {
      // Hemos vuelto a la base B
      current_path_ = &game.out_door_B_to_base_B_path_;
      // Hacemos la ruta inversa y volvemos a la puerta B a abrir puertas.
      inverse_path_ = true;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
      // Si todos los prisioneros han sido liberados, nos quedamos en base.
      if (game.num_prisoners_free_ >= kNumPrisoners) {
        movement_ = kSoldierMovement_None;
        state_ = kSoldierState_End;
      }
    }
  }
  else if (current_path_ == &game.out_door_A_to_out_door_B_) {
    // He llegado a la puerta B desde la base B.
    if (tracking_id_ < 0) {
    // Abres la puerta A y entras en la fortaleza para buscar la puerta B.
      game.openDoorA(true);
      current_path_ = &game.door_A_to_door_B_path_;
      inverse_path_ = false;
      tracking_id_ = 0;
      path_has_changed = true;
    }
  }
  else if (current_path_ == &game.door_A_to_door_B_path_) {
    // Si llego a la puerta A desde dentro (me falta un punto para llegar), asi
    // que voy hasta el ultimo punto de la resting to working path.
    if (tracking_id_ < 0) {
      current_path_ = &game.resting_to_door_A_path_;
      inverse_path_ = false;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
    }
    // Si llego a la puerta B desde dentro, la abro compruebo la alarma.
    else if (tracking_id_ >= current_path_->length()) {
      game.openDoorB(true);
      // Si no esta la alarma vuelvo a la otra puerta para abrirla.
      current_path_ = &game.door_A_to_door_B_path_;
      inverse_path_ = true;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
    }
  }
  else if (current_path_ == &game.resting_to_door_A_path_) {
    // Si estoy en la puerta A por dentro. La abro y compruebo alarma.
    if (tracking_id_ >= current_path_->length()) {
      game.openDoorA(true);
      current_path_ = &game.door_A_to_door_B_path_;
      inverse_path_ = false;
      tracking_id_ = 0;
      path_has_changed = true;
    }
  }
  else if (current_path_ == &game.base_A_to_out_door_A_path_) {
    // Si he ido hasta la base A, hago el camino inverso y vuelvo a buscar puertas.
    if (tracking_id_ < 0) {
      inverse_path_ = false;
      tracking_id_ = 0;
      path_has_changed = true;
      // Si todos los prisioneros han sido liberados, nos quedamos en base.
      if (game.num_prisoners_free_ >= kNumPrisoners) {
        movement_ = kSoldierMovement_None;
        state_ = kSoldierState_End;
      }
    }
    // Si llegamos a la puerta A por fuera, la abrimos y vamos a la puerta B.
    else if (tracking_id_ >= current_path_->length()) {
      game.openDoorA(true);
      current_path_ = &game.door_A_to_door_B_path_;
      inverse_path_ = false;
      tracking_id_ = 0;
      path_has_changed = true;
    }
  }

  if (path_has_changed) {
    destiny_ = current_path_->path_vector_[tracking_id_];
    lookAt(destiny_);
  }

}



void Soldier::alarmUpdate() {
  
  auto& game = Game::instance();
  
  bool path_has_changed = false;

  if (current_path_ == &game.out_door_B_to_base_B_path_) {
    // He llegado a la puerta B desde la base B.
    if (tracking_id_ < 0) {
      // Como esta la alarma vuelvo a base.
      inverse_path_ = false;
      tracking_id_ = 0;
      path_has_changed = true;
    }
    else if (tracking_id_ >= current_path_->length()) {
      // Hemos vuelto a la base B
      current_path_ = &game.out_door_B_to_base_B_path_;
      // Hacemos la ruta inversa y volvemos a la puerta B a abrir puertas.
      inverse_path_ = true;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
      // Si todos los prisioneros han sido liberados, nos quedamos en base.
      if (game.num_prisoners_free_ >= kNumPrisoners) {
        movement_ = kSoldierMovement_None;
        state_ = kSoldierState_End;
      }
    }
  }
  else if (current_path_ == &game.out_door_A_to_out_door_B_) {
    // He llegado a la puerta A desde la puerta B.
    if (tracking_id_ < 0) {
      // Como esta la alarma simplemente vuelvo a base A.
      current_path_ = &game.base_A_to_out_door_A_path_;
      inverse_path_ = true;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
    }
  }
  else if (current_path_ == &game.door_A_to_door_B_path_) {
    // Si llego a la puerta A desde dentro (me falta un punto para llegar), asi
    // que voy hasta el ultimo punto de la resting to working path.
    if (tracking_id_ < 0) {
      current_path_ = &game.resting_to_door_A_path_;
      inverse_path_ = false;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
    }
    // Si llego a la puerta B desde dentro, la abro compruebo la alarma.
    else if (tracking_id_ >= current_path_->length()) {
      game.openDoorB(true);
      // si esta la alarma vuelvo a base.
      current_path_ = &game.out_door_B_to_base_B_path_;
      inverse_path_ = false;
      tracking_id_ = 0;
      path_has_changed = true;
    }
  }
  else if (current_path_ == &game.resting_to_door_A_path_) {
    // Si estoy en la puerta A por dentro. La abro y compruebo alarma.
    if (tracking_id_ >= current_path_->length()) {
      game.openDoorA(true);
      current_path_ = &game.base_A_to_out_door_A_path_;
      inverse_path_ = true;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
    }
  }
  else if (current_path_ == &game.base_A_to_out_door_A_path_) {
    // Si he ido hasta la base A, hago el camino inverso y vuelvo a buscar puertas.
    if (tracking_id_ < 0) {
      inverse_path_ = false;
      tracking_id_ = 0;
      path_has_changed = true;
      // Si todos los prisioneros han sido liberados, nos quedamos en base.
      if (game.num_prisoners_free_ >= kNumPrisoners) {
        movement_ = kSoldierMovement_None;
        state_ = kSoldierState_End;
      }
    }
    // Si llegamos a la puerta A como estamos en alarma volvemos a la base A.
    else if (tracking_id_ >= current_path_->length()) {
      current_path_ = &game.base_A_to_out_door_A_path_;
      inverse_path_ = true;
      tracking_id_ = current_path_->length() - 1;
      path_has_changed = true;
    }
  }

  if (path_has_changed) {
    destiny_ = current_path_->path_vector_[tracking_id_];
    lookAt(destiny_);
  }

}




/*******************************************************************************
***                             BODY UPDATES                                 ***
*******************************************************************************/

void Soldier::bodyUpdate() {

  switch (movement_) {
    case kSoldierMovement_Determ:
    moveDeterm();
    break;
    case kSoldierMovement_Tracker:
    moveTracker();
    break;
  }

}

void Soldier::lookAt(JMATH::Vec2 destiny) {
  velocity_ = { destiny.x - position_.x,destiny.y - position_.y };
  velocity_ = Vec2Normalize(velocity_);
}

void Soldier::moveDeterm() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_};
}

void Soldier::moveTracker() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ };

  if (isNextToDestiny(destiny_)) {
    if (inverse_path_) { tracking_id_--; }
    else { tracking_id_++; }
    if (tracking_id_ < current_path_->length() && tracking_id_ >= 0) {
      destiny_ = current_path_->path_vector_[tracking_id_];
      lookAt(destiny_);
    }
  }

}

/*******************************************************************************
***                              TRANSITIONS                                 ***
*******************************************************************************/


void Soldier::startAlarm() {
  if (state_ != kSoldierState_End && state_ != kSoldierState_Init) {
    state_ = kSoldierState_Alarm;
  }
}

void Soldier::stopAlarm() {
  state_ = kSoldierState_SearchingDoors;
}



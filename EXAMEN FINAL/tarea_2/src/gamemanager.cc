/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "gamemanager.h"
#include "jmath.h"
#include <stdio.h>
#include <stdlib.h>
#include "ESAT_SDK/draw.h"



void DrawSquare(JMATH::Vec2 position, 
                unsigned char r, 
                unsigned char g, 
                unsigned char b, 
                unsigned char a,
                float size) {
  float half_size = size * 0.5f;
  float vertices[10] = { position.x - half_size, position.y - half_size,
                         position.x - half_size, position.y + half_size,
                         position.x + half_size, position.y + half_size,
                         position.x + half_size, position.y - half_size,
                         position.x - half_size, position.y - half_size };
  ESAT::DrawSetStrokeColor(0, 0, 0, 255);
  ESAT::DrawSetFillColor(r, g, b, a);
  ESAT::DrawSolidPath(vertices, 5, true);
}

void DrawRect(JMATH::Vec2 position, 
              unsigned char r, 
              unsigned char g, 
              unsigned char b,
              unsigned char a, 
              JMATH::Vec2 size) {
  JMATH::Vec2 half_size = { size.x * 0.5f, size.y * 0.5f };
  float vertices[10] = { position.x - half_size.x, position.y - half_size.y,
                         position.x - half_size.x, position.y + half_size.y,
                         position.x + half_size.x, position.y + half_size.y,
                         position.x + half_size.x, position.y - half_size.y,
                         position.x - half_size.x, position.y - half_size.y };
  ESAT::DrawSetStrokeColor(255, 255, 255, 255);
  ESAT::DrawSetFillColor(r, g, b, a);
  ESAT::DrawSolidPath(vertices, 5, true);
}

/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

Game& Game::instance() {
  static Game* singleton = new Game();
  return *singleton;
}

Game::Game() {
  end_program = false;
  time_step = 16.6;
  window_width = 800;
  window_height = 600;
  goal = origin = { 0.0f, 0.0f };
  goal_set = origin_set = input_mouse_button_up = false;
  prisoners_swap_between_resting_working_timer_ = kSwapBetweenRestingAndWorkingTime * 0.5f;
  is_door_A_opened_ = false;
  is_door_B_opened_ = false;
  is_alarm_mode_on_ = false;
  num_prisoners_free_ = 0;
}

Game::~Game() {

}

void Game::updateTimers(const double delta_time) {

  if (is_alarm_mode_on_) {
    alarm_timer_ -= (float)delta_time;
    if (alarm_timer_ <= 0.0f) {
      deactivateAlarm();
    }
  }
  prisoners_swap_between_resting_working_timer_ -= (float)delta_time;

  if (prisoners_swap_between_resting_working_timer_ <= 0.0f) {
    prisoners_swap_between_resting_working_timer_ = kSwapBetweenRestingAndWorkingTime;
    for (int i = 0; i < kNumPrisoners; i++) {
      prisoner_[i].swapBetweenWorkingAndResting();
    }
  }
  else {
    for (int i = 0; i < kNumPrisoners; i++) {
      prisoner_[i].update(delta_time);
    }
  }
}

void Game::activeAlarm() {
  is_alarm_mode_on_ = true;
  alarm_timer_ = kAlarmDeactivateTime;
  for (int i = 0; i < kNumPrisoners; i++) {
    prisoner_[i].startAlarm();
  }
  for (int i = 0; i < kNumSoldiers; i++) {
    soldier_[i].startAlarm();
  }
  for (int i = 0; i < kNumGuards; i++) {
    guard_[i].startAlarm();
  }
}

void Game::deactivateAlarm() {
  is_alarm_mode_on_ = false;
  for (int i = 0; i < kNumGuards; i++) {
    guard_[i].state_ = kGuardState_Normal;
  }

  for (int i = 0; i < kNumPrisoners; i++) {
    prisoner_[i].stopAlarm();
  }
  for (int i = 0; i < kNumSoldiers; i++) {
    soldier_[i].stopAlarm();
  }
  for (int i = 0; i < kNumGuards; i++) {
    guard_[i].stopAlarm();
  }
}

void Game::initAreasLimits() {

  // AREAS LIMITS
  min_resting_area_ = { window_width * 0.57f, window_height * 0.69f };
  max_resting_area_ = { window_width * 0.80f, window_height * 0.77f };
  min_packing_area_ = { window_width * 0.43f, window_height * 0.30f };
  max_packing_area_ = { window_width * 0.46f, window_height * 0.51f };
  min_unpacking_area_ = { window_width * 0.70f, window_height * 0.30f };
  max_unpacking_area_ = { window_width * 0.73f, window_height * 0.51f };
  min_working_area_ = { window_width * 0.43f, window_height * 0.30f };
  max_working_area_ = { window_width * 0.73f, window_height * 0.51f };
  min_base_B_area_ = { window_width * 0.01f, window_height * 0.91f };
  max_base_B_area_ = { window_width * 0.08f, window_height * 0.99f };

  door_B_area_[0] = window_width * 0.145f;
  door_B_area_[1] = window_height * 0.305f;
  door_B_area_[2] = window_width * 0.145f;
  door_B_area_[3] = window_height * 0.32f;
  door_B_area_[4] = window_width * 0.18f;
  door_B_area_[5] = window_height * 0.32f;
  door_B_area_[6] = window_width * 0.18f;
  door_B_area_[7] = window_height * 0.305f;
  door_B_area_[8] = window_width * 0.145f;
  door_B_area_[9] = window_height * 0.305f;

  door_A_area_[0] = window_width * 0.85f;
  door_A_area_[1] = window_height * 0.365f;
  door_A_area_[2] = window_width * 0.85f;
  door_A_area_[3] = window_height * 0.41f;
  door_A_area_[4] = window_width * 0.86f;
  door_A_area_[5] = window_height * 0.41f;
  door_A_area_[6] = window_width * 0.86f;
  door_A_area_[7] = window_height * 0.365f;
  door_A_area_[8] = window_width * 0.85f;
  door_A_area_[9] = window_height * 0.365f;

}

void Game::initPaths() {

  openDoorA(false);
  openDoorB(false);
  
  resting_to_working_path_.addPoint({ window_width * 0.5925f, window_height * 0.70f });
  resting_to_working_path_.addPoint({ window_width * 0.5925f, window_height * 0.63f });
  resting_to_working_path_.addPoint({ window_width * 0.4833f, window_height * 0.58f });
  resting_to_working_path_.addPoint({ window_width * 0.4833f, window_height * 0.53f });

  resting_to_door_A_path_.addPoint({ window_width * 0.5925f, window_height * 0.72f });
  resting_to_door_A_path_.addPoint({ window_width * 0.5925f, window_height * 0.63f });
  resting_to_door_A_path_.addPoint({ window_width * 0.64f, window_height * 0.58f });
  resting_to_door_A_path_.addPoint({ window_width * 0.64f, window_height * 0.53f });
  resting_to_door_A_path_.addPoint({ window_width * 0.73f, window_height * 0.39f });
  resting_to_door_A_path_.addPoint({ window_width * 0.84f, window_height * 0.39f });

  working_to_resting_path_.addPoint({ window_width * 0.64f, window_height * 0.53f });
  working_to_resting_path_.addPoint({ window_width * 0.64f, window_height * 0.58f });
  working_to_resting_path_.addPoint({ window_width * 0.5925f, window_height * 0.63f });
  working_to_resting_path_.addPoint({ window_width * 0.5925f, window_height * 0.72f });

  door_A_to_door_B_path_.addPoint({ window_width * 0.73f, window_height * 0.39f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.73f, window_height * 0.26f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.785f, window_height * 0.26f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.785f, window_height * 0.09f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.33f, window_height * 0.09f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.30f, window_height * 0.215f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.265f, window_height * 0.22f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.265f, window_height * 0.365f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.165f, window_height * 0.365f });
  door_A_to_door_B_path_.addPoint({ window_width * 0.165f, window_height * 0.33f });

  astar_.generatePath({ window_width * 0.05f, window_height * 0.95f },
                      { window_width * 0.17f, window_height * 0.29f },
                      &cost_map_, &out_door_B_to_base_B_path_);
  
  astar_.generatePath({ window_width * 0.17f, window_height * 0.25f },
                      { window_width * 0.87f, window_height * 0.39f },
                      &cost_map_, &out_door_A_to_out_door_B_);
  
  astar_.generatePath({ window_width * 0.87f, window_height * 0.39f },
                      { window_width * 0.95f, window_height * 0.95f },
                      &cost_map_, &base_A_to_out_door_A_path_);


}

void Game::initCheckPoints() {

  // Positions
  checkpoint_list_[0].position_ = { window_width * 0.51f, window_height * 0.715f };
  checkpoint_list_[1].position_ = { window_width * 0.33f, window_height * 0.715f };
  checkpoint_list_[2].position_ = { window_width * 0.33f, window_height * 0.64f };
  checkpoint_list_[3].position_ = { window_width * 0.265f, window_height * 0.64f };
  checkpoint_list_[4].position_ = { window_width * 0.265f, window_height * 0.365f };
  checkpoint_list_[5].position_ = { window_width * 0.165f, window_height * 0.365f };
  checkpoint_list_[6].position_ = { window_width * 0.265f, window_height * 0.22f };
  checkpoint_list_[7].position_ = { window_width * 0.31f, window_height * 0.22f };
  checkpoint_list_[8].position_ = { window_width * 0.31f, window_height * 0.12f };
  checkpoint_list_[9].position_ = { window_width * 0.22f, window_height * 0.12f };
  checkpoint_list_[10].position_ = { window_width * 0.39f, window_height * 0.09f };
  checkpoint_list_[11].position_ = { window_width * 0.68f, window_height * 0.09f };
  checkpoint_list_[12].position_ = { window_width * 0.785f, window_height * 0.09f };
  checkpoint_list_[13].position_ = { window_width * 0.785f, window_height * 0.30f };
  checkpoint_list_[14].position_ = { window_width * 0.84f, window_height * 0.30f };
  checkpoint_list_[15].position_ = { window_width * 0.485f, window_height * 0.16f };
  checkpoint_list_[16].position_ = { window_width * 0.635f, window_height * 0.16f };
  checkpoint_list_[17].position_ = { window_width * 0.635f, window_height * 0.24f };
  checkpoint_list_[18].position_ = { window_width * 0.485f, window_height * 0.24f };
  checkpoint_list_[19].position_ = { window_width * 0.38f, window_height * 0.20f };
  checkpoint_list_[20].position_ = { window_width * 0.38f, window_height * 0.27f };
  checkpoint_list_[21].position_ = { window_width * 0.32f, window_height * 0.27f };
  checkpoint_list_[22].position_ = { window_width * 0.32f, window_height * 0.37f };
  checkpoint_list_[23].position_ = { window_width * 0.845f, window_height * 0.45f };
  checkpoint_list_[24].position_ = { window_width * 0.845f, window_height * 0.50f };
  checkpoint_list_[25].position_ = { window_width * 0.79f, window_height * 0.50f };
  checkpoint_list_[26].position_ = { window_width * 0.79f, window_height * 0.61f };
  checkpoint_list_[27].position_ = { window_width * 0.73f, window_height * 0.61f };
  
  // NEIGHBORS
  checkpoint_list_[0].addNeighbor(&checkpoint_list_[1]);
  checkpoint_list_[1].addNeighbor(&checkpoint_list_[0]);
  checkpoint_list_[1].addNeighbor(&checkpoint_list_[2]);
  checkpoint_list_[2].addNeighbor(&checkpoint_list_[1]);
  checkpoint_list_[2].addNeighbor(&checkpoint_list_[3]);
  checkpoint_list_[3].addNeighbor(&checkpoint_list_[2]);
  checkpoint_list_[3].addNeighbor(&checkpoint_list_[4]);
  checkpoint_list_[4].addNeighbor(&checkpoint_list_[3]);
  checkpoint_list_[4].addNeighbor(&checkpoint_list_[5]);
  checkpoint_list_[4].addNeighbor(&checkpoint_list_[6]);
  checkpoint_list_[5].addNeighbor(&checkpoint_list_[4]);
  checkpoint_list_[6].addNeighbor(&checkpoint_list_[7]);
  checkpoint_list_[6].addNeighbor(&checkpoint_list_[6]);
  checkpoint_list_[7].addNeighbor(&checkpoint_list_[8]);
  checkpoint_list_[8].addNeighbor(&checkpoint_list_[7]);
  checkpoint_list_[8].addNeighbor(&checkpoint_list_[9]);
  checkpoint_list_[8].addNeighbor(&checkpoint_list_[10]);
  checkpoint_list_[9].addNeighbor(&checkpoint_list_[8]);
  checkpoint_list_[10].addNeighbor(&checkpoint_list_[11]);
  checkpoint_list_[11].addNeighbor(&checkpoint_list_[12]);
  checkpoint_list_[11].addNeighbor(&checkpoint_list_[10]);
  checkpoint_list_[12].addNeighbor(&checkpoint_list_[11]);
  checkpoint_list_[12].addNeighbor(&checkpoint_list_[13]);
  checkpoint_list_[13].addNeighbor(&checkpoint_list_[14]);
  checkpoint_list_[13].addNeighbor(&checkpoint_list_[12]);
  checkpoint_list_[14].addNeighbor(&checkpoint_list_[13]);

  checkpoint_list_[15].addNeighbor(&checkpoint_list_[16]);
  checkpoint_list_[15].addNeighbor(&checkpoint_list_[18]);
  checkpoint_list_[16].addNeighbor(&checkpoint_list_[15]);
  checkpoint_list_[16].addNeighbor(&checkpoint_list_[17]);
  checkpoint_list_[17].addNeighbor(&checkpoint_list_[16]);
  checkpoint_list_[17].addNeighbor(&checkpoint_list_[18]);
  checkpoint_list_[18].addNeighbor(&checkpoint_list_[17]);
  checkpoint_list_[18].addNeighbor(&checkpoint_list_[15]);

  checkpoint_list_[19].addNeighbor(&checkpoint_list_[20]);
  checkpoint_list_[20].addNeighbor(&checkpoint_list_[21]);
  checkpoint_list_[21].addNeighbor(&checkpoint_list_[22]);
  checkpoint_list_[22].addNeighbor(&checkpoint_list_[21]);
  checkpoint_list_[21].addNeighbor(&checkpoint_list_[20]);
  checkpoint_list_[20].addNeighbor(&checkpoint_list_[19]);

  checkpoint_list_[23].addNeighbor(&checkpoint_list_[24]);
  checkpoint_list_[24].addNeighbor(&checkpoint_list_[25]);
  checkpoint_list_[25].addNeighbor(&checkpoint_list_[26]);
  checkpoint_list_[26].addNeighbor(&checkpoint_list_[27]);
  checkpoint_list_[27].addNeighbor(&checkpoint_list_[26]);
  checkpoint_list_[26].addNeighbor(&checkpoint_list_[25]);
  checkpoint_list_[25].addNeighbor(&checkpoint_list_[24]);
  checkpoint_list_[24].addNeighbor(&checkpoint_list_[23]);
}

void Game::initAgents() {
  // Prisioneros.
  for (int i = 0; i < kNumPrisoners; i++) {
    if (i < kNumPrisoners / 2) {
      // La mitad trabajando y la mitad descansando.
      prisoner_[i].position_ = JMATH::GetRandomPointInsideArea(min_working_area_, max_working_area_);
    }
    else {
      prisoner_[i].position_ = JMATH::GetRandomPointInsideArea(min_resting_area_, max_resting_area_);
    }
  }
  // Soldiers.
  for (int i = 0; i < kNumSoldiers; i++) {
    soldier_[i].position_ = JMATH::GetRandomPointInsideArea(min_base_B_area_, max_base_B_area_);
  }
  // Guards.
  for (int i = 0; i < kNumGuards; i++) {
    if (i < kNumGuards / 2) {
      // Para que salgan en las rutas de los soldados por dentro de la fortaleza.
      guard_[i].current_checkpoint_ = &checkpoint_list_[rand() % 14];
    }
    else {
      guard_[i].current_checkpoint_ = &checkpoint_list_[rand() % (kNumCheckPoints - 14) + 13];
    }
  }
}

void Game::drawAgents() {
  for (int i = 0; i < kNumPrisoners; i++) {
    prisoner_[i].draw();
  }
  for (int i = 0; i < kNumSoldiers; i++) {
    soldier_[i].draw();
  }
  for (int i = 0; i < kNumGuards; i++) {
    guard_[i].draw();
    guard_[i].drawCollisionRadius();
  }
}

void Game::updateAgents(const double delta_time) {
  for (int i = 0; i < kNumPrisoners; i++) {
    prisoner_[i].update(delta_time);
  }
  for (int i = 0; i < kNumSoldiers; i++) {
    soldier_[i].update(delta_time);
  }
  for (int i = 0; i < kNumGuards; i++) {
    guard_[i].update(delta_time);
  }
}



void Game::openDoorA(const bool open) {
  is_door_A_opened_ = open;
  cost_map_.openDoorA(open);
}

void Game::openDoorB(const bool open) {
  is_door_B_opened_ = open;
  cost_map_.openDoorB(open);
}


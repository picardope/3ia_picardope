/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "astar.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <vector>
#include "ESAT_SDK/time.h"
#include "gamemanager.h"


/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

AStar::AStar() {}
AStar::~AStar() {}


bool AStar::generatePath(const JMATH::Vec2 origin_position,
                         const JMATH::Vec2 destination_position,
                         CostMap* map,
                         Path* path) {

  printf("\n Calculando algoritmo A* ...\n");
  double start_time = ESAT::Time();
  float cost_multiplier = 1.0f;

  auto& game = Game::instance();

  std::vector<CellInfo> closed_list;
  std::vector<CellInfo> open_list;
  CellInfo current;
  CellInfo neighbor_list[8]; // To optimize, we use this not to waste memory alloc time.

  int horizontal_g_cost = (int)(game.cost_map_.cell_size_.x * cost_multiplier);
  int vertical_g_cost = (int)(game.cost_map_.cell_size_.y * cost_multiplier);
  int diagonal_g_cost = (int)(JMATH::Vec2Magnitude({ game.cost_map_.cell_size_.y,
                                                     game.cost_map_.cell_size_.y }));

  // Temp vars to use as values and indices and bool.
  int value, index, id, i, j; value = id = index = i = j = 0;
  bool found_in_open_list = false;
  bool found_in_closed_list = false;

  // 1 Create a node containing the goal state: node_goal
  CellInfo goal = map->cost_map_vector_[map->getMapVectorIndexByPosition(destination_position)];
  
  // 2 Create a node containing the start state: node_start
  CellInfo origin = map->cost_map_vector_[map->getMapVectorIndexByPosition(origin_position)];

  // First of all we will check if origin and goal are walkable nodes.
  if (!goal.is_walkable || !origin.is_walkable) {
    printf(" ERROR: Origin or destination are not valid nodes.\n");
    return false;
  }


  // 3 Put node_start on the OPEN list
  open_list.push_back(origin);

  // 4 while the OPEN list is not empty
  while (!open_list.empty()) {

    // 5 Get the node OFF the OPEN list with the lowest f and call it node_current
    value = 999999999;
    index = -1;
    for (i = open_list.size() - 1; i >= 0; --i) {
      if (open_list[i].f < value) {
        current = open_list[i];
        index = i;
        value = open_list[i].f;
      }
    }
    open_list.erase(open_list.begin() + index);

    // 6 If node_current is the same state as node_goal, break from the while loop
    if (current.position == goal.position) { break; }

    // 7 Generate each state node_successor that can come after node_current
    value = getUsefulNodeSuccessors(&current, map, neighbor_list);

    // 8 Set the cost of node_successor to be the cost of node_current plus
    //   the cost to get to node_successor from node_current
    for (i = 0; i < value; i++) {
      switch (neighbor_list[i].g) {
        case 1: { neighbor_list[i].g = current.g + horizontal_g_cost; } break;
        case 2: { neighbor_list[i].g = current.g + vertical_g_cost; } break;
        case 3: { neighbor_list[i].g = current.g + diagonal_g_cost; } break;
        default: printf(" ERROOOOOOOOOOOOOOR");
      }

      // 9.A Find node_successor on the OPEN list
      found_in_open_list = false;
      index = 0;
      for (j = open_list.size() - 1; j >= 0; --j) {
        if (open_list[j].position == neighbor_list[i].position) {
          index = j;
          found_in_open_list = true;
          break;
        }
      }

      // 9.B If node_successor is on the OPEN list but the existing one is as good or
      //     better then discard this successor and continue with next successor
      if (found_in_open_list && open_list[index].g <= neighbor_list[i].g) {
        continue;
      }

      // 10.A Find node_successor on the CLOSED list
      found_in_closed_list = false;
      id = 0;
      for (j = closed_list.size() - 1; j >= 0; --j) {
        if (closed_list[j].position == neighbor_list[i].position) {
          id = j;
          found_in_closed_list = true;
          break;
        }
      }

      // 10.B If node_successor is on the CLOSED list but the existing one is as good or
      //      better then discard this successor and continue with next successor
      if (found_in_closed_list && closed_list[id].g <= neighbor_list[i].g) {
        continue;
      }

      // 11 Remove occurences of node_successor from OPEN and CLOSED
      if (found_in_open_list) {
        open_list.erase(open_list.begin() + index);
      }
      if (found_in_closed_list) {
        closed_list.erase(closed_list.begin() + id);
      }

      // 12 Set the parent of node_successor to node_current
      id = map->getMapVectorIndexByPosition(current.position);
      neighbor_list[i].parent = &map->cost_map_vector_[id];

      // 13 Set h to be the estimated distance to node_goal (heuristic function)
      neighbor_list[i].h = (int)((JMATH::FAbs(goal.position.x - neighbor_list[i].position.x) * cost_multiplier) +
        (JMATH::FAbs(goal.position.y - neighbor_list[i].position.y) * cost_multiplier));

      neighbor_list[i].f = neighbor_list[i].g + neighbor_list[i].h;

      // 14 Add node_successor to the OPEN list
      open_list.push_back(neighbor_list[i]);
    }

    // 15 Add node_current to the CLOSED list
    closed_list.push_back(current);
  }

  ////  END OF THE AStar ALGORYTHM  //// 
  //printf(" A* algorythm time cost: %f ms.\n", ESAT::Time() - start_time);

  // Now we are going to save the path.
  if (current.position == goal.position && !open_list.empty()) {
    // We take off the node in the CLOSED list to the path.
    for (j = closed_list.size() - 1; j >= 0; --j) {
      if (closed_list[j].position == current.position) {
        path->addPoint(current.position);
        closed_list.erase(closed_list.begin() + j);
        break;
      }
    }

    while (current.position != origin.position) {
      // We take off the parent node in the CLOSED list to the path.
      for (j = closed_list.size() - 1; j >= 0; --j) {
        if (closed_list[j].position == current.parent->position) {
          current = closed_list[j];
          path->addPoint(current.position);
          closed_list.erase(closed_list.begin() + j);
          break;
        }
      }
    }
  }
  else {
    printf(" ERROR: No path found.\n");
    return false;
  }

  /*
  printf("\n Por tareas:\n");
  printf(" 5-6: %f ms.\n", total_time[0]);
  printf(" 7: %f ms.\n", total_time[1]);
  printf(" 9: %f ms.\n", total_time[3]);
  printf(" 10: %f ms.\n", total_time[4]);
  printf(" 11 - 12: %f ms.\n", total_time[5]);
  printf(" 13 - 14: %f ms.\n", total_time[2]);
  */
  return true;
}

/* 
* Only useful neighbors will be saved in the list. For Example:
* if total = 3,  only neighbor_list[0, 1 and 2] will be saved.
* to precalculate g costs. G will be set with 3 values depending on their cost:
* 1 - Horizontal cost.    2 - Vertical cost.   3 - Diagonal cost.
*/
int AStar::getUsefulNodeSuccessors(CellInfo* node,
                                   CostMap* map, 
                                   CellInfo neighbor_list[8]) {
  int total = 0;
  JMATH::Vec2 position = { 0.0f, 0.0f };
  auto& game = Game::instance();

  // RIGHT or X+
  position = { node->position.x + map->cell_size_.x, node->position.y };
  if (position.x < game.window_width) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 1; // Horizontal cost
      total += 1;
    }
  }

  // LEFT or X-
  position = { node->position.x - map->cell_size_.x, node->position.y };
  if (position.x > 0.0f) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 1; // Horizontal cost
      total += 1;
    }
  }

  // TOP or Y-
  position = { node->position.x, node->position.y - map->cell_size_.y };
  if (position.y > 0.0f) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 2; // Vertical cost
      total += 1;
    }
  }

  // BOT or Y+
  position = { node->position.x, node->position.y + map->cell_size_.y };
  if (position.y < game.window_height) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 2; // Vertical cost
      total += 1;
    }
  }
  
  // TOP-LEFT or X- Y-
  position = { node->position.x - map->cell_size_.x, node->position.y - map->cell_size_.y };
  if (position.x > 0.0f && position.y > 0.0f) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 3; // Diagonal cost
      total += 1;
    }
  }

  // BOT-LEFT or X- Y+
  position = { node->position.x - map->cell_size_.x, node->position.y + map->cell_size_.y };
  if (position.x > 0.0f && position.y < game.window_height) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 3; // Diagonal cost
      total += 1;
    }
  }

  // BOT-RIGHT or X+ Y+
  position = { node->position.x + map->cell_size_.x, node->position.y + map->cell_size_.y };
  if (position.x < game.window_width && position.y < game.window_height) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 3; // Diagonal cost
      total += 1;
    }
  }

  // TOP-RIGHT or X+ Y-
  position = { node->position.x + map->cell_size_.x, node->position.y - map->cell_size_.y };
  if (position.x < game.window_width && position.y > 0.0f) {
    neighbor_list[total] = map->cost_map_vector_[map->getMapVectorIndexByPosition(position)];
    if (neighbor_list[total].is_walkable) {
      neighbor_list[total].g = 3; // Diagonal cost
      total += 1;
    }
  }
  

  return total;
}


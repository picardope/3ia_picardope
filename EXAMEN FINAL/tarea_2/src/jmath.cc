/**
*
* @project JMATH Library
* @file jmath.cc
* @brief Mathematic 2D and 3D library.
* @author Julio Marcelo Picardo <picardope@esat-alumni.com>
* @date 28/11/2015
*
*/

#include "jmath.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ESAT_SDK/draw.h>

namespace JMATH {

/*
*/
/***************************************************************************
********************      COMPARISON OPERATORS      ************************
***************************************************************************/

  bool operator==(const Vec2& vector_1, const Vec2& vector_2) {
    if(vector_1.x == vector_2.x &&
       vector_1.y == vector_2.y){
      return true;
    }
    return false;
  }

  bool operator!=(const Vec2& vector_1, const Vec2& vector_2) {
    if(vector_1.x != vector_2.x ||
       vector_1.y != vector_2.y){
      return true;
    }
    return false;
  }

  bool operator==(const Vec3& vector_1, const Vec3& vector_2) {
    if(vector_1.x == vector_2.x &&
       vector_1.y == vector_2.y &&
       vector_1.z == vector_2.z){
      return true;
    }
    return false;
  }

  bool operator!=(const Vec3& vector_1, const Vec3& vector_2) {
    if(vector_1.x != vector_2.x ||
       vector_1.y != vector_2.y ||
       vector_1.z != vector_2.z){
      return true;
    }
    return false;
  }

  bool operator==(const Vec4& vector_1, const Vec4& vector_2) {
    if(vector_1.x == vector_2.x &&
       vector_1.y == vector_2.y &&
       vector_1.z == vector_2.z &&
       vector_1.w == vector_2.w){
      return true;
    }
    return false;
  }

  bool operator!=(const Vec4& vector_1, const Vec4& vector_2) {
    if(vector_1.x != vector_2.x ||
       vector_1.y != vector_2.y ||
       vector_1.z != vector_2.z ||
       vector_1.w != vector_2.w){
      return true;
    }
    return false;
  }

  bool operator==(const Mat2& matrix_1, const Mat2& matrix_2) {

    for (unsigned short int i = 0; i < 4; i++){
      if (matrix_1.e[i] != matrix_2.e[i]){ return false; }
    }
    return true;
  }

  bool operator!=(const Mat2& matrix_1, const Mat2& matrix_2) {

    for (unsigned short int i = 0; i < 4; i++){
      if (matrix_1.e[i] != matrix_2.e[i]){ return true; }
    }
    return false;
  }

  bool operator==(const Mat3& matrix_1, const Mat3& matrix_2) {

    for (unsigned short int i = 0; i < 9; i++){
      if (matrix_1.e[i] != matrix_2.e[i]){ return false; }
    }
    return true;
  }

  bool operator!=(const Mat3& matrix_1, const Mat3& matrix_2) {

    for (unsigned short int i = 0; i < 9; i++){
      if (matrix_1.e[i] != matrix_2.e[i]){ return true; }
    }
    return false;
  }

  bool operator==(const Mat4& matrix_1, const Mat4& matrix_2) {

    for (unsigned short int i = 0; i < 16; i++){
      if (matrix_1.e[i] != matrix_2.e[i]){ return false; }
    }
    return true;
  }

  bool operator!=(const Mat4& matrix_1, const Mat4& matrix_2) {

    for (unsigned short int i = 0; i < 16; i++){
      if (matrix_1.e[i] != matrix_2.e[i]){ return true; }
    }
    return false;
  }


/***************************************************************************
***********************          MATH UTILS         ************************
***************************************************************************/

  double SqrtASM(double number) {
    if (number < 0.0){
      printf("\n ERROR, negative number.\n");
      return 0.0;
    }
    // Assembly instruction of the micro.
    else {
      __asm{
        fld number
        fsqrt
      }
    }
  }


  float Power(const float base, const int exponent) {

    float result = 1.0f;

    if (exponent > 0){
      for (int i = 0; i < exponent; i++){
        result *= base;
      }
    }
    else{
      for (int i = 0; i > exponent; i--){
        result /= base;
      }
    }

    return result;
  }

  unsigned int Factorial(const unsigned int number) {

    int result = 1;

    for (unsigned int i = 2; i <= number; i++){
      result *= i;
    }

    return result;
  }


  double DAbs(double number) {
    if (number < 0.0) { return -number; }
    return number;
  }

  float FAbs(float number) {
    if (number < 0.0f) { return -number; }
    return number;
  }

  int Abs(int number) {
    if (number < 0) { return -number; }
    return number;
  }

  /*******************************************
  ********       ANGLE CONVERSOR       *******
  *******************************************/

  float GetRadians(const float degrees) {
    return (degrees * N_Pi) / 180;
  }

  float GetDegrees(const float radians) {
    return (radians * 180) / N_Pi;
  }

  /*******************************************
  ********        TRIGONOMETRY         *******
  *******************************************/

  double Sin(const double radians, unsigned short int accuracy) {

    unsigned short int i,j;
    double nominator, denominator, sign, sine = 0.0;
    double rad = radians;

    // The angle value range is converted to 0-360 degrees or 0-2PI radians.
    if (rad <= 0.0){
      rad *= -1;
    }
    while (rad >= N_2Pi){
      rad -= N_2Pi;
    }
    // between 270 and 360 degrees we will need more accuracy.
    // Thats because at this range, the results aren accurate enough.
    if (rad > N_Pi){accuracy += 2;}

    for(i = 0 ; i <= accuracy; i++){
      nominator  = 1;
      for (j = 0; j < 2 * i + 1; j++){
        nominator = nominator * rad;
      }
      denominator = 1;
      for (j = 1; j <= 2 * i + 1; j++){
        denominator = denominator * j;
      }
      if (i % 2 == 0){
        sign = 1;
      }
      else{
        sign = -1;
      }
      sine = sine + (nominator / denominator) * sign;
    }
    return sine;
  }

  double Cos(const double radians) {

    double rad = radians;
    double result = 0.0;

    if (rad <= 0.0){
      rad *= -1;
    }
    while (rad >= N_2Pi){
      rad -= N_2Pi;
    }

    result = SqrtASM(1.0 - Power(Sin(rad), 2));

    if (rad >= N_Pi2 && rad <= N_Pi2 * 3){
      result *= -1;
    }

    return result;
  }

  double Tan(const double radians) {
    return (Sin(radians) / Cos(radians));
  }

  double Cosec(const double radians) {
    return (1 / Sin(radians));
  }

  double Sec(const double radians) {
    return (1 / Cos(radians));
  }

  double Cotan(const double radians) {
    return (Cos(radians) / Sin(radians));
  }

/***************************************************************************
***********************          2D METHODS         ************************
***************************************************************************/

/*******************************************
********           VECTOR2           *******
*******************************************/
/*****************************
*****        INITS        ****
*****************************/

  Vec2 Vec2InitByComponents(float comp_x, float comp_y) {

    Vec2 vector;

    vector.x = comp_x;
    vector.y = comp_y;

    return vector;
  }

  Vec2 Vec2InitFrom2Points(const Vec2& origin, const Vec2& destiny) {

    Vec2 vector;

    vector = destiny - origin;

    return vector;
  }

/*****************************
*****      OPERATIONS     ****
*****************************/

  Vec2 Vec2Addition(const Vec2& vector_1, const Vec2& vector_2) {

    Vec2 addition;

    addition = Vec2InitByComponents(vector_1.x + vector_2.x,
                                    vector_1.y + vector_2.y);

    return addition;
  }

  Vec2 operator+(const Vec2& vector_1, const Vec2& vector_2) {

  	Vec2 addition;

  	addition = Vec2InitByComponents(vector_1.x + vector_2.x,
                                    vector_1.y + vector_2.y);

  	return addition;
  }

  void operator+= (Vec2& vector_1, const Vec2& vector_2) {

    vector_1.x += vector_2.x;
    vector_1.y += vector_2.y;
  }

  Vec2 Vec2Substract(const Vec2& vector_1, const Vec2& vector_2) {

  	Vec2 result;

  	result = Vec2InitByComponents(vector_1.x - vector_2.x,
                                  vector_1.y - vector_2.y);

  	return result;
  }

  Vec2 operator-(const Vec2& vector_1, const Vec2& vector_2) {

    Vec2 addition;

    addition = Vec2InitByComponents(vector_1.x - vector_2.x,
                                    vector_1.y - vector_2.y);

    return addition;
  }

  void operator-= (Vec2& vector_1, const Vec2& vector_2) {

    vector_1.x -= vector_2.x;
    vector_1.y -= vector_2.y;
  }

  Vec2 operator-(const Vec2& vector) {

    Vec2 opposite;

    opposite = vector * -1.0f;

    return opposite;
  }

  Vec2 Vec2MultiplyByScalar(const Vec2& vector, const float scalar) {

    Vec2 result;

    result = Vec2InitByComponents(vector.x * scalar,
                                  vector.y * scalar);

    return result;
  }

  Vec2 operator*(const Vec2& vector, const float scalar) {

    Vec2 result;

    result = Vec2InitByComponents(vector.x * scalar,
                                  vector.y * scalar);

    return result;
  }

  Vec2 operator*(const float scalar, const Vec2& vector) {

    Vec2 result;

    result = Vec2InitByComponents(vector.x * scalar,
                                  vector.y * scalar);

    return result;
  }

  void operator*=(Vec2& vector, const float scalar) {

    vector.x *= scalar;
    vector.y *= scalar;
  }

  Vec2 operator/(const float scalar, const Vec2& vector) {

    Vec2 result;

    result = Vec2InitByComponents(scalar / vector.x,
                                  scalar / vector.y);

    return result;
  }

  Vec2 operator/(const Vec2& vector, const float scalar) {

    Vec2 result;

    result = Vec2InitByComponents(vector.x / scalar,
                                  vector.y / scalar);

    return result;
  }

  void operator/=(Vec2& vector, const float scalar) {

    vector.x /= scalar;
    vector.y /= scalar;
  }

  Vec2 Vec2Perpendicular(const Vec2& vector) {

    Vec2 perpendicular;

    perpendicular = Vec2InitByComponents(-vector.y, vector.x);

    return perpendicular;
  }

  Vec2 Vec2MidPoint(const Vec2& point_1, const Vec2& point_2) {

    Vec2 mid_point;

    mid_point = point_1 + point_2;
    mid_point /= 2;

    return mid_point;
  }

  float Vec2Magnitude(const Vec2& vector) {

    float magnitude;

    magnitude = pow(vector.x, 2) + pow(vector.y, 2);
    magnitude = sqrtf(magnitude);

    return magnitude;
  }

  Vec2 Vec2Normalize(const Vec2& vector) {

    Vec2 normalized;
    float inverse_magnitude = 1.0f / Vec2Magnitude(vector);

    normalized = vector * inverse_magnitude;

    return normalized;
  }

  float Vec2DotProduct(const Vec2& vector_1, const Vec2& vector_2) {

    float result;

    result = (vector_1.x * vector_2.x) + (vector_1.y * vector_2.y);

    return result;
  }

  float operator*(const Vec2& vector_1, const Vec2& vector_2) {

    float result;

    result = (vector_1.x * vector_2.x) + (vector_1.y * vector_2.y);

    return result;
  }


  float Vec2Angle(const Vec2& vector_1, const Vec2& vector_2){

  	float radians;

  	radians = (vector_1 * vector_2) /
              (Vec2Magnitude(vector_1) * Vec2Magnitude(vector_2) );
  	radians = acos(radians);

  	return radians;
  }

  float Vec2Projection(const Vec2& vector_1, const Vec2& vector_2){

  	float projection;

  	projection = (vector_1 * vector_2) / Vec2Magnitude(vector_2);

  	return projection;
  }

/*******************************************
********         2x2 MATRICES        *******
*******************************************/

  float& Mat2::operator[](const int position){
    return e[position];
  }


/*****************************
*****        INITS        ****
*****************************/

  Mat2 Mat2InitByColumns(const Vec2& column_1, const Vec2& column_2) {

    Mat2 matrix = { column_1.x, column_1.y, column_2.x, column_2.y };

    return matrix;
  }

  Mat2 Mat2InitByElements(const float element_0, const float element_1,
                          const float element_2, const float element_3) {

    Mat2 matrix = { element_0, element_1, element_2, element_3 };

    return matrix;
  }


  Mat2 Mat2Identity() {
    Mat2 identity = { 1.0f, 0.0f,
                      0.0f, 1.0f };
    return identity;
  }

/*****************************
*****      OPERATIONS     ****
*****************************/

  Mat2 Mat2MultiplyByScalar(const Mat2& matrix, const float scalar) {

    Mat2 result;

    for (unsigned short int i = 0; i < 4; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return matrix;
  }

  Mat2 operator*(const Mat2& matrix, const float scalar) {

    Mat2 result;

    for (unsigned short int i = 0; i < 4; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return matrix;
  }

  Mat2 operator*(const float scalar, const Mat2& matrix) {

    Mat2 result;

    for (unsigned short int i = 0; i < 4; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return result;
  }

  void operator*=(Mat2& matrix, const float scalar) {

    for (unsigned short int i = 0; i < 4; i++){
      matrix.e[i] *= scalar;
    }
  }

  float Mat2Determinant(const Mat2& matrix) {

    float determinant = 0.0f;

    determinant = matrix.e[0] * matrix.e[3] - matrix.e[1] * matrix.e[2];

    return determinant;
  }

  float Mat2ElementCofactor(const Mat2& matrix,
                            const unsigned short int row,
                            const unsigned short int col) {

    if (row == 0 && col == 0){ return matrix.e[3]; }
  	if (row == 0 && col == 1){ return -matrix.e[1]; }
  	if (row == 1 && col == 1){ return matrix.e[0]; }
  	if (row == 1 && col == 0){ return -matrix.e[2]; }
    else {
      printf("\n WRONG COLUMN OR ROW... FUNCTION RETURNS 0");
      return 0.0f;
    }
  }

  Mat2 Mat2Cofactors(const Mat2& matrix) {

    Mat2 cofactors;
    cofactors.e[0] = Mat2ElementCofactor(matrix, 0, 0);
  	cofactors.e[1] = Mat2ElementCofactor(matrix, 0, 1);
  	cofactors.e[2] = Mat2ElementCofactor(matrix, 1, 0);
  	cofactors.e[3] = Mat2ElementCofactor(matrix, 1, 1);
    return cofactors;
  }

  Mat2 Mat2Transpose(const Mat2& matrix) {

    Mat2 transpose;

    transpose.e[0] = matrix.e[0];
    transpose.e[1] = matrix.e[2];
    transpose.e[2] = matrix.e[1];
    transpose.e[3] = matrix.e[3];

    return transpose;
  }

  Mat2 Mat2Adjoint(const Mat2& matrix) {

    Mat2 adjoint;

    adjoint = Mat2Cofactors(matrix);
    adjoint = Mat2Transpose(adjoint);

    return adjoint;
  }

  Mat2 Mat2Inverse(const Mat2& matrix) {

    Mat2 inverse;
    float det = Mat2Determinant(matrix);

    //ERROR CHECKING
    if ((int)det == 0){
      printf("\n\n Matrix determinant is 0 so it has no inverse\n\n");
      return matrix;
    }

    inverse = Mat2Adjoint(matrix);
    inverse = Mat2MultiplyByScalar(inverse, 1/det);

    return inverse;
  }

  Mat2 Mat2Addition(const Mat2& matrix_1, const Mat2& matrix_2) {

    Mat2 addition;

    for(unsigned short int i = 0; i < 4; i++){
      addition.e[i] = matrix_1.e[i] + matrix_2.e[i];
    }

    return addition;
  }

  Mat2 operator+(const Mat2& matrix_1, const Mat2& matrix_2) {

    Mat2 addition;

    for(unsigned short int i = 0; i < 4; i++){
      addition.e[i] = matrix_1.e[i] + matrix_2.e[i];
    }

    return addition;
  }

  void operator+= (Mat2& matrix_1, const Mat2& matrix_2) {

    for(unsigned short int i = 0; i < 4; i++){
      matrix_1.e[i] += matrix_2.e[i];
    }
  }

  Mat2 operator-(const Mat2& matrix_1, const Mat2& matrix_2) {

    Mat2 addition;

    for(unsigned short int i = 0; i < 4; i++){
      addition.e[i] = matrix_1.e[i] - matrix_2.e[i];
    }

    return addition;
  }

  void operator-= (Mat2& matrix_1, const Mat2& matrix_2) {

    for(unsigned short int i = 0; i < 4; i++){
      matrix_1.e[i] -= matrix_2.e[i];
    }
  }




/*******************************************
********           VECTOR3           *******
*******************************************/

/*****************************
*****        INITS        ****
*****************************/

  Vec3 Vec3InitByComponents(float comp_x, float comp_y, float comp_z) {

    Vec3 vector;

    vector.x = comp_x;
    vector.y = comp_y;
    vector.z = comp_z;

    return vector;
  }

  Vec3 Vec3InitFrom2Points(const Vec3& origin, const Vec3& destiny) {

    Vec3 vector;

    vector = destiny - origin;

    return vector;
  }



/*****************************
*****      OPERATIONS     ****
*****************************/

  Vec3 Vec3Addition(const Vec3& vector_1, const Vec3& vector_2) {

    Vec3 addition;

    addition = Vec3InitByComponents(vector_1.x + vector_2.x,
                                    vector_1.y + vector_2.y,
                                    vector_1.z + vector_2.z);

    return addition;
  }

  Vec3 operator+(const Vec3& vector_1, const Vec3& vector_2) {

  	Vec3 addition;

  	addition = Vec3InitByComponents(vector_1.x + vector_2.x,
                                    vector_1.y + vector_2.y,
                                    vector_1.z + vector_2.z);

  	return addition;
  }

  void operator+= (Vec3& vector_1, const Vec3& vector_2) {

    vector_1.x += vector_2.x;
    vector_1.y += vector_2.y;
    vector_1.z += vector_2.z;
  }

  Vec3 Vec3Substract(const Vec3& vector_1, const Vec3& vector_2) {

  	Vec3 result;

  	result = Vec3InitByComponents(vector_1.x - vector_2.x,
                                  vector_1.y - vector_2.y,
                                  vector_1.z - vector_2.z);

  	return result;
  }

  Vec3 operator-(const Vec3& vector_1, const Vec3& vector_2) {

    Vec3 addition;

    addition = Vec3InitByComponents(vector_1.x - vector_2.x,
                                    vector_1.y - vector_2.y,
                                    vector_1.z - vector_2.z);

    return addition;
  }

  void operator-= (Vec3& vector_1, const Vec3& vector_2) {

    vector_1.x -= vector_2.x;
    vector_1.y -= vector_2.y;
    vector_1.z -= vector_2.z;
  }

  Vec3 operator-(const Vec3& vector) {

    Vec3 opposite;

    opposite = vector * -1.0f;

    return opposite;
  }

  Vec3 Vec3MultiplyByScalar(const Vec3& vector, const float scalar) {

    Vec3 result;

    result = Vec3InitByComponents(vector.x * scalar,
                                  vector.y * scalar,
                                  vector.z * scalar);

    return result;
  }

  Vec3 operator*(const Vec3& vector, const float scalar) {

    Vec3 result;

    result = Vec3InitByComponents(vector.x * scalar,
                                  vector.y * scalar,
                                  vector.z * scalar);

    return result;
  }

  Vec3 operator*(const float scalar, const Vec3& vector) {

    Vec3 result;

    result = Vec3InitByComponents(vector.x * scalar,
                                  vector.y * scalar,
                                  vector.z * scalar);

    return result;
  }

  void operator*=(Vec3& vector, const float scalar) {

    vector.x *= scalar;
    vector.y *= scalar;
    vector.z *= scalar;
  }

  Vec3 operator/(const float scalar, const Vec3& vector) {

    Vec3 result;

    result = Vec3InitByComponents(scalar / vector.x,
                                  scalar / vector.y,
                                  scalar / vector.z);

    return result;
  }

  Vec3 operator/(const Vec3& vector, const float scalar) {

    Vec3 result;

    result = Vec3InitByComponents(vector.x / scalar,
                                  vector.y / scalar,
                                  vector.z / scalar);

    return result;
  }

  void operator/=(Vec3& vector, const float scalar) {

    vector.x /= scalar;
    vector.y /= scalar;
    vector.z /= scalar;
  }

  Vec3 Vec3Perpendicular(const Vec3& vector) {

    Vec3 perpendicular;

    perpendicular = Vec3InitByComponents(-vector.y, vector.x, vector.z);

    return perpendicular;
  }

  Vec3 Vec3Homogenize(const Vec3& vector) {

    Vec3 homogeneous;
    float scalar;

    if (vector.z == 0.0f){
      printf("\n\n Component z == 0, so it's not a point.");
      return vector;
    }

    scalar = 1.0f / vector.z;
    homogeneous = vector * scalar;

    return homogeneous;
  }

  Vec3 Vec3MidPoint(const Vec3& point_1, const Vec3& point_2) {

    Vec3 mid_point;

    mid_point = point_1 + point_2;
    mid_point = Vec3Homogenize(mid_point);

    return mid_point;
  }

  float Vec3Magnitude(const Vec3& vector) {

    float magnitude;

    magnitude = pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2);
    magnitude = sqrtf(magnitude);

    return magnitude;
  }

  Vec3 Vec3Normalize(const Vec3& vector) {

    Vec3 normalized;
    float inverse_magnitude = 1.0f / Vec3Magnitude(vector);

    normalized = vector * inverse_magnitude;

    return normalized;
  }

  float Vec3DotProduct(const Vec3& vector_1, const Vec3& vector_2) {

    float result;

    result = (vector_1.x * vector_2.x) +
             (vector_1.y * vector_2.y) +
             (vector_1.z * vector_2.z);

    return result;
  }

  float operator*(const Vec3& vector_1, const Vec3& vector_2) {

    float result;

    result = (vector_1.x * vector_2.x) +
             (vector_1.y * vector_2.y) +
             (vector_1.z * vector_2.z);

    return result;
  }


  float Vec3Angle(const Vec3& vector_1, const Vec3& vector_2){

  	float radians;

  	radians = (vector_1 * vector_2) /
              (Vec3Magnitude(vector_1) * Vec3Magnitude(vector_2) );
  	radians = acos(radians);

  	return radians;
  }

  float Vec3Projection(const Vec3& vector_1, const Vec3& vector_2){

  	float projection;

  	projection = (vector_1 * vector_2) / Vec3Magnitude(vector_2);

  	return projection;
  }

/*******************************************
********         3x3 MATRICES        *******
*******************************************/

  float& Mat3::operator[](const int position){
    return e[position];
  }

/*****************************
*****        INITS        ****
*****************************/

  Mat3 Mat3InitByColumns(const Vec3& column_1,
                         const Vec3& column_2,
                         const Vec3& column_3) {

    Mat3 matrix = { column_1.x, column_1.y, column_1.z,
                    column_2.x, column_2.y, column_2.z,
                    column_3.x, column_3.y, column_3.z };

    return matrix;
  }

  Mat3 Mat3InitByElements(float element_0, float element_1, float element_2,
                          float element_3, float element_4, float element_5,
                          float element_6, float element_7, float element_8) {

    Mat3 matrix = { element_0, element_1, element_2,
                    element_3, element_4, element_5,
                    element_6, element_7, element_8 };

    return matrix;
  }


  Mat3 Mat3Identity() {
    Mat3 identity = { 1.0f, 0.0f, 0.0f,
                      0.0f, 1.0f, 0.0f,
                      0.0f, 0.0f, 1.0f  };
    return identity;
  }

  Mat3 Mat3SetTranslation(const float trans_x, const float trans_y) {
    Mat3 translated = { 1.0f, 0.0f, trans_x,
                        0.0f, 1.0f, trans_y,
                        0.0f, 0.0f, 1.0f  };
    return translated;
  }

  Mat3 Mat3SetRotation(const float radians) {
    Mat3 translated = { cos(radians), sin(radians), 0.0f,
                        -(sin(radians)), cos(radians), 0.0f,
                        0.0f, 0.0f, 1.0f  };
    return translated;
  }

  Mat3 Mat3SetScale(const float scale_x, const float scale_y) {
    Mat3 scale = { scale_x, 0.0f, 0.0f,
                    0.0f, scale_y, 0.0f,
                    0.0f, 0.0f, 1.0f  };
    return scale;
  }


/*****************************
*****      OPERATIONS     ****
*****************************/

  Mat3 Mat3MultiplyByScalar(const Mat3& matrix, const float scalar) {

    Mat3 result;

    for (unsigned short int i = 0; i < 9; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return matrix;
  }

  Mat3 operator*(const Mat3& matrix, const float scalar) {

    Mat3 result;

    for (unsigned short int i = 0; i < 9; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return matrix;
  }

  Mat3 operator*(const float scalar, const Mat3& matrix) {

    Mat3 result;

    for (unsigned short int i = 0; i < 9; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return result;
  }

  void operator*=(Mat3& matrix, const float scalar) {

    for (unsigned short int i = 0; i < 9; i++){
      matrix.e[i] *= scalar;
    }
  }

  float Mat3Determinant(const Mat3& matrix) {

    float determinant = 0.0f;
    // Positive diagonals.
    determinant += matrix.e[8] * matrix.e[4] * matrix.e[0];
    determinant += matrix.e[3] * matrix.e[7] * matrix.e[2];
    determinant += matrix.e[6] * matrix.e[1] * matrix.e[5];
    // Negative diagonals.
    determinant -= matrix.e[6] * matrix.e[4] * matrix.e[2];
    determinant -= matrix.e[0] * matrix.e[7] * matrix.e[5];
    determinant -= matrix.e[3] * matrix.e[1] * matrix.e[8];

    return determinant;
  }

  float Mat3ElementCofactor(const Mat3& matrix,
                            const unsigned short int row,
                            const unsigned short int col) {

  	unsigned short int i, j, counter = 0;
    //Here we will save the data needed to calculate its determinant.
  	float aux[2][2] = { 0, 0, 0, 0 };

    //ERROR CHECKING
    if (row < 0 || row > 3 || col < 0 || col > 3){
      printf("\n\n INCORRECT ELEMENT POSITION: function will return 0.0f\n\n");
      return 0.0f;
    }

  	for (i = 0; i < 3; i++){
  		for (j = 0; j < 3; j++){
        // Just select the elements that are not in the row and column selected.
  			if (i != row && j != col){
  				switch (counter){
          // first value is saved in [0][0] and the last one in [1][1]
  				case 0:{ aux[0][0] = matrix.e[i+j*3]; counter += 1; }break;
  				case 1:{ aux[0][1] = matrix.e[i+j*3]; counter += 1; }break;
  				case 2:{ aux[1][0] = matrix.e[i+j*3]; counter += 1; }break;
  				case 3:{ aux[1][1] = matrix.e[i+j*3]; counter += 1; }break;
  				}
  			}
  		}
  	}

  	/*
  	| + - + |   if (row + column) result is an EVEN number, cofactor is positive.
  	| - + - |
  	| + - + |	  if (row + column) result is an ODD number, cofactor is negative.
  	*/
  	if ((row + col) % 2 == 0){
      return ((aux[0][0] * aux[1][1]) - (aux[0][1] * aux[1][0]));
    }
  	else{
      return ((-1) * ((aux[0][0] * aux[1][1]) - (aux[0][1] * aux[1][0])));
    }
  }

  Mat3 Mat3Cofactors(const Mat3& matrix) {

    Mat3 cofactors;
  	for (unsigned short int i = 0; i < 3; i++){
  		for (unsigned short int j = 0; j < 3; j++){
  			cofactors.e[i+j*3] = Mat3ElementCofactor(matrix, i, j);
  		}
  	}
    return cofactors;
  }

  Mat3 Mat3Transpose(const Mat3& matrix) {

    Mat3 transpose;

    transpose.e[0] = matrix.e[0];
    transpose.e[1] = matrix.e[3];
    transpose.e[2] = matrix.e[6];
    transpose.e[3] = matrix.e[1];
    transpose.e[4] = matrix.e[4];
    transpose.e[5] = matrix.e[7];
    transpose.e[6] = matrix.e[2];
    transpose.e[7] = matrix.e[5];
    transpose.e[8] = matrix.e[8];

    return transpose;
  }

  Mat3 Mat3Adjoint(const Mat3& matrix) {

    Mat3 adjoint;

    adjoint = Mat3Cofactors(matrix);
    adjoint = Mat3Transpose(adjoint);

    return adjoint;
  }

  Mat3 Mat3Inverse(const Mat3& matrix) {

    Mat3 inverse;
    float det = Mat3Determinant(matrix);

    //ERROR CHECKING
    if ((int)det == 0){
      printf("\n\n Matrix determinant is 0 so it has no inverse\n\n");
      return matrix;
    }

    inverse = Mat3Adjoint(matrix);
    inverse = Mat3MultiplyByScalar(inverse, 1/det);

    return inverse;
  }

  Mat3 Mat3Addition(const Mat3& matrix_1, const Mat3& matrix_2) {

    Mat3 addition;

    for(unsigned short int i = 0; i < 9; i++){
      addition.e[i] = matrix_1.e[i] + matrix_2.e[i];
    }

    return addition;
  }

  Mat3 operator+(const Mat3& matrix_1, const Mat3& matrix_2) {

    Mat3 addition;

    for(unsigned short int i = 0; i < 9; i++){
      addition.e[i] = matrix_1.e[i] + matrix_2.e[i];
    }

    return addition;
  }

  void operator+= (Mat3& matrix_1, const Mat3& matrix_2) {

    for(unsigned short int i = 0; i < 9; i++){
      matrix_1.e[i] += matrix_2.e[i];
    }
  }

  Mat3 operator-(const Mat3& matrix_1, const Mat3& matrix_2) {

    Mat3 addition;

    for(unsigned short int i = 0; i < 9; i++){
      addition.e[i] = matrix_1.e[i] - matrix_2.e[i];
    }

    return addition;
  }

  void operator-= (Mat3& matrix_1, const Mat3& matrix_2) {

    for(unsigned short int i = 0; i < 9; i++){
      matrix_1.e[i] -= matrix_2.e[i];
    }
  }


/*******************************************
*****   VEC3 && MAT3 TRANSFORMATIONS   *****
*******************************************/

  Vec3 Vec3Mat3Multiply(const Vec3& vector, const Mat3& matrix) {

  	Vec3 result;

  	result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z ;

    result.y = matrix.e[3] * vector.x +
               matrix.e[4] * vector.y +
               matrix.e[5] * vector.z ;

    result.z = matrix.e[6] * vector.x +
               matrix.e[7] * vector.y +
               matrix.e[8] * vector.z ;

  	return result;
  }

  Vec3 operator*(const Vec3& vector, const Mat3& matrix) {

    Vec3 result;

    result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z ;

    result.y = matrix.e[3] * vector.x +
               matrix.e[4] * vector.y +
               matrix.e[5] * vector.z ;

    result.z = matrix.e[6] * vector.x +
               matrix.e[7] * vector.y +
               matrix.e[8] * vector.z ;

    return result;
  }

  Vec3 operator*(const Mat3& matrix, const Vec3& vector) {

    Vec3 result;

    result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z ;

    result.y = matrix.e[3] * vector.x +
               matrix.e[4] * vector.y +
               matrix.e[5] * vector.z ;

    result.z = matrix.e[6] * vector.x +
               matrix.e[7] * vector.y +
               matrix.e[8] * vector.z ;

    return result;
  }

  void operator*=(Vec3& vector, const Mat3& matrix) {

    Vec3 result;

    result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z ;

    result.y = matrix.e[3] * vector.x +
               matrix.e[4] * vector.y +
               matrix.e[5] * vector.z ;

    result.z = matrix.e[6] * vector.x +
               matrix.e[7] * vector.y +
               matrix.e[8] * vector.z ;

    vector.x = result.x;
    vector.y = result.y;
    vector.z = result.z;
  }

  Mat3 Mat3Multiply(const Mat3& matrix_1, const Mat3& matrix_2) {

    Mat3 result;

    for (unsigned short int i = 0; i < 3; i++){
      for (unsigned short int j = 0; j < 3; j++){

        result.e[i * 3 + j] = matrix_1.e[j] * matrix_2.e[i * 3] +
                              matrix_1.e[3 + j] * matrix_2.e[i * 3 + 1] +
                              matrix_1.e[6 + j] * matrix_2.e[i * 3 + 2];
      }
    }

    return result;
  }

  Mat3 operator*(const Mat3& matrix_1, const Mat3& matrix_2) {

    Mat3 result;

    for (unsigned short int i = 0; i < 3; i++){
      for (unsigned short int j = 0; j < 3; j++){

        result.e[i * 3 + j] = matrix_1.e[j] * matrix_2.e[i * 3] +
                              matrix_1.e[3 + j] * matrix_2.e[i * 3 + 1] +
                              matrix_1.e[6 + j] * matrix_2.e[i * 3 + 2];
      }
    }

    return result;
  }

  void operator*= (Mat3& matrix_1, const Mat3& matrix_2) {

    Mat3 result;

    for (unsigned short int i = 0; i < 3; i++){
      for (unsigned short int j = 0; j < 3; j++){

        result.e[i * 3 + j] = matrix_1.e[j] * matrix_2.e[i * 3] +
                              matrix_1.e[3 + j] * matrix_2.e[i * 3 + 1] +
                              matrix_1.e[6 + j] * matrix_2.e[i * 3 + 2];
      }
    }

    for (unsigned short int i = 0; i < 9; i++){
      matrix_1.e[i] = result.e[i];
    }
  }

  Mat3 Mat3Rotate(const Mat3& matrix, const float radians) {

    Mat3 rotated;

    rotated = Mat3Multiply(matrix, Mat3SetRotation(radians));

    return rotated;
  }

  Mat3 Mat3Translate(const Mat3& matrix, const float trans_x, const float trans_y) {

    Mat3 translated;

    translated = Mat3Multiply(matrix, Mat3SetTranslation(trans_x, trans_y));

    return translated;
  }

  Mat3 Mat3Scale(const Mat3& matrix, const float scale_x, const float scale_y) {

    Mat3 scale;

    scale = Mat3Multiply(matrix, Mat3SetScale(scale_x, scale_y));

    return scale;
  }

/*******************************************
*****            2D FIGURES          *******
*******************************************/

  Vec3* GetRegularPolygon(const unsigned short int num_vertex,
                          const float radius) {


    float angle = N_2Pi / num_vertex;
    Vec3* vertex = (Vec3*)malloc(sizeof(Vec3) * num_vertex);

    for (unsigned short int i = 0; i < num_vertex; i++){
      vertex[i] = Vec3InitByComponents(radius * cos(i * angle),
                                       radius * sin(i * angle),
                                       1.0f);
    }

    return vertex;
  }



/***************************************************************************
***********************          3D METHODS         ************************
***************************************************************************/

/*******************************************
********           VECTOR4           *******
*******************************************/

/*****************************
*****        INITS        ****
*****************************/


  Vec4 Vec4InitByComponents(const float comp_x,
                            const float comp_y,
                            const float comp_z,
                            const float comp_w) {

    Vec4 vector;

    vector.x = comp_x;
    vector.y = comp_y;
    vector.z = comp_z;
    vector.w = comp_w;

    return vector;
  }

  Vec4 Vec4InitFrom2Points(const Vec4& origin, const Vec4& destiny) {

    Vec4 vector;

    vector = Vec4Addition(destiny, Vec4Opposite(origin));

    return vector;
  }


/*****************************
*****      OPERATIONS     ****
*****************************/

  Vec4 Vec4Addition(const Vec4& vector_1, const Vec4& vector_2) {

  	Vec4 addition;

  	addition = Vec4InitByComponents(vector_1.x + vector_2.x,
                                    vector_1.y + vector_2.y,
                                    vector_1.z + vector_2.z,
                                    vector_1.w + vector_2.w);

  	return addition;
  }

  Vec4 operator+(const Vec4& vector_1, const Vec4& vector_2) {

    Vec4 addition;

    addition = Vec4InitByComponents(vector_1.x + vector_2.x,
                                    vector_1.y + vector_2.y,
                                    vector_1.z + vector_2.z,
                                    vector_1.w + vector_2.w);

    return addition;
  }

  void operator+= (Vec4& vector_1, const Vec4& vector_2) {

    vector_1.x += vector_2.x;
    vector_1.y += vector_2.y;
    vector_1.z += vector_2.z;
    vector_1.w += vector_2.w;
  }

  Vec4 Vec4Substract(const Vec4& vector_1, const Vec4& vector_2) {

    Vec4 result;

    result = Vec4InitByComponents(vector_1.x - vector_2.x,
                                  vector_1.y - vector_2.y,
                                  vector_1.z - vector_2.z,
                                  vector_1.w - vector_2.w);

    return result;
  }

  Vec4 operator- (const Vec4& vector_1, const Vec4& vector_2) {

    Vec4 result;

    result = Vec4InitByComponents(vector_1.x - vector_2.x,
                                  vector_1.y - vector_2.y,
                                  vector_1.z - vector_2.z,
                                  vector_1.w - vector_2.w);

    return result;
  }

  void operator-= (Vec4& vector_1, const Vec4& vector_2) {

    vector_1.x -= vector_2.x;
    vector_1.y -= vector_2.y;
    vector_1.z -= vector_2.z;
    vector_1.w -= vector_2.w;
  }

  Vec4 Vec4Opposite(const Vec4& vector) {

    Vec4 opposite;

    opposite = Vec4MultiplyByScalar(vector, -1.0f);

    return opposite;
  }

  Vec4 operator- (const Vec4& vector) {

    Vec4 opposite;

    opposite = Vec4MultiplyByScalar(vector, -1.0f);

    return opposite;
  }

  Vec4 Vec4MultiplyByScalar(const Vec4& vector, const float scalar) {

    Vec4 result;

    result = Vec4InitByComponents(vector.x * scalar,
                                  vector.y * scalar,
                                  vector.z * scalar,
                                  vector.w * scalar);

    return result;
  }

  Vec4 operator* (const Vec4& vector, const float scalar) {

    Vec4 result;

    result = Vec4InitByComponents(vector.x * scalar,
                                  vector.y * scalar,
                                  vector.z * scalar,
                                  vector.w * scalar);

    return result;
  }

  Vec4 operator* (const float scalar, const Vec4& vector) {

    Vec4 result;

    result = Vec4InitByComponents(vector.x * scalar,
                                  vector.y * scalar,
                                  vector.z * scalar,
                                  vector.w * scalar);

    return result;
  }

  void operator*=(Vec4& vector, const float scalar) {

    vector.x *= scalar;
    vector.y *= scalar;
    vector.z *= scalar;
    vector.w *= scalar;
  }

  Vec4 operator/ (const Vec4& vector, const float scalar) {

    Vec4 result;

    result = Vec4InitByComponents(vector.x / scalar,
                                  vector.y / scalar,
                                  vector.z / scalar,
                                  vector.w / scalar);

    return result;
  }

  Vec4 operator/ (const float scalar, const Vec4& vector) {

    Vec4 result;

    result = Vec4InitByComponents(vector.x / scalar,
                                  vector.y / scalar,
                                  vector.z / scalar,
                                  vector.w / scalar);

    return result;
  }

  void operator/=(Vec4& vector, const float scalar) {

    vector.x /= scalar;
    vector.y /= scalar;
    vector.z /= scalar;
    vector.w /= scalar;
  }

  Vec4 Vec4Homogenize(const Vec4& vector) {

    Vec4 homogeneous;
    float scalar;

    if (vector.w == 0.0f){
      printf("\n\n Component w == 0, so it's not a point.");
      return vector;
    }

    scalar = 1.0f / vector.w;
    homogeneous = Vec4MultiplyByScalar(vector, scalar);

    return homogeneous;
  }

  Vec4 Vec4MidPoint(const Vec4& point_1, const Vec4& point_2) {

    Vec4 mid_point;

    mid_point = Vec4Addition(point_1, point_2);
    mid_point = Vec4Homogenize(mid_point);

    return mid_point;
  }

  float Vec4Magnitude(const Vec4& vector) {

    float magnitude;

    magnitude = pow(vector.x, 2) +
                pow(vector.y, 2) +
                pow(vector.z, 2) +
                pow(vector.w, 2);

    magnitude = sqrtf(magnitude);

    return magnitude;
  }

  Vec4 Vec4Normalize(const Vec4& vector) {

    Vec4 normalized;
    float inverse_magnitude = 1.0f / Vec4Magnitude(vector);

    normalized = Vec4MultiplyByScalar(vector, inverse_magnitude);

    return normalized;
  }

  float Vec4DotProduct(const Vec4& vector_1, const Vec4& vector_2) {

    float result;

    result = (vector_1.x * vector_2.x) +
             (vector_1.y * vector_2.y) +
             (vector_1.z * vector_2.z) +
             (vector_1.w * vector_2.w);

    return result;
  }

  float operator* (const Vec4& vector_1, const Vec4& vector_2) {

    float result;

    result = (vector_1.x * vector_2.x) +
             (vector_1.y * vector_2.y) +
             (vector_1.z * vector_2.z) +
             (vector_1.w * vector_2.w);

    return result;
  }

  Vec4 Vec4CrossProduct(const Vec4& vector_1, const Vec4& vector_2) {

    Vec4 normal;

    normal.x = (vector_1.y * vector_2.z) - (vector_1.z * vector_2.y);
  	normal.y = (vector_1.z * vector_2.x) - (vector_1.x * vector_2.z);
  	normal.z = (vector_1.x * vector_2.y) - (vector_1.y * vector_2.x);
    normal.w = 0.0f;

  	return normal;
  }

  float Vec4Angle(const Vec4& vector_1, const Vec4& vector_2) {

    float radians;

  	radians = Vec4DotProduct(vector_1, vector_2) /
              (Vec4Magnitude(vector_1) * Vec4Magnitude(vector_2) );
  	radians = acos(radians);

  	return radians;
  }





/*******************************************
********         4x4 MATRICES        *******
*******************************************/

  float& Mat4::operator[](const int position){
    return e[position];
  }

/*****************************
*****        INITS        ****
*****************************/

  Mat4 Mat4InitByColumns(const Vec4& column_1,
                         const Vec4& column_2,
                         const Vec4& column_3,
                         const Vec4& column_4) {

    Mat4 matrix = {column_1.x, column_1.y, column_1.z, column_1.w,
                   column_2.x, column_2.y, column_2.z, column_2.w,
                   column_3.x, column_3.y, column_3.z, column_3.w,
                   column_4.x, column_4.y, column_4.z, column_4.w };

    return matrix;
  }

  Mat4 Mat4InitByElements(const float element_0, const float element_1,
                          const float element_2, const float element_3,
                          const float element_4, const float element_5,
                          const float element_6, const float element_7,
                          const float element_8, const float element_9,
                          const float element_10, const float element_11,
                          const float element_12, const float element_13,
                          const float element_14, const float element_15) {

    Mat4 matrix = { element_0, element_1, element_2, element_3,
                    element_4, element_5, element_6, element_7,
                    element_8, element_9, element_10, element_11,
                    element_12, element_13, element_14, element_15 };

    return matrix;
  }


  Mat4 Mat4Identity() {

    Mat4 identity = { 1.0f, 0.0f, 0.0f, 0.0f,
                      0.0f, 1.0f, 0.0f, 0.0f,
                      0.0f, 0.0f, 1.0f, 0.0f,
                      0.0f, 0.0f, 0.0f, 1.0f  };

    return identity;
  }

  Mat4 Mat4SetTranslation(const float trans_x,
                          const float trans_y,
                          const float trans_z) {

    Mat4 translated = { 1.0f, 0.0f, 0.0f, trans_x,
                        0.0f, 1.0f, 0.0f, trans_y,
                        0.0f, 0.0f, 1.0f, trans_z,
                        0.0f, 0.0f, 0.0f, 1.0f };

    return translated;
  }

  Mat4 Mat4SetScale(const float scale_x,
                    const float scale_y,
                    const float scale_z) {

    Mat4 scale = { scale_x, 0.0f, 0.0f, 0.0f,
                    0.0f, scale_y, 0.0f, 0.0f,
                    0.0f, 0.0f, scale_z, 0.0f,
                    0.0f, 0.0f, 0.0f, 1.0f };
    return scale;
  }

  Mat4 Mat4SetRotationX(const float radians) {

  	Mat4 rotated = { 1.0f, 0.0f, 0.0f, 0.0f,
                     0.0f, cos(radians), sin(radians), 0.0f,
                     0.0f, -sin(radians), cos(radians), 0.0f,
                     0.0f, 0.0f, 0.0f, 1.0f };

  	return rotated;
  }

  Mat4 Mat4SetRotationY(const float radians) {

    Mat4 rotated = { cos(radians), 0.0f, -sin(radians), 0.0f,
                     0.0f, 1.0f, 0.0f, 0.0f,
                     sin(radians), 0.0f, cos(radians), 0.0f,
                     0.0f, 0.0f, 0.0f, 1.0f };

    return rotated;
  }

  Mat4 Mat4SetRotationZ(const float radians) {

    Mat4 rotated = { cos(radians), sin(radians), 0.0f, 0.0f,
                     -sin(radians), cos(radians), 0.0f, 0.0f,
                     0.0f, 0.0f, 1.0f, 0.0f,
                     0.0f, 0.0f, 0.0f, 1.0f };

  	return rotated;
  }

  Mat4 Mat4SetProjection3Dto2D() {

  	Mat4 projected = {1.0f, 0.0f, 0.0f, 0.0f,
                      0.0f, 1.0f, 0.0f, 0.0f,
                      0.0f, 0.0f, 1.0f, 0.0f,
                      0.0f, 0.0f, 1.0f, 0.0f  };

  	return projected;
  }


/*****************************
*****      OPERATIONS     ****
*****************************/

  Mat4 Mat4MultiplyByScalar(const Mat4& matrix, const float scalar) {

    Mat4 result;

    for (unsigned short int i = 0; i < 16; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return result;
  }

  Mat4 operator*(const Mat4& matrix, const float scalar) {

    Mat4 result;

    for (unsigned short int i = 0; i < 16; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return result;
  }

  Mat4 operator*(const float scalar, const Mat4& matrix) {

    Mat4 result;

    for (unsigned short int i = 0; i < 16; i++){
      result.e[i] = matrix.e[i] * scalar;
    }

    return result;
  }

  Mat4 operator/(const Mat4& matrix, const float scalar) {

    Mat4 result;

    for (unsigned short int i = 0; i < 16; i++){
      result.e[i] = matrix.e[i] / scalar;
    }

    return result;
  }

  void operator*=(Mat4& matrix, const float scalar) {

    for (unsigned short int i = 0; i < 16; i++){
      matrix.e[i] *= scalar;
    }
  }

  void operator/=(Mat4& matrix, const float scalar) {

    for (unsigned short int i = 0; i < 16; i++){
      matrix.e[i] /= scalar;
    }
  }

  float Mat4Determinant(const Mat4& matrix) {

    float determinant;

    determinant = matrix.e[0] * Mat4ElementCofactor(matrix, 0, 0) +
    		          matrix.e[1] * Mat4ElementCofactor(matrix, 1, 0) +
                  matrix.e[2] * Mat4ElementCofactor(matrix, 2, 0) +
                  matrix.e[3] * Mat4ElementCofactor(matrix, 3, 0);

    return determinant;
  }

  float Mat4ElementCofactor(const Mat4& matrix,
                            const unsigned short int row,
                            const unsigned short int col) {

  	unsigned short int i, j, counter = 0;
    //Here we will save the data needed to calculate its determinant.
  	Mat3 aux = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    //ERROR CHECKING
    if (row < 0 || row > 4 || col < 0 || col > 4){
      printf("\n\n INCORRECT ELEMENT POSITION: function will return 0.0f\n\n");
      return 0.0f;
    }

  	for (i = 0; i < 4; i++){
  		for (j = 0; j < 4; j++){
        // Just select the elements that are not in the row and column selected.
  			if (i != row && j != col){
  				switch (counter){
          // values will be saved by rows, so we need to adapt the matrix
  				case 0:{ aux.e[0] = matrix.e[i+j*4]; counter += 1; }break;
  				case 1:{ aux.e[3] = matrix.e[i+j*4]; counter += 1; }break;
  				case 2:{ aux.e[6] = matrix.e[i+j*4]; counter += 1; }break;
  				case 3:{ aux.e[1] = matrix.e[i+j*4]; counter += 1; }break;
          case 4:{ aux.e[4] = matrix.e[i+j*4]; counter += 1; }break;
  				case 5:{ aux.e[7] = matrix.e[i+j*4]; counter += 1; }break;
  				case 6:{ aux.e[2] = matrix.e[i+j*4]; counter += 1; }break;
  				case 7:{ aux.e[5] = matrix.e[i+j*4]; counter += 1; }break;
          case 8:{ aux.e[8] = matrix.e[i+j*4]; counter += 1; }break;
  				}
  			}
  		}
  	}

  	/*
  	| + - + - |  if (row + column) result is an EVEN number, cofactor is positive.
  	| - + - + |
  	| + - + - |	 if (row + column) result is an ODD number, cofactor is negative.
  	| - + - + |
    */

  	if ((row + col) % 2 == 0){ return Mat3Determinant(aux); }
  	else{ return -Mat3Determinant(aux); }
  }

  Mat4 Mat4Cofactors(const Mat4& matrix){
    Mat4 cofactors;
    for (unsigned short int i = 0; i < 4; i++){
      for (unsigned short int j = 0; j < 4; j++){
        cofactors.e[i+j*4] = Mat4ElementCofactor(matrix, i, j);
      }
    }
    return cofactors;
  }

  Mat4 Mat4Transpose(const Mat4& matrix) {

    Mat4 transpose;

    transpose.e[0] = matrix.e[0];
    transpose.e[1] = matrix.e[4];
    transpose.e[2] = matrix.e[8];
    transpose.e[3] = matrix.e[12];
    transpose.e[4] = matrix.e[1];
    transpose.e[5] = matrix.e[5];
    transpose.e[6] = matrix.e[9];
    transpose.e[7] = matrix.e[13];
    transpose.e[8] = matrix.e[2];
    transpose.e[9] = matrix.e[6];
    transpose.e[10] = matrix.e[10];
    transpose.e[11] = matrix.e[14];
    transpose.e[12] = matrix.e[3];
    transpose.e[13] = matrix.e[7];
    transpose.e[14] = matrix.e[11];
    transpose.e[15] = matrix.e[15];


    return transpose;
  }

  Mat4 Mat4Adjoint(const Mat4& matrix) {

    Mat4 adjoint;

    adjoint = Mat4Cofactors(matrix);
    adjoint = Mat4Transpose(adjoint);

    return adjoint;
  }

  Mat4 Mat4Inverse(const Mat4& matrix) {

    Mat4 inverse;
    float det = Mat4Determinant(matrix);

    //ERROR CHECKING
    if ((int)det == 0){
      printf("\n\n Matrix determinant is 0 so it has no inverse\n\n");
      return matrix;
    }

    inverse = Mat4Adjoint(matrix);
    inverse = Mat4MultiplyByScalar(inverse, 1/det);

    return inverse;
  }

  Mat4 Mat4Addition(const Mat4& matrix_1, const Mat4& matrix_2) {

    Mat4 addition;

    for(unsigned short int i = 0; i < 16; i++){
      addition.e[i] = matrix_1.e[i] + matrix_2.e[i];
    }

    return addition;
  }

  Mat4 operator+(const Mat4& matrix_1, const Mat4& matrix_2) {

    Mat4 addition;

    for(unsigned short int i = 0; i < 16; i++){
      addition.e[i] = matrix_1.e[i] + matrix_2.e[i];
    }

    return addition;
  }

  void operator+=(Mat4& matrix_1, const Mat4& matrix_2) {

    for(unsigned short int i = 0; i < 16; i++){
      matrix_1.e[i] += matrix_2.e[i];
    }
  }

  Mat4 Mat4Substract(const Mat4& matrix_1, const Mat4& matrix_2) {

    Mat4 addition;

    for(unsigned short int i = 0; i < 16; i++){
      addition.e[i] = matrix_1.e[i] - matrix_2.e[i];
    }

    return addition;
  }

  Mat4 operator-(const Mat4& matrix_1, const Mat4& matrix_2) {

    Mat4 addition;

    for(unsigned short int i = 0; i < 16; i++){
      addition.e[i] = matrix_1.e[i] - matrix_2.e[i];
    }

    return addition;
  }

  void operator-=(Mat4& matrix_1, const Mat4& matrix_2) {

    for(unsigned short int i = 0; i < 16; i++){
      matrix_1.e[i] -= matrix_2.e[i];
    }
  }


/*******************************************
*****   VEC3 && MAT3 TRANSFORMATIONS   *****
*******************************************/

  Vec4 Vec4Mat4Multiply(const Vec4& vector, const Mat4& matrix) {

  	Vec4 result;

  	result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z +
               matrix.e[3] * vector.w ;

    result.y = matrix.e[4] * vector.x +
               matrix.e[5] * vector.y +
               matrix.e[6] * vector.z +
               matrix.e[7] * vector.w ;

    result.z = matrix.e[8] * vector.x +
               matrix.e[9] * vector.y +
               matrix.e[10] * vector.z +
               matrix.e[11] * vector.w ;

    result.w = matrix.e[12] * vector.x +
               matrix.e[13] * vector.y +
               matrix.e[14] * vector.z +
               matrix.e[15] * vector.w ;

  	return result;
  }

  Vec4 operator*(const Vec4& vector, const Mat4& matrix) {

    Vec4 result;

    result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z +
               matrix.e[3] * vector.w ;

    result.y = matrix.e[4] * vector.x +
               matrix.e[5] * vector.y +
               matrix.e[6] * vector.z +
               matrix.e[7] * vector.w ;

    result.z = matrix.e[8] * vector.x +
               matrix.e[9] * vector.y +
               matrix.e[10] * vector.z +
               matrix.e[11] * vector.w ;

    result.w = matrix.e[12] * vector.x +
               matrix.e[13] * vector.y +
               matrix.e[14] * vector.z +
               matrix.e[15] * vector.w ;

    return result;
  }

  Vec4 operator*(const Mat4& matrix, const Vec4& vector) {

    Vec4 result;

    result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z +
               matrix.e[3] * vector.w ;

    result.y = matrix.e[4] * vector.x +
               matrix.e[5] * vector.y +
               matrix.e[6] * vector.z +
               matrix.e[7] * vector.w ;

    result.z = matrix.e[8] * vector.x +
               matrix.e[9] * vector.y +
               matrix.e[10] * vector.z +
               matrix.e[11] * vector.w ;

    result.w = matrix.e[12] * vector.x +
               matrix.e[13] * vector.y +
               matrix.e[14] * vector.z +
               matrix.e[15] * vector.w ;

    return result;
  }

  void operator*=(Vec4& vector, const Mat4& matrix) {

    Vec4 result;

    result.x = matrix.e[0] * vector.x +
               matrix.e[1] * vector.y +
               matrix.e[2] * vector.z +
               matrix.e[3] * vector.w ;

    result.y = matrix.e[4] * vector.x +
               matrix.e[5] * vector.y +
               matrix.e[6] * vector.z +
               matrix.e[7] * vector.w ;

    result.z = matrix.e[8] * vector.x +
               matrix.e[9] * vector.y +
               matrix.e[10] * vector.z +
               matrix.e[11] * vector.w ;

    result.w = matrix.e[12] * vector.x +
               matrix.e[13] * vector.y +
               matrix.e[14] * vector.z +
               matrix.e[15] * vector.w ;

    vector.x = result.x;
    vector.y = result.y;
    vector.z = result.z;
    vector.w = result.w;
  }

  Mat4 Mat4Multiply(const Mat4& matrix_1, const Mat4& matrix_2) {

    Mat4 result;

    for (unsigned short int i = 0; i < 4; i++){
      for (unsigned short int j = 0; j < 4; j++){

        result.e[i * 4 + j] = matrix_1.e[j] * matrix_2.e[i * 4] +
                              matrix_1.e[4 + j] * matrix_2.e[i * 4 + 1] +
                              matrix_1.e[8 + j] * matrix_2.e[i * 4 + 2] +
                              matrix_1.e[12 + j] * matrix_2.e[i * 4 + 3];
      }
    }

    return result;
  }

  Mat4 operator*(const Mat4& matrix_1, const Mat4& matrix_2) {

    Mat4 result;

    for (unsigned short int i = 0; i < 4; i++){
      for (unsigned short int j = 0; j < 4; j++){

        result.e[i * 4 + j] = matrix_1.e[j] * matrix_2.e[i * 4] +
                              matrix_1.e[4 + j] * matrix_2.e[i * 4 + 1] +
                              matrix_1.e[8 + j] * matrix_2.e[i * 4 + 2] +
                              matrix_1.e[12 + j] * matrix_2.e[i * 4 + 3];
      }
    }

    return result;
  }

  void operator*=(Mat4& matrix_1, const Mat4& matrix_2) {

    Mat4 result;

    for (unsigned short int i = 0; i < 4; i++){
      for (unsigned short int j = 0; j < 4; j++){

        result.e[i * 4 + j] = matrix_1.e[j] * matrix_2.e[i * 4] +
                              matrix_1.e[4 + j] * matrix_2.e[i * 4 + 1] +
                              matrix_1.e[8 + j] * matrix_2.e[i * 4 + 2] +
                              matrix_1.e[12 + j] * matrix_2.e[i * 4 + 3];
      }
    }

    for (unsigned short int i = 0; i < 16; i++){
      matrix_1.e[i] = result.e[i];
    }
  }

  Mat4 Mat4Rotate(const Mat4& matrix,
                  const float radians_x,
                  const float radians_y,
                  const float radians_z ) {

    Mat4 rotated;

    rotated = Mat4Multiply(matrix, Mat4SetRotationX(radians_x));
    rotated = Mat4Multiply(rotated, Mat4SetRotationY(radians_y));
    rotated = Mat4Multiply(rotated, Mat4SetRotationZ(radians_z));

    return rotated;
  }

  Mat4 Mat4RotateX(const Mat4& matrix, const float radians) {

    Mat4 rotated;

    rotated = Mat4Multiply(matrix, Mat4SetRotationX(radians));

    return rotated;
  }

  Mat4 Mat4RotateY(const Mat4& matrix, const float radians) {

    Mat4 rotated;

    rotated = Mat4Multiply(matrix, Mat4SetRotationY(radians));

    return rotated;
  }

  Mat4 Mat4RotateZ(const Mat4& matrix, const float radians) {

    Mat4 rotated;

    rotated = Mat4Multiply(matrix, Mat4SetRotationZ(radians));

    return rotated;
  }

  Mat4 Mat4Translate(const Mat4& matrix,
                     const float trans_x,
                     const float trans_y,
                     const float trans_z) {

    Mat4 translated;

    translated = Mat4Multiply(matrix,
        Mat4SetTranslation(trans_x, trans_y, trans_z));

    return translated;
  }

  Mat4 Mat4Scale(const Mat4& matrix,
                 const float scale_x,
                 const float scale_y,
                 const float scale_z) {

    Mat4 scale;

    scale = Mat4Multiply(matrix, Mat4SetScale(scale_x, scale_y, scale_z));

    return scale;
  }

  Mat4 Mat4UniformScale(const Mat4& matrix, const float scale) {

    Mat4 uniform;

    uniform = Mat4Multiply(matrix, Mat4SetScale(scale, scale, scale));

    return uniform;
  }

  Mat4 Mat4ProjectTo2D(const Mat4& matrix) {

    Mat4 projected;

    projected = Mat4Multiply(matrix, Mat4SetProjection3Dto2D());

    return projected;
  }


  /*******************************************
  *****            DRAW FIGURES          *****
  *******************************************/

  void Vec2DrawExtrudedShape(Vec2* vertex,
                             const unsigned short int num_vertex,
                             const Mat4& transform,
                             const float height) {

  unsigned short int i, j;
  float half_height = height / 2;

  Vec4 points[4];

    for (i = 0; i < num_vertex; i++){

      points[0] = Vec4InitByComponents(vertex[i].x,
                                       vertex[i].y,
                                       -half_height,
                                       1);

      points[1] = Vec4InitByComponents(vertex[(i + 1) % num_vertex].x,
                                       vertex[(i + 1) % num_vertex].y,
                                       -half_height,
                                       1);

      points[2] = Vec4InitByComponents(points[0].x,
                                       points[0].y,
                                       half_height,
                                       1);

      points[3] = Vec4InitByComponents(points[1].x,
                                       points[1].y,
                                       half_height,
                                       1);


      for (j = 0; j < 4; j++){
        points[j] = Vec4Mat4Multiply(points[j], transform);
        points[j] = Vec4Homogenize(points[j]);
      }

      ESAT::DrawLine(points[0].x, points[0].y, points[1].x, points[1].y);
      ESAT::DrawLine(points[3].x, points[3].y, points[1].x, points[1].y);
      ESAT::DrawLine(points[2].x, points[2].y, points[0].x, points[0].y);
      ESAT::DrawLine(points[2].x, points[2].y, points[3].x, points[3].y);
    }
  }

  void Vec2DrawSurfaceRevolution(Vec2* vertex,
                                 const unsigned short int num_vertex,
                                 const Mat4& transform,
                                 const unsigned short int divisions) {

  	unsigned short int i,j,k;
  	float radius1, radius2;
  	//Angle per division, will be constant.
  	float piece_angle = N_2Pi / divisions;

  	Vec4 points[4];

  	for (i = 0; i < divisions; i++){

  		for (j = 0; j < num_vertex - 1; j++){

  			radius1 = vertex[j].x;
  			radius2 = vertex[(j + 1)].x;

  			points[0] = Vec4InitByComponents(radius1 * cos(i * piece_angle),
                                         vertex[j].y,
                                         radius1 * sin(i * piece_angle),
                                         1);

  			points[1] = Vec4InitByComponents(radius1 * cos((i + 1) * piece_angle),
                                         vertex[j].y,
                                         radius1 * sin((i + 1) * piece_angle),
                                         1);

  			points[2] = Vec4InitByComponents(radius2 * cos(i * piece_angle),
                                         vertex[j + 1].y,
                                         radius2 * sin(i * piece_angle),
                                         1);

  			points[3] = Vec4InitByComponents(radius2 * cos((i + 1) * piece_angle),
                                         vertex[j + 1].y,
                                         radius2 * sin((i + 1) * piece_angle),
                                         1);

  			for (k = 0; k < 4; k++){
  				points[k] = Vec4Mat4Multiply(points[k], transform);
  				points[k] = Vec4Homogenize(points[k]);
  			}

  			ESAT::DrawLine(points[0].x, points[0].y, points[1].x, points[1].y);
  			ESAT::DrawLine(points[2].x, points[2].y, points[3].x, points[3].y);
  			ESAT::DrawLine(points[0].x, points[0].y, points[2].x, points[2].y);
  			ESAT::DrawLine(points[1].x, points[1].y, points[3].x, points[3].y);

  		}
  	}
  }



  void Vec3DrawExtrudedShape(Vec3* vertex,
                             const unsigned short int num_vertex,
                             const Mat4& transform,
                             const float height) {

  unsigned short int i, j;
  float half_height = height / 2;

  Vec4 points[4];

    for (i = 0; i < num_vertex; i++){

      points[0] = Vec4InitByComponents(vertex[i].x,
                                       vertex[i].y,
                                       -half_height,
                                       1);

      points[1] = Vec4InitByComponents(vertex[(i + 1) % num_vertex].x,
                                       vertex[(i + 1) % num_vertex].y,
                                       -half_height,
                                       1);

      points[2] = Vec4InitByComponents(points[0].x,
                                       points[0].y,
                                       half_height,
                                       1);

      points[3] = Vec4InitByComponents(points[1].x,
                                       points[1].y,
                                       half_height,
                                       1);


      for (j = 0; j < 4; j++){
        points[j] = Vec4Mat4Multiply(points[j], transform);
        points[j] = Vec4Homogenize(points[j]);
      }

      ESAT::DrawLine(points[0].x, points[0].y, points[1].x, points[1].y);
      ESAT::DrawLine(points[3].x, points[3].y, points[1].x, points[1].y);
      ESAT::DrawLine(points[2].x, points[2].y, points[0].x, points[0].y);
      ESAT::DrawLine(points[2].x, points[2].y, points[3].x, points[3].y);
    }
  }

  void Vec3DrawSurfaceRevolution(Vec3* vertex,
                                 const unsigned short int num_vertex,
                                 const Mat4& transform,
                                 const unsigned short int divisions) {

  	unsigned short int i,j,k;
  	float radius1, radius2;
  	//Angle per division, will be constant.
  	float piece_angle = N_2Pi / divisions;

  	Vec4 points[4];

  	for (i = 0; i < divisions; i++){

  		for (j = 0; j < num_vertex - 1; j++){

  			radius1 = vertex[j].x;
  			radius2 = vertex[(j + 1)].x;

  			points[0] = Vec4InitByComponents(radius1 * cos(i * piece_angle),
                                         vertex[j].y,
                                         radius1 * sin(i * piece_angle),
                                         1);

  			points[1] = Vec4InitByComponents(radius1 * cos((i + 1) * piece_angle),
                                         vertex[j].y,
                                         radius1 * sin((i + 1) * piece_angle),
                                         1);

  			points[2] = Vec4InitByComponents(radius2 * cos(i * piece_angle),
                                         vertex[j + 1].y,
                                         radius2 * sin(i * piece_angle),
                                         1);

  			points[3] = Vec4InitByComponents(radius2 * cos((i + 1) * piece_angle),
                                         vertex[j + 1].y,
                                         radius2 * sin((i + 1) * piece_angle),
                                         1);

  			for (k = 0; k < 4; k++){
  				points[k] = Vec4Mat4Multiply(points[k], transform);
  				points[k] = Vec4Homogenize(points[k]);
  			}

  			ESAT::DrawLine(points[0].x, points[0].y, points[1].x, points[1].y);
  			ESAT::DrawLine(points[2].x, points[2].y, points[3].x, points[3].y);
  			ESAT::DrawLine(points[0].x, points[0].y, points[2].x, points[2].y);
  			ESAT::DrawLine(points[1].x, points[1].y, points[3].x, points[3].y);

  		}
  	}
  }


  void Vec4DrawExtrudedShape(Vec4* vertex,
                             const unsigned short int num_vertex,
                             const Mat4& transform,
                             const float height) {

  unsigned short int i, j;
  float half_height = height / 2;

  Vec4 points[4];

    for (i = 0; i < num_vertex; i++){

      points[0] = Vec4InitByComponents(vertex[i].x,
                                       vertex[i].y,
                                       -half_height,
                                       1);

      points[1] = Vec4InitByComponents(vertex[(i + 1) % num_vertex].x,
                                       vertex[(i + 1) % num_vertex].y,
                                       -half_height,
                                       1);

      points[2] = Vec4InitByComponents(points[0].x,
                                       points[0].y,
                                       half_height,
                                       1);

      points[3] = Vec4InitByComponents(points[1].x,
                                       points[1].y,
                                       half_height,
                                       1);


      for (j = 0; j < 4; j++){
        points[j] = Vec4Mat4Multiply(points[j], transform);
        points[j] = Vec4Homogenize(points[j]);
      }

      ESAT::DrawLine(points[0].x, points[0].y, points[1].x, points[1].y);
      ESAT::DrawLine(points[3].x, points[3].y, points[1].x, points[1].y);
      ESAT::DrawLine(points[2].x, points[2].y, points[0].x, points[0].y);
      ESAT::DrawLine(points[2].x, points[2].y, points[3].x, points[3].y);
    }
  }

  void Vec4DrawSurfaceRevolution(Vec4* vertex,
                                 const unsigned short int num_vertex,
                                 const Mat4& transform,
                                 const unsigned short int divisions) {

    unsigned short int i,j,k;
    float radius1, radius2;
    //Angle per division, will be constant.
    float piece_angle = N_2Pi / divisions;

    Vec4 points[4];

    for (i = 0; i < divisions; i++){

      for (j = 0; j < num_vertex - 1; j++){

        radius1 = vertex[j].x;
        radius2 = vertex[(j + 1)].x;

        points[0] = Vec4InitByComponents(radius1 * cos(i * piece_angle),
                                         vertex[j].y,
                                         radius1 * sin(i * piece_angle),
                                         1);

        points[1] = Vec4InitByComponents(radius1 * cos((i + 1) * piece_angle),
                                         vertex[j].y,
                                         radius1 * sin((i + 1) * piece_angle),
                                         1);

        points[2] = Vec4InitByComponents(radius2 * cos(i * piece_angle),
                                         vertex[j + 1].y,
                                         radius2 * sin(i * piece_angle),
                                         1);

        points[3] = Vec4InitByComponents(radius2 * cos((i + 1) * piece_angle),
                                         vertex[j + 1].y,
                                         radius2 * sin((i + 1) * piece_angle),
                                         1);

        for (k = 0; k < 4; k++){
          points[k] = Vec4Mat4Multiply(points[k], transform);
          points[k] = Vec4Homogenize(points[k]);
        }

        ESAT::DrawLine(points[0].x, points[0].y, points[1].x, points[1].y);
        ESAT::DrawLine(points[2].x, points[2].y, points[3].x, points[3].y);
        ESAT::DrawLine(points[0].x, points[0].y, points[2].x, points[2].y);
        ESAT::DrawLine(points[1].x, points[1].y, points[3].x, points[3].y);

      }
    }
  }




/*****************************
*****        PRINT        ****
*****************************/

void PrintMat2(const Mat2& matrix) {

  printf("\n\n\n     Matriz 2x2: ");
  printf("\n\n %7.02f  %7.02f", matrix.e[0], matrix.e[2]);
  printf("\n\n %7.02f  %7.02f", matrix.e[1], matrix.e[3]);
}

  void PrintMat3(const Mat3& matrix) {

  	printf("\n\n\n          Matriz 3x3: ");
  	printf("\n\n %7.02f  %7.02f  %7.02f", matrix.e[0], matrix.e[3], matrix.e[6]);
  	printf("\n\n %7.02f  %7.02f  %7.02f", matrix.e[1], matrix.e[4], matrix.e[7]);
  	printf("\n\n %7.02f  %7.02f  %7.02f", matrix.e[2], matrix.e[5], matrix.e[8]);
  }

  void PrintMat4(const Mat4& matrix) {

  	printf("\n\n\n               Matriz 4x4: ");
    printf("\n\n %7.02f  %7.02f  %7.02f  %7.02f",
        matrix.e[0], matrix.e[4], matrix.e[8], matrix.e[12] );
    printf("\n\n %7.02f  %7.02f  %7.02f  %7.02f",
        matrix.e[1], matrix.e[5], matrix.e[9], matrix.e[13] );
    printf("\n\n %7.02f  %7.02f  %7.02f  %7.02f",
        matrix.e[2], matrix.e[6], matrix.e[10], matrix.e[14] );
    printf("\n\n %7.02f  %7.02f  %7.02f  %7.02f",
        matrix.e[3], matrix.e[7], matrix.e[11], matrix.e[15] );
  }

  void PrintVec2(const Vec2& vector) {

    printf("\n\n\n Vector2: ( %7.05f , %7.05f )", vector.x, vector.y);
  }

  void PrintVec3(const Vec3& vector) {

  	printf("\n\n\n Vector3: ( %7.02f , %7.02f , %7.02f )",
           vector.x, vector.y, vector.z);
  }

  void PrintVec4(const Vec4& vector) {

  	printf("\n\n\n Vector4: ( %7.02f , %7.02f , %7.02f , %7.02f )",
           vector.x, vector.y, vector.z, vector.w);
  }

  Vec2 GetRandomPointInsideArea(const Vec2& min, const Vec2& max) {
    JMATH::Vec2i range = { max.x - min.x - 2.0f, max.y - min.y - 2.0f };
    return { (float)(rand() % range.x) + min.x + 1.0f,
             (float)(rand() % range.y) + min.y + 1.0f};
  }

  bool isPointInsideArea(const Vec2 & point, const Vec2 & min, const Vec2 & max) {
    
    return (point.x >= min.x && point.x <= max.x &&
            point.y >= min.y && point.y <= max.y);
  }

  Vec2 getFlockingVariationRange(const int x_radius, const int y_radius) {
    
    return { (float)((rand() % x_radius * 20) - x_radius * 10) * 0.1f,
             (float)((rand() % y_radius * 20) - y_radius * 10) * 0.1f};
  }


};

/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA Prisoner class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "prisoner.h"
#include "ESAT_SDK/Time.h"
#include "ESAT_SDK/draw.h"
#include "jmath.h"
#include "gamemanager.h"


/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/


Prisoner::Prisoner() : Agent() {
  movement_ = kPrisonerMovement_Random;
  state_ = kPrisonerState_Init;
  state_before_escaping_ = kPrisonerState_Init;
  
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = 0.0f;
  delta_ = 0.0;
  collision_radius_ = 0.0f;
  movement_timer_ = 0.0f;
  random_movement_time_to_change_ = 0.0f;
  has_a_package_ = false;
  tracking_id_ = 0;
  current_path_ = nullptr;
  inverse_path_ = false;
  
}

Prisoner::Prisoner(const Prisoner& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
}
Prisoner& Prisoner::operator=(const Prisoner& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
  return *this;
}

Prisoner::~Prisoner() {}


/*******************************************************************************
***                             AGENT METHODS                                ***
*******************************************************************************/

void Prisoner::init() {
  auto& game = Game::instance();
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = kPrisonerNormalSpeed + (float)((rand() % 10));
  velocity_ = { 1.0f, 0.0f };
  random_movement_time_to_change_ = 3000.0f;
  if (JMATH::isPointInsideArea(position_, game.min_resting_area_, game.max_resting_area_)) {
    state_ = kPrisonerState_Resting;
    movement_ = kPrisonerMovement_Random;
  }
  else if (JMATH::isPointInsideArea(position_, game.min_working_area_, game.max_working_area_)) {
    state_ = kPrisonerState_Working;
    movement_ = kPrisonerMovement_Determ;
    leavePackage();
  }
  
}

void Prisoner::draw() {
  
  DrawSquare(position_, 255,255,255,255, 5.0f);
}

void Prisoner::update(const double delta_time) {
  delta_ = delta_time;
  mindUpdate();
  bodyUpdate();
}


/*******************************************************************************
***                              MIND UPDATES                                   ***
*******************************************************************************/



void Prisoner::mindUpdate() {
  switch (state_) {
    case kPrisonerState_Init:
      init();
    break;
    case kPrisonerState_Working:
      workingUpdate();
    break;
    case kPrisonerState_Resting:
      restingUpdate();
    break;
    case kPrisonerState_RestingToWorking:
      restingToWorkingUpdate();
    break;
    case kPrisonerState_WorkingToResting:
      workingToRestingUpdate();
    break;
    case kPrisonerState_Alarm:
      alarmUpdate();
    break;
    case kPrisonerState_Freedom:
      freedomUpdate();
    break;
  }
  
}


void Prisoner::restingUpdate() {

  auto& game = Game::instance();
  
  // No me hagas pensar, que estoy descansando :) gracias.


}

void Prisoner::workingUpdate() {

  auto& game = Game::instance();

  if (isNextToDestiny(destiny_)) {
    if (has_a_package_) {
      leavePackage();
    }
    else {
      loadPackage();
    }
  }
  
}

void Prisoner::loadPackage() {
  auto& game = Game::instance();
  has_a_package_ = true;
  destiny_ = JMATH::GetRandomPointInsideArea(game.min_unpacking_area_, game.max_unpacking_area_);
  lookAt(destiny_);
  speed_ = kPrisonerNormalSpeed * 0.5f;
}

void Prisoner::leavePackage() {
  auto& game = Game::instance();
  has_a_package_ = false;
  destiny_ = JMATH::GetRandomPointInsideArea(game.min_packing_area_, game.max_packing_area_);
  lookAt(destiny_);
  speed_ = kPrisonerNormalSpeed;
}

void Prisoner::workingToRestingUpdate() {
  if (tracking_id_ >= current_path_->length()) {
    state_ = kPrisonerState_Resting;
    if (Game::instance().is_alarm_mode_on_) {
      startAlarm();
    }
    else {
      startResting();
    }
  }
}

void Prisoner::restingToWorkingUpdate() {
  if (tracking_id_ >= current_path_->length()) {
    state_ = kPrisonerState_Working;
    if (Game::instance().is_alarm_mode_on_) {
      startAlarm();
    }
    else {
      startWorking();
    }
  }
}

void Prisoner::alarmUpdate() {
  auto& game = Game::instance();

  if (current_path_ == &game.resting_to_door_A_path_) {
    if (tracking_id_ >= current_path_->length()) {
      // Comprobamos la puerta A y si esta abierta nos escapamos.
      if (game.is_door_A_opened_) {
        state_ = kPrisonerState_Freedom;
        current_path_ = &game.base_A_to_out_door_A_path_;
        inverse_path_ = true;
        tracking_id_ = current_path_->length() - 1;
        destiny_ = current_path_->path_vector_[tracking_id_];
        destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
        lookAt(destiny_);
        Game::instance().num_prisoners_free_ += 1;
      }
      else {
        // Si no esta abierta y seguimos en alarma nos vamos a comprobar la puerta B
        tracking_id_ = 0;
        inverse_path_ = false;
        current_path_ = &game.door_A_to_door_B_path_;
        destiny_ = current_path_->path_vector_[tracking_id_];
        destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
        lookAt(destiny_);
        if (!game.is_alarm_mode_on_) {
        // Si ya no estamos en alarma, volvemos a trabajar o a descansar.
          if (state_before_escaping_ == kPrisonerState_Working) {
            state_ = kPrisonerState_RestingToWorking;
            tracking_id_ = current_path_->length() - 1; // Esto es para que se ponga a currar.
          }
          else {
            state_ = kPrisonerState_WorkingToResting;
            current_path_ = &game.working_to_resting_path_;
            tracking_id_ = -1;
          }

        }
      }
    }
  }
  else if (current_path_ == &game.door_A_to_door_B_path_) {
    if (tracking_id_ >= current_path_->length()) {
      // Al llegar a la puerta B comprobamos si esta abierta.
      if (game.is_door_B_opened_) {
        state_ = kPrisonerState_Freedom;
        current_path_ = &game.out_door_B_to_base_B_path_;
        inverse_path_ = false;
        tracking_id_ = 0;
        destiny_ = current_path_->path_vector_[tracking_id_];
        destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
        lookAt(destiny_);
        Game::instance().num_prisoners_free_ += 1;
      }
      else {
        // Si no esta abierta volvemos a la puerta A.
        tracking_id_ -= 2;
        inverse_path_ = true;
        destiny_ = current_path_->path_vector_[tracking_id_];
        destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
        lookAt(destiny_);
      }
    }
    else if (tracking_id_ < 0) {
      
      if (game.is_alarm_mode_on_) { // Continuamos buscando puertas abiertas si esta la alarma.
        inverse_path_ = false;
        current_path_ = &Game::instance().resting_to_door_A_path_;
        tracking_id_ = current_path_->length() - 1;
        destiny_ = current_path_->path_vector_[tracking_id_];
        destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
        lookAt(destiny_);
      }
      else {
        if (state_before_escaping_ == kPrisonerState_Working) {
          startWorking();
          inverse_path_ = false;
        }
        else {
          state_ = kPrisonerState_WorkingToResting;
          tracking_id_ = 0;
          inverse_path_ = false;
          current_path_ = &game.working_to_resting_path_;
          destiny_ = current_path_->path_vector_[tracking_id_];
          destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
          lookAt(destiny_);
        }
      }

    }
  }
}

void Prisoner::freedomUpdate() {
  if (tracking_id_ >= current_path_->length() || tracking_id_ < 0) {
    state_ = kPrisonerState_End;
    movement_ = kPrisonerMovement_None;
  }
}


/*******************************************************************************
***                             BODY UPDATES                                 ***
*******************************************************************************/

void Prisoner::bodyUpdate() {

  switch (movement_) {
    case kPrisonerMovement_Determ:
    moveDeterm();
    break;
    case kPrisonerMovement_Random:
    moveRandom();
    break;
    case kPrisonerMovement_Tracker:
    moveTracker();
    break;
  }

}

void Prisoner::lookAt(JMATH::Vec2 destiny) {
  velocity_ = { destiny.x - position_.x,destiny.y - position_.y };
  velocity_ = Vec2Normalize(velocity_);
}

float Prisoner::getDistanceToPoint(const JMATH::Vec2 point) {
  JMATH::Vec2 distance = { point.x - position_.x, point.y - position_.y };
  float result = JMATH::Vec2Magnitude(distance);
  if (result < 0.0f) {
    result *= -1.0f;
  }
  return result;
}

void Prisoner::moveDeterm() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_};
}



void Prisoner::moveRandom() {
  auto& game = Game::instance();

  movement_timer_ -= (float)delta_;
  if (movement_timer_ < 0.0) {
    movement_timer_ = random_movement_time_to_change_;
    velocity_ = { (float)((rand() % 30) - 15), (float)((rand() % 30) - 15) };
    velocity_ = Vec2Normalize(velocity_);
  }
 
  JMATH::Vec2 distance = { velocity_.x * ((float)delta_ * 0.001f) * speed_,
                           velocity_.y * ((float)delta_ * 0.001f) * speed_ };

  
  bool is_out_of_area = false;

  if (position_.x + distance.x <= game.min_resting_area_.x || 
      position_.x + distance.x >= game.max_resting_area_.x) {
    velocity_.x *= -1;
    is_out_of_area = true;
  }
  if (position_.y + distance.y <= game.min_resting_area_.y || 
      position_.y + distance.y >= game.max_resting_area_.y) {
    velocity_.y *= -1;
    is_out_of_area = true;
  }

  if (!is_out_of_area) {
    position_ = position_ + distance;
  }
}

void Prisoner::moveTracker() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ };

  if (isNextToDestiny(destiny_)) {
    if (inverse_path_) { tracking_id_--; }
    else { tracking_id_++; }
    if (tracking_id_ < current_path_->length() && tracking_id_ >= 0) {
      destiny_ = current_path_->path_vector_[tracking_id_];
      if (current_path_ != &Game::instance().base_A_to_out_door_A_path_ &&
          current_path_ != &Game::instance().out_door_B_to_base_B_path_) {
        destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
      }
      lookAt(destiny_);
    }
  }

}

/*******************************************************************************
***                              TRANSITIONS                                 ***
*******************************************************************************/

void Prisoner::startResting() {
  state_ = kPrisonerState_Resting;
  movement_ = kPrisonerMovement_Random;
}

void Prisoner::startWorking() {
  state_ = kPrisonerState_Working;
  movement_ = kPrisonerMovement_Determ;
  leavePackage();
}

void Prisoner::swapBetweenWorkingAndResting() {
  if (state_ == kPrisonerState_Resting) {
    movement_ = kPrisonerMovement_Tracker;
    tracking_id_ = 0;
    current_path_ = &Game::instance().resting_to_working_path_;
    destiny_ = current_path_->path_vector_[tracking_id_];
    destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
    lookAt(destiny_);
    state_ = kPrisonerState_RestingToWorking;
  }
  else if (state_ == kPrisonerState_Working) {
    leavePackage();
    movement_ = kPrisonerMovement_Tracker;
    tracking_id_ = 0;
    current_path_ = &Game::instance().working_to_resting_path_;
    destiny_ = current_path_->path_vector_[tracking_id_];
    destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
    lookAt(destiny_);
    state_ = kPrisonerState_WorkingToResting;
  }
}


void Prisoner::startAlarm() {
  switch (state_) {
    case kPrisonerState_Resting: {
      state_before_escaping_ = kPrisonerState_Resting;
      current_path_ = &Game::instance().resting_to_door_A_path_;
      tracking_id_ = 0;
      destiny_ = current_path_->path_vector_[tracking_id_];
      destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
      lookAt(destiny_);
      state_ = kPrisonerState_Alarm;
      movement_ = kPrisonerMovement_Tracker;
    } break;
    case kPrisonerState_Working: {
      state_before_escaping_ = kPrisonerState_Working;
      current_path_ = &Game::instance().resting_to_door_A_path_;
      // para que empiece por los dos ultimos que son los que van a la puerta A
      // desde la sala de working
      leavePackage();
      tracking_id_ = current_path_->length() - 2; 
      destiny_ = current_path_->path_vector_[tracking_id_];
      destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
      lookAt(destiny_);
      state_ = kPrisonerState_Alarm;
      movement_ = kPrisonerMovement_Tracker;
    } break;
  }
}

void Prisoner::stopAlarm() {

  auto& game = Game::instance();

  // Volvemos atras para volver a currar o descansar.
  if (current_path_ == &game.door_A_to_door_B_path_ && !inverse_path_) {
    if (tracking_id_ > 0) {
      tracking_id_ -= 1;
      inverse_path_ = true;
      destiny_ = current_path_->path_vector_[tracking_id_];
      destiny_ += JMATH::getFlockingVariationRange(kPrisonerFlockingRadius, kPrisonerFlockingRadius);
      lookAt(destiny_);
    } 
  }

}


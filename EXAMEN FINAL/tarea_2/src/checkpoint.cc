/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA Checkpoint class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "checkpoint.h"
#include <stdio.h>
#include "gamemanager.h"


/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

CheckPoint::CheckPoint() {
  num_neighbors_ = 0;
  position_ = { 0.0f, 0.0f };
}


CheckPoint::~CheckPoint() {
  neighbor_list_.clear();
}


void CheckPoint::draw() {
  for (unsigned int i = 0; i < neighbor_list_.size(); ++i) {
    DrawRect(neighbor_list_[i]->position_, 255, 0, 255, 255, Game::instance().cost_map_.cell_size_);
  }
}

CheckPoint * CheckPoint::getRandomNeigbor() {
  if (num_neighbors_ < 1) {
    printf("\n No neighbor available.");
    return nullptr;
  }

  return neighbor_list_[rand() % num_neighbors_];
}


void CheckPoint::addNeighbor(CheckPoint* neighbor) {
  neighbor_list_.push_back(neighbor);
  num_neighbors_ += 1;
}




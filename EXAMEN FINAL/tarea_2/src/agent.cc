/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Examen Final Tarea 2
*  @brief IA agent class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "agent.h"
#include "gamemanager.h"

Agent::Agent() {
  id_ = 0;
  alive_ = false;
  speed_ = 0.0f;
  velocity_ = { 0.0f, 0.0f };
  position_ = { 0.0f, 0.0f };
}

Agent::~Agent() {}

Agent::Agent(const Agent& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
}
Agent& Agent::operator=(const Agent& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
  return *this;
}

bool Agent::isNextToDestiny(JMATH::Vec2 destiny) {
  if (position_.x < destiny.x + 3.0f &&
      position_.x > destiny.x - 3.0f &&
      position_.y < destiny.y + 3.0f &&
      position_.y > destiny.y - 3.0f) {
    return true;
  }
  return false;
}
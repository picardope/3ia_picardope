/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __PATH_H__ 
#define __PATH_H__ 1

#include "jmath.h"
#include <vector>

class Path {
 public:
  
/*******************************************************************************
***                        Constructor and destructor                        ***
*******************************************************************************/

  Path();
  ~Path();

/*******************************************************************************
***                                  Methods                                 ***
*******************************************************************************/
	
  /**
  * @brief Add a new point position to the path vector.
  * @param position Position of the point we want to add.
  * @return True if success, False if fail.
  */
  bool addPoint(const JMATH::Vec2 position);

  /**
  * @brief Gets the length of the path vector.
  * @return Int with the number of steps.
  */
	int length();

  /**
  * @brief Gets the center position of the next point cell.
  * @return Vec2 with the center position of the next point cell.
  */
	JMATH::Vec2 getNextPoint();

  /// @brief Draws squares in all the positions of the route.
	void draw(); 

/*******************************************************************************
***                             Public Properties                                 ***
*******************************************************************************/

  /// Saves all the points of the path.
  std::vector<JMATH::Vec2> path_vector_;
  /// Current index of the vector.
  int current_point_;

/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
private:

  Path(const Path& copy);
  Path& operator=(const Path& copy);

};
#endif 
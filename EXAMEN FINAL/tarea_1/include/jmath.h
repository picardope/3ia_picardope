/**
*
* @project JMATH Library
* @file jmath.h
* @brief Mathematic 2D and 3D library.
* @author Julio Marcelo Picardo <picardope@esat-alumni.com>
* @date 28/11/2015
*
*/

/*
* REMEMBER 3D:
* First step is to apply the initial transformations to the vectors.
* Then project it from 3D to 2D (like taking a picture).
* And after that, scale the picture in X and Z... BUT JUST 1 IN Z AXIS.
* Then translate the picture in X and Y ... BUT NOT IN Z ... TRANSLATE IN Z == 0
* AFTER ALL -> HOMOGENIZE ALL THE VECTORS.
*/

#ifndef __JMATH_H__
#define __JMATH_H__ 1


#define N_Pi 3.14159265358979323846
#define N_2Pi 6.2831853071795865
#define N_Pi2 1.57079632679489661923


/// @brief JMATH namespace to identify "Julio MArTHelo's library"
namespace JMATH {
  /*******************************************
  ********           STRUCTS           *******
  *******************************************/

  /// @brief 2D vector without Homogeneous coordinate.
  typedef struct {
    float x;
    float y;
  } Vec2;

  /// @brief 2D vector without Homogeneous coordinate.
  typedef struct {
    int x;
    int y;
  } Vec2i;

  /// @brief 2D vector where Z is the Homogeneous coordinate.
  typedef struct {
    float x;
    float y;
    float z;
  } Vec3;

  /// @brief 2D vector where Z is the Homogeneous coordinate.
  typedef struct {
    int x;
    int y;
    int z;
  } Vec3i;

  /// @brief 2D vector where W is the Homogeneous coordinate.
  typedef struct {
    float x;
    float y;
    float z;
    float w;
  } Vec4;

  /**
  * @brief Square 2 dimensions matrix .
  *
  * e[4] represent each of the elements of the matrix.
  * Matrix elements are ordered by columns, so the structure will be like this:
  *
  *      e0  e2
  *      e1  e3
  */
  typedef struct {
    /**
    * @brief Access to an element to get or set its value.
    * @param position Position in the array.
    * @returns a reference to the element.
    */
    float& operator[](const int position);
    float e[4];
  } Mat2;

  /**
  * @brief Square 3 dimensions matrix .
  *
  * e[9] represent each of the elements of the matrix.
  * Matrix elements are ordered by columns, so the structure will be like this:
  *
  *      e0  e3  e6
  *      e1  e4  e7
  *      e2  e5  e8
  */
  typedef struct {
    /**
    * @brief Access to an element to get or set its value.
    * @param position Position in the array.
    * @returns a reference to the element.
    */
    float& operator[](const int position);
    float e[9];
  } Mat3;

  /**
  * @brief Square 4 dimensions matrix .
  *
  * e[16] represent each of the elements of the matrix.
  * Matrix elements are ordered by columns, so the structure will be like this:
  *
  *      e0   e4   e8   e12
  *      e1   e5   e9   e13
  *      e2   e6   e10  e14
  *      e3   e7   e11  e15
  */
  typedef struct {
    /**
    * @brief Access to an element to get or set its value.
    * @param position Position in the array.
    * @returns a reference to the element.
    */
    float& operator[](const int position);
    float e[16];
  } Mat4;


  /***************************************************************************
  ********************      COMPARISON OPERATORS      ************************
  ***************************************************************************/

  /**
  * @brief Equality comparison between two elements.
  *
  * @param vector_1 first element to compare.
  * @param vector_2 second element to compare.
  * @returns True if equal, false if not.
  */
  bool operator==(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Difference comparison between two elements.
  *
  * @param vector_1 first element to compare.
  * @param vector_2 second element to compare.
  * @returns False if equal, true if not.
  */
  bool operator!=(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Equality comparison between two elements.
  *
  * @param vector_1 first element to compare.
  * @param vector_2 second element to compare.
  * @returns True if equal, false if not.
  */
  bool operator==(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Difference comparison between two elements.
  *
  * @param vector_1 first element to compare.
  * @param vector_2 second element to compare.
  * @returns False if equal, true if not.
  */
  bool operator!=(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Equality comparison between two elements.
  *
  * @param vector_1 first element to compare.
  * @param vector_2 second element to compare.
  * @returns True if equal, false if not.
  */
  bool operator==(const Vec4& vector_1, const Vec4& vector_2);

  /**
  * @brief Difference comparison between two elements.
  *
  * @param vector_1 first element to compare.
  * @param vector_2 second element to compare.
  * @returns False if equal, true if not.
  */
  bool operator!=(const Vec4& vector_1, const Vec4& vector_2);

  /**
  * @brief Equality comparison between two elements.
  *
  * @param matrix_1 first element to compare.
  * @param matrix_2 second element to compare.
  * @returns True if equal, false if not.
  */
  bool operator==(const Mat2& matrix_1, const Mat2& matrix_2);

  /**
  * @brief Difference comparison between two elements.
  *
  * @param matrix_1 first element to compare.
  * @param matrix_2 second element to compare.
  * @returns False if equal, true if not.
  */
  bool operator!=(const Mat2& matrix_1, const Mat2& matrix_2);

  /**
  * @brief Equality comparison between two elements.
  *
  * @param matrix_1 first element to compare.
  * @param matrix_2 second element to compare.
  * @returns True if equal, false if not.
  */
  bool operator==(const Mat3& matrix_1, const Mat3& matrix_2);

  /**
  * @brief Difference comparison between two elements.
  *
  * @param matrix_1 first element to compare.
  * @param matrix_2 second element to compare.
  * @returns False if equal, true if not.
  */
  bool operator!=(const Mat3& matrix_1, const Mat3& matrix_2);

  /**
  * @brief Equality comparison between two elements.
  *
  * @param matrix_1 first element to compare.
  * @param matrix_2 second element to compare.
  * @returns True if equal, false if not.
  */
  bool operator==(const Mat4& matrix_1, const Mat4& matrix_2);

  /**
  * @brief Difference comparison between two elements.
  *
  * @param matrix_1 first element to compare.
  * @param matrix_2 second element to compare.
  * @returns False if equal, true if not.
  */
  bool operator!=(const Mat4& matrix_1, const Mat4& matrix_2);

  /***************************************************************************
  ***********************          MATH UTILS         ************************
  ***************************************************************************/

  /**
  * @brief Calculates the square root of a number, uses an assembly instruction.
  *
  * @param number Number to calculate it's square root.
  * @returns Square root result.
  */
  double SqrtASM(double number);


  /**
  * @brief Calculates the power of a number.
  *
  * @param base Base of the power.
  * @param exponent Exponent of the power.
  * @returns Power result.
  */
  float Power(const float base, const int exponent);

  /**
  * @brief Calculates the factorial of a number.
  *
  * @param number Integer and positive number we want to calculate its factorial.
  * @returns Factorial of the number.
  */
  unsigned int Factorial(const unsigned int number);

  /**
  * @brief Calculates the absolute value of a number.
  *
  * @param number Number to calculate its absolute value.
  * @returns Double.
  */
  double DAbs(double number);

  /**
  * @brief Calculates the absolute value of a number.
  *
  * @param number Number to calculate its absolute value.
  * @returns Float.
  */
  float FAbs(float number);

  /**
  * @brief Calculates the absolute value of a number.
  *
  * @param number Number to calculate its absolute value.
  * @returns Int.
  */
  int Abs(int number);

  /*******************************************
  ********       ANGLE CONVERSOR       *******
  *******************************************/

  /**
  * @brief Angle conversor between degrees and radians.
  *
  * @param degrees Angle degrees.
  * @returns Radians.
  */
  float GetRadians(const float degrees);

  /**
  * @brief Angle conversor from radians to degrees.
  *
  * @param radians Angle radians.
  * @returns Degrees.
  */
  float GetDegrees(const float radians);


  /*******************************************
  ********        TRIGONOMETRY         *******
  *******************************************/

  /**
  * @brief Calculates the sine of an angle. Using Taylor series.
  *
  * @param radians Angle to calculate its sine.
  * @param accuracy Accuracy of the sine result.
  * @returns Sine of the angle.
  */
  double Sin(double radians, unsigned short int accuracy = 9);

  /**
  * @brief Calculates the cosine of an angle. Using Taylor series.
  *
  * @param radians Angle to calculate its cosine.
  * @returns Sine of the angle.
  */
  double Cos(const double radians);

  /**
  * @brief Calculates the tangent of an angle.
  *
  * @param radians Angle to calculate its tangent.
  * @returns Sine of the angle.
  */
  double Tan(const double radians);

  /**
  * @brief Calculates the cosecant of an angle.
  *
  * @param radians Angle to calculate its cosecant.
  * @returns Cosecant of the angle.
  */
  double Cosec(const double radians);

  /**
  * @brief Calculates the secant of an angle.
  *
  * @param radians Angle to calculate its secant.
  * @returns Secant of the angle.
  */
  double Sec(const double radians);

  /**
  * @brief Calculates the cotangent of an angle.
  *
  * @param radians Angle to calculate its cotangent.
  * @returns Cotangent of the angle.
  */
  double Cotan(const double radians);


  /***************************************************************************
  ***********************          2D METHODS         ************************
  ***************************************************************************/

  /*******************************************
  ********           VECTOR2           *******
  *******************************************/
  /*****************************
  *****        INITS        ****
  *****************************/

  /**
  * @brief Init a vector by components.
  *
  * @param comp_x component x.
  * @param comp_y component y.
  * @returns Initialized Vec2.
  */
  Vec2 Vec2InitByComponents(const float comp_x, const float comp_y);

  /**
  * @brief Init a vector from two points.
  * @param origin Point designed as the origin of the new vector.
  * @param destiny Point designed as destiny.
  * @returns Initialized Vec2.
  */
  Vec2 Vec2InitFrom2Points(const Vec2& origin, const Vec2& destiny);



  /*****************************
  *****      OPERATIONS     ****
  *****************************/

  /**
  * @brief Adds two vectors.
  * @param vector_1 Vector selected as first addend.
  * @param vector_2 Vector selected as second addend.
  * @returns Vec2 with the result of the addition.
  */
  Vec2 Vec2Addition(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Adds two vectors.
  * @param vector_1 Vector selected as first addend.
  * @param vector_2 Vector selected as second addend.
  * @returns Addition result.
  */
  Vec2 operator+(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Adds two vectors (v1 = v1 + v2).
  * @param vector_1 Vector we want to modify it's value.
  * @param vector_2 Vector selected as second addend.
  */
  void operator+= (Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Substracts two vectors (vector1 - vector2).
  * @param vector_1 Vector selected as minuend.
  * @param vector_2 Vector selected as subtrahend.
  * @returns Vec2 with the result of the operation.
  */
  Vec2 Vec2Substract(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Substracts two vectors (vector1 - vector2).
  * @param vector_1 Vector selected as minuend.
  * @param vector_2 Vector selected as subtrahend.
  * @returns Vec2 with the result of the operation.
  */
  Vec2 operator-(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Substracts two vectors (v1 = v1 - v2).
  * @param vector_1 Vector we want to modify it's value.
  * @param vector_2 Vector selected as second subtrahend.
  */
  void operator-= (Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Gets a perpendicular vector.
  * @param vector Vector to calculate its perpendicular.
  * @returns Perpendicular Vec2.
  */
  Vec2 Vec2Perpendicular(const Vec2& vector);

  /**
  * @brief Gets the opposite of a vector.
  * @param vector Vector to calculate its opposite.
  * @returns Opposite Vec2.
  */
  Vec2 operator-(const Vec2& vector);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec2 Vec2MultiplyByScalar(const Vec2& vector, const float scalar);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec2 operator*(const Vec2& vector, const float scalar);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec2 operator*(const float scalar, const Vec2& vector);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  void operator*=(Vec2& vector, const float scalar);

  /**
  * @brief Divides all the components of the vector by a scalar number.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec2 operator/(const Vec2& vector, const float scalar);

  /**
  * @brief Divides scalar/vector.x, scalar/vector.y, scalar/vector.z.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec2 operator/(const float scalar, const Vec2& vector);

  /**
  * @brief Divides all the components of the vector by a scalar number.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  void operator/=(Vec2& vector, const float scalar);

  /**
  * @brief Gets the middle point between two points.
  * @param point_1 First point.
  * @param point_2 Second point.
  * @returns Mid point.
  */
  Vec2 Vec2MidPoint(const Vec2& point_1, const Vec2& point_2);

  /**
  * @brief Calculates the magnitude of a vector.
  * @param vector Vector to calculate it's magnitude.
  * @returns Vector magnitude.
  */
  float Vec2Magnitude(const Vec2& vector);

  /**
  * @brief Gets a vector with the same direction which magnitude is 1.
  * @param vector Vector to normalize.
  * @returns Normalized vector.
  */
  Vec2 Vec2Normalize(const Vec2& vector);

  /**
  * @brief Gets the dot product from two vectors.
  * @param vector_1 Vector selected as multiplicand.
  * @param vector_2 Vector selected as multiplier.
  * @returns Dot product result.
  */
  float Vec2DotProduct(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Gets the dot product from two vectors.
  * @param vector_1 Vector selected as multiplicand.
  * @param vector_2 Vector selected as multiplier.
  * @returns Dot product result.
  */
  float operator*(const Vec2& vector_1, const Vec2& vector_2);


  /**
  * @brief Gets the angle (radians) between between two vectors .
  * @param vector_1 first vector.
  * @param vector_2 second vector.
  * @returns Angle in radians.
  */
  float Vec2Angle(const Vec2& vector_1, const Vec2& vector_2);

  /**
  * @brief Gets the ortogonal projection of vector_1 on vector_2 .
  * @param vector_1 first vector.
  * @param vector_2 second vector.
  * @returns Projection.
  */
  float Vec2Projection(const Vec2& vector_1, const Vec2& vector_2);


  /*******************************************
  ********         2x2 MATRICES        *******
  *******************************************/
  /*****************************
  *****        INITS        ****
  *****************************/
  /**
  * @brief 2 Dimensions identity matrix.
  *
  *      1  0
  *      0  1
  *
  * @returns Identity Mat2.
  */
  Mat2 Mat2Identity();

  /**
  * @brief 2 Dimensions matrix initialized using vectors as columns.
  *
  *    v1.x  v2.x
  *    v1.y  v2.y
  *
  * @returns Initialized Mat2.
  */
  Mat2 Mat2InitByColumns(const Vec2& column_1, const Vec2& column_2);

  /**
  * @brief 2 Dimensions matrix initialized element by element.
  *
  *     e0  e2
  *     e1  e3
  *
  * @returns Initialized Mat2.
  */
  Mat2 Mat2InitByElements(const float element_0, const float element_1,
                          const float element_2, const float element_3 );



  /*****************************
  *****      OPERATIONS     ****
  *****************************/
  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat2 Mat2MultiplyByScalar(const Mat2& matrix, const float scalar);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat2 operator*(const Mat2& matrix, const float scalar);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat2 operator*(const float scalar, const Mat2& matrix);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  void operator*=(Mat2& matrix, const float scalar);

  /**
  * @brief Calculate the determinant of a matrix.
  * @param matrix Matrix we want to multiply.
  * @returns Matrix determinant.
  */
  float Mat2Determinant(const Mat2& matrix);

  /**
  * @brief Calculates the cofactor value of a matrix 3x3 element.
  * @param matrix Matrix we want to use.
  * @param row Row of the matrix element we want to calculate its cofactor.
  * @param col Column of the Mat2 element.
  * @returns Element cofactor value.
  */
  float Mat2ElementCofactor(const Mat2& matrix,
                            const unsigned short int row,
                            const unsigned short int col);

  /**
  * @brief Calculates the matrix of cofactors.
  * @param matrix Matrix we want to calculate its cofactors.
  * @returns Mat2 of cofactors.
  */
  Mat2 Mat2Cofactors(const Mat2& matrix);

  /**
  * @brief Calculates the transpose of a matrix.
  * @param matrix Matrix we want to calculate its transpose.
  * @returns Transpose of a Mat2.
  */
  Mat2 Mat2Transpose(const Mat2& matrix);

  /**
  * @brief Calculates the adjugate of a matrix.
  * @param matrix Matrix we want to calculate its adjugate.
  * @returns Adjugate of a Mat2.
  */
  Mat2 Mat2Adjoint(const Mat2& matrix);

  /**
  * @brief Calculates the inverse of a matrix.
  * @param matrix Matrix we want to calculate its inverse
  * @returns inverse of a Mat2.
  */
  Mat2 Mat2Inverse(const Mat2& matrix);

  /**
  * @brief Adds two matrices.
  * @param matrix_1 Matrix selected as first addend.
  * @param matrix_2 Matrix selected as second addend.
  * @returns Addition result.
  */
  Mat2 Mat2Addition(const Mat2& matrix_1 , const Mat2& matrix_2);

  /**
  * @brief Adds two matrices.
  * @param matrix_1 Matrix selected as first addend.
  * @param matrix_2 Matrix selected as second addend.
  * @returns Addition result.
  */
  Mat2 operator+(const Mat2& matrix_1 , const Mat2& matrix_2);

  /**
  * @brief Adds two matrices (m1 = m1 + m2).
  * @param matrix_1 Matrix we want to modify it's value.
  * @param matrix_2 Matrix selected as second addend.
  */
  void operator+= (Mat2& matrix_1, const Mat2& matrix_2);

  /**
  * @brief Substract two matrices.
  * @param matrix_1 Matrix selected as minuend.
  * @param matrix_2 Matrix selected as subtrahend.
  * @returns Substraction result.
  */
  Mat2 operator-(const Mat2& matrix_1 , const Mat2& matrix_2);

  /**
  * @brief Substract two matrices (m1 = m1 - m2).
  * @param matrix_1 Matrix we want to modify it's value.
  * @param matrix_2 Matrix selected as second subtrahend.
  */
  void operator-= (Mat2& matrix_1, const Mat2& matrix_2);


  /*******************************************
  ********           VECTOR3           *******
  *******************************************/
  /*****************************
  *****        INITS        ****
  *****************************/

  /**
  * @brief Init a vector by components.
  *
  * POINT   ->   comp_z == 1
  * VECTOR  ->   comp_z == 0
  *
  * @param comp_x component x.
  * @param comp_y component y.
  * @param comp_z component z. Homogeneous coordinate.
  * @returns Initialized Vec3.
  */
  Vec3 Vec3InitByComponents(const float comp_x,
                            const float comp_y,
                            const float comp_z);

  /**
  * @brief Init a vector from two points.
  * @param origin Point designed as the origin of the new vector.
  * @param destiny Point designed as destiny.
  * @returns Initialized Vec3.
  */
  Vec3 Vec3InitFrom2Points(const Vec3& origin, const Vec3& destiny);


  /*****************************
  *****      OPERATIONS     ****
  *****************************/

  /**
  * @brief Adds two vectors.
  * @param vector_1 Vector selected as first addend.
  * @param vector_2 Vector selected as second addend.
  * @returns Vec3 with the result of the addition.
  */
  Vec3 Vec3Addition(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Adds two vectors.
  * @param vector_1 Vector selected as first addend.
  * @param vector_2 Vector selected as second addend.
  * @returns Addition result.
  */
  Vec3 operator+(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Adds two vectors (v1 = v1 + v2).
  * @param vector_1 Vector we want to modify it's value.
  * @param vector_2 Vector selected as second addend.
  */
  void operator+= (Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Substracts two vectors (vector1 - vector2).
  * @param vector_1 Vector selected as minuend.
  * @param vector_2 Vector selected as subtrahend.
  * @returns Vec3 with the result of the operation.
  */
  Vec3 Vec3Substract(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Substracts two vectors (vector1 - vector2).
  * @param vector_1 Vector selected as minuend.
  * @param vector_2 Vector selected as subtrahend.
  * @returns Vec3 with the result of the operation.
  */
  Vec3 operator-(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Substracts two vectors (v1 = v1 - v2).
  * @param vector_1 Vector we want to modify it's value.
  * @param vector_2 Vector selected as second subtrahend.
  */
  void operator-= (Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Gets a perpendicular vector.
  * @param vector Vector to calculate its perpendicular.
  * @returns Perpendicular Vec3.
  */
  Vec3 Vec3Perpendicular(const Vec3& vector);

  /**
  * @brief Gets the opposite of a vector.
  * @param vector Vector to calculate its opposite.
  * @returns Opposite Vec3.
  */
  Vec3 operator-(const Vec3& vector);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec3 Vec3MultiplyByScalar(const Vec3& vector, const float scalar);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec3 operator*(const Vec3& vector, const float scalar);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec3 operator*(const float scalar, const Vec3& vector);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  void operator*=(Vec3& vector, const float scalar);

  /**
  * @brief Divides all the components of the vector by a scalar number.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec3 operator/(const Vec3& vector, const float scalar);

  /**
  * @brief Divides scalar/vector.x, scalar/vector.y, scalar/vector.z.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec3 operator/(const float scalar, const Vec3& vector);

  /**
  * @brief Divides all the components of the vector by a scalar number.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  void operator/=(Vec3& vector, const float scalar);

  /**
  * @brief Homogenizes a vector.
  * @param vector Vector to homogenize.
  * @returns homogeneous Vec3.
  */
  Vec3 Vec3Homogenize(const Vec3& vector);

  /**
  * @brief Gets the middle point between two points.
  * @param point_1 First point.
  * @param point_2 Second point.
  * @returns Mid point.
  */
  Vec3 Vec3MidPoint(const Vec3& point_1, const Vec3& point_2);

  /**
  * @brief Calculates the magnitude of a vector.
  * @param vector Vector to calculate it's magnitude.
  * @returns Vector magnitude.
  */
  float Vec3Magnitude(const Vec3& vector);

  /**
  * @brief Gets a vector with the same direction which magnitude is 1.
  * @param vector Vector to normalize.
  * @returns Normalized vector.
  */
  Vec3 Vec3Normalize(const Vec3& vector);

  /**
  * @brief Gets the dot product from two vectors.
  * @param vector_1 Vector selected as multiplicand.
  * @param vector_2 Vector selected as multiplier.
  * @returns Dot product result.
  */
  float Vec3DotProduct(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Gets the dot product from two vectors.
  * @param vector_1 Vector selected as multiplicand.
  * @param vector_2 Vector selected as multiplier.
  * @returns Dot product result.
  */
  float operator*(const Vec3& vector_1, const Vec3& vector_2);


  /**
  * @brief Gets the angle (radians) between between two vectors .
  * @param vector_1 first vector.
  * @param vector_2 second vector.
  * @returns Angle in radians.
  */
  float Vec3Angle(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Gets the ortogonal projection of vector_1 on vector_2 .
  * @param vector_1 first vector.
  * @param vector_2 second vector.
  * @returns Projection.
  */
  float Vec3Projection(const Vec3& vector_1, const Vec3& vector_2);

  /*******************************************
  ********         3x3 MATRICES        *******
  *******************************************/
  /*****************************
  *****        INITS        ****
  *****************************/
  /**
  * @brief 3 Dimensions identity matrix.
  *
  *      1  0  0
  *      0  1  0
  *      0  0  1
  *
  * @returns Identity Mat3.
  */
  Mat3 Mat3Identity();

  /**
  * @brief 3 Dimensions matrix initialized using vectors as columns.
  *
  *      v1.x  v2.x  v3.x
  *      v1.y  v2.y  v3.y
  *      v1.z  v2.z  v3.z
  *
  * @returns Initialized Mat3.
  */
  Mat3 Mat3InitByColumns(const Vec3& column_1,
                         const Vec3& column_2,
                         const Vec3& column_3);

  /**
  * @brief 3 Dimensions matrix initialized element by element.
  *
  *      e0  e3  e6
  *      e1  e4  e7
  *      e2  e5  e8
  *
  * @returns Initialized Mat3.
  */
  Mat3 Mat3InitByElements(float element_0, float element_1, float element_2,
                          float element_3, float element_4, float element_5,
                          float element_6, float element_7, float element_8);

  /**
  * @brief 3 Dimensions translated matrix.
  *
  *      1  0  0
  *      0  1  0
  *      tx ty 1
  *
  * @param trans_x translation applied to X coordinate.
  * @param trans_y translation applied to Y coordinate.
  * @returns Translated Mat3.
  */
  Mat3 Mat3SetTranslation(const float trans_x, const float trans_y);

  /**
  * @brief 3 Dimensions rotated matrix.
  *
  *      cos   sin  0
  *      -sin  cos  0
  *      0     0    1
  *
  * @param radians Rotation angle (in radians) of the matrix.
  * @returns Rotated Mat3.
  */
  Mat3 Mat3SetRotation(const float radians);

  /**
  * @brief 3 Dimensions scaled matrix.
  *
  *      sx  0   0
  *      0   sy  0
  *      0   0   1
  *
  * @returns Scaled Mat3.
  */
  Mat3 Mat3SetScale(const float scale_x, const float scale_y);

  /*****************************
  *****      OPERATIONS     ****
  *****************************/
  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat3 Mat3MultiplyByScalar(const Mat3& matrix, const float scalar);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat3 operator*(const Mat3& matrix, const float scalar);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat3 operator*(const float scalar, const Mat3& matrix);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  void operator*=(Mat3& matrix, const float scalar);

  /**
  * @brief Calculate the determinant of a matrix.
  * @param matrix Matrix we want to multiply.
  * @returns Matrix determinant.
  */
  float Mat3Determinant(const Mat3& matrix);

  /**
  * @brief Calculates the cofactor value of a matrix 3x3 element.
  * @param matrix Matrix we want to use.
  * @param row Row of the matrix element we want to calculate its cofactor.
  * @param col Column of the Mat3 element.
  * @returns Element cofactor value.
  */
  float Mat3ElementCofactor(const Mat3& matrix,
                            const unsigned short int row,
                            const unsigned short int col);

  /**
  * @brief Calculates the matrix of cofactors.
  * @param matrix Matrix we want to calculate its cofactors.
  * @returns Mat3 of cofactors.
  */
  Mat3 Mat3Cofactors(const Mat3& matrix);

  /**
  * @brief Calculates the transpose of a matrix.
  * @param matrix Matrix we want to calculate its transpose.
  * @returns Transpose of a Mat3.
  */
  Mat3 Mat3Transpose(const Mat3& matrix);

  /**
  * @brief Calculates the adjugate of a matrix.
  * @param matrix Matrix we want to calculate its adjugate.
  * @returns Adjugate of a Mat3.
  */
  Mat3 Mat3Adjoint(const Mat3& matrix);

  /**
  * @brief Calculates the inverse of a matrix.
  * @param matrix Matrix we want to calculate its inverse
  * @returns inverse of a Mat3.
  */
  Mat3 Mat3Inverse(const Mat3& matrix);

  /**
  * @brief Adds two matrices.
  * @param matrix_1 Matrix selected as first addend.
  * @param matrix_2 Matrix selected as second addend.
  * @returns Addition result.
  */
  Mat3 Mat3Addition(const Mat3& matrix_1 , const Mat3& matrix_2);

  /**
  * @brief Adds two matrices.
  * @param matrix_1 Matrix selected as first addend.
  * @param matrix_2 Matrix selected as second addend.
  * @returns Addition result.
  */
  Mat3 operator+(const Mat3& matrix_1 , const Mat3& matrix_2);

  /**
  * @brief Adds two matrices (m1 = m1 + m2).
  * @param matrix_1 Matrix we want to modify it's value.
  * @param matrix_2 Matrix selected as second addend.
  */
  void operator+= (Mat3& matrix_1, const Mat3& matrix_2);

  /**
  * @brief Substract two matrices.
  * @param matrix_1 Matrix selected as minuend.
  * @param matrix_2 Matrix selected as subtrahend.
  * @returns Substraction result.
  */
  Mat3 operator-(const Mat3& matrix_1 , const Mat3& matrix_2);

  /**
  * @brief Substract two matrices (m1 = m1 - m2).
  * @param matrix_1 Matrix we want to modify it's value.
  * @param matrix_2 Matrix selected as second subtrahend.
  */
  void operator-= (Mat3& matrix_1, const Mat3& matrix_2);


  /*******************************************
  *****     2D VECTORS && MATRICES     *******
  *******************************************/
  /**
  * @brief Multiply a Mat3 and a Vec3. Useful to transform a point or vector.
  * @param vector Vector to transform.
  * @param matrix Matrix applied to the vector.
  * @returns Multiplication result, Vec3 transformed.
  */
  Vec3 Vec3Mat3Multiply(const Vec3& vector, const Mat3& matrix);

  /**
  * @brief Multiply a Mat3 and a Vec3. Useful to transform a point or vector.
  * @param vector Vector to transform.
  * @param matrix Matrix we want to multiply.
  * @returns Multiplication result, Vec3 transformed.
  */
  Vec3 operator*(const Vec3& vector, const Mat3& matrix);

  /**
  * @brief Multiply a Mat3 and a Vec3. Useful to transform a point or vector.
  * @param matrix Matrix we want to multiply.
  * @param vector Vector to transform.
  * @returns Multiplication result, Vec3 transformed.
  */
  Vec3 operator*(const Mat3& matrix, const Vec3& vector);

  /**
  * @brief Multiply a Mat3 and a Vec3. (vec = vec * mat).
  * @param vector Vector to transform.
  * @param matrix Matrix we want to multiply.
  * @returns Multiplication result, Vec3 transformed.
  */
  void operator*=(Vec3& vector, const Mat3& matrix);

  /**
  * @brief Multiply two matrices.
  *
  * In case that we are using them to acumulate transformations...
  * matrix_1 will be APPLIED FIRST, and matrix_2 AFTER FIRST.
  *
  * @param matrix_1 matrix we want to multiply. (FIRST TRANSFORMATION).
  * @param matrix_2 matrix we want to multiply.
  * @returns Multiplication result, Mat3 transformed.
  */
  Mat3 Mat3Multiply(const Mat3& matrix_1, const Mat3& matrix_2);

  /**
  * @brief Multiply two matrices.
  *
  * In case that we are using them to acumulate transformations...
  * matrix_1 will be APPLIED FIRST, and matrix_2 AFTER FIRST.
  *
  * @param matrix_1 matrix we want to multiply. (FIRST TRANSFORMATION).
  * @param matrix_2 matrix we want to multiply.
  * @returns Multiplication result, Mat3 transformed.
  */
  Mat3 operator*(const Mat3& matrix_1, const Mat3& matrix_2);

  /**
  * @brief Multiply two matrices. Adds a new transformation to matrix_1.
  *
  * In case that we are using them to acumulate transformations...
  * matrix_1 will be APPLIED FIRST, and matrix_2 AFTER FIRST.
  *
  * @param matrix_1 matrix we want to multiply. (FIRST TRANSFORMATION).
  * @param matrix_2 matrix we want to multiply.
  * @returns Multiplication result, Mat3 transformed.
  */
  void operator*= (Mat3& matrix_1, const Mat3& matrix_2);

  /**
  * @brief Applies or adds a rotation to a matrix .
  *
  * matrix transformations will be first, and then the rotation is added.
  *
  * @param matrix Matrix we want to apply the rotation.
  * @param radians Radians we want to rotate.
  * @returns Mat3 with the rotation transformation applied.
  */
  Mat3 Mat3Rotate(const Mat3& matrix, const float radians);

  /**
  * @brief Applies or adds a translation to a matrix .
  *
  * matrix transformations will be first, and then the translation is added.
  *
  * @param matrix Matrix we want to apply the translation.
  * @param trans_x translation applied to X coordinate.
  * @param trans_y translation applied to Y coordinate.
  * @returns Mat3 with the translation transformation applied.
  */
  Mat3 Mat3Translate(const Mat3& matrix, const float trans_x, const float trans_y);

  /**
  * @brief Applies or adds a scale to a matrix .
  *
  * matrix transformations will be first, and then the scale is added.
  *
  * @param matrix matrix we want to apply the scale.
  * @param scale_x Scale applied to X coordinate.
  * @param scale_y Scale applied to Y coordinate.
  * @returns Mat3 with the scale transformation applied.
  */
  Mat3 Mat3Scale(const Mat3& matrix, const float scale_x, const float scale_y);


  /*******************************************
  *****            2D FIGURES          *******
  *******************************************/

  /**
  * @brief Creates a regular polygon and return its vertex.
  *
  * @param num_vertex Number of vertex or sides.
  * @param radius Radius lenght (distance between the center and a vertex).
  * @returns Vec3 Pointer to the vertex array.
  */
  Vec3* GetRegularPolygon(const unsigned short int num_vertex,
                          const float radius = 1.0f);


  /***************************************************************************
  ***********************          3D METHODS         ************************
  ***************************************************************************/
  /*******************************************
  ********           VECTOR4           *******
  *******************************************/
  /*****************************
  *****        INITS        ****
  *****************************/

  /**
  * @brief Init a vector by components.
  *
  * POINT   ->   comp_w == 1
  * VECTOR  ->   comp_w == 0
  *
  * @param comp_x component x.
  * @param comp_y component y.
  * @param comp_z component z.
  * @param comp_z component w. Homogeneous coordinate.
  * @returns Initialized Vec4.
  */
  Vec4 Vec4InitByComponents(const float comp_x,
                            const float comp_y,
                            const float comp_z,
                            const float comp_w);

  /**
  * @brief Init a vector from two points.
  * @param origin Point designed as the origin of the new vector.
  * @param destiny Point designed as destiny.
  * @returns Initialized Vec4.
  */
  Vec4 Vec4InitFrom2Points(const Vec4& origin, const Vec4& destiny);


  /*****************************
  *****      OPERATIONS     ****
  *****************************/
  /**
  * @brief Adds two vectors.
  * @param vector_1 Vector selected as first addend.
  * @param vector_2 Vector selected as second addend.
  * @returns Addition result.
  */
  Vec4 Vec4Addition(const Vec4& vector_1, const Vec4& vector_2);

  /**
  * @brief Adds two vectors.
  * @param vector_1 Vector selected as first addend.
  * @param vector_2 Vector selected as second addend.
  * @returns Addition result.
  */
  Vec4 operator+ (const Vec4& vector_1, const Vec4& vector_2);

  /**
  * @brief Adds two vectors (v1 = v1 + v2).
  * @param vector_1 Vector we want to modify it's value.
  * @param vector_2 Vector selected as second addend.
  */
  void operator+= (Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Substracts two vectors (vector1 - vector2).
  * @param vector_1 Vector selected as minuend.
  * @param vector_2 Vector selected as subtrahend.
  * @returns Result of the operation.
  */
  Vec3 operator-(const Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Substracts two vectors (v1 = v1 - v2).
  * @param vector_1 Vector we want to modify it's value.
  * @param vector_2 Vector selected as second subtrahend.
  */
  void operator-= (Vec3& vector_1, const Vec3& vector_2);

  /**
  * @brief Gets the opposite of a vector.
  * @param vector Vector to calculate its opposite.
  * @returns Opposite Vec4.
  */
  Vec4 Vec4Opposite(const Vec4& vector);

  /**
  * @brief Gets the opposite of a vector.
  * @param vector Vector to calculate its opposite.
  * @returns Opposite Vec4.
  */
  Vec4 operator-(const Vec4& vector);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec4 Vec4MultiplyByScalar(const Vec4& vector, const float scalar);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec4 operator*(const Vec4& vector, const float scalar);

  /**
  * @brief Multiply all the components of the vector by a scalar number.
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec4 operator*(const float scalar, const Vec4& vector);

  /**
  * @brief Multiplies the vector components by a scalar number. (v = v * scalar)
  * @param vector Vector we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  void operator*=(Vec4& vector, const float scalar);

  /**
  * @brief Divides all the components of the vector by a scalar number.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec4 operator/(const Vec4& vector, const float scalar);

  /**
  * @brief Divides scalar/vector.x, scalar/vector.y, scalar/vector.z.
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  Vec4 operator/(const float scalar, const Vec4& vector);

  /**
  * @brief Divides the vector components by a scalar number. (v = v / scalar).
  * @param vector Vector we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Vector Result of the operation.
  */
  void operator/=(Vec4& vector, const float scalar);

  /**
  * @brief Homogenizes a vector.
  * @param vector Vector to homogenize.
  * @returns homogeneous Vec4.
  */
  Vec4 Vec4Homogenize(const Vec4& vector);

  /**
  * @brief Gets the middle point between two points.
  * @param point_1 First point.
  * @param point_2 Second point.
  * @returns Mid point.
  */
  Vec4 Vec4MidPoint(const Vec4& point_1, const Vec4& point_2);

  /**
  * @brief Calculates the magnitude of a vector.
  * @param vector Vector to calculate it's magnitude.
  * @returns Vector magnitude.
  */
  float Vec4Magnitude(const Vec4& vector);

  /**
  * @brief Calculates de cross product between two vec4.
  *
  * Gets a vector that is perpendicular to both
  * and therefore normal to the plane containing them.
  *
  * @param vector Vector to normalize.
  * @returns Normalized vector.
  */
  Vec4 Vec4CrossProduct(const Vec4& vector_1, const Vec4& vector_2);

  /**
  * @brief Gets a vector with the same direction which magnitude is 1.
  * @param vector_1 Vector selected as miltiplicand.
  * @param vector_2 Vector selected as multiplier.
  * @returns Perpendicular Vec4.
  */
  Vec4 Vec4Normalize(const Vec4& vector);

  /**
  * @brief Gets the dot product from two vectors.
  * @param vector_1 Vector selected as multiplicand.
  * @param vector_2 Vector selected as multiplier.
  * @returns Dot product result.
  */
  float Vec4DotProduct(const Vec4& vector_1, const Vec4& vector_2);

  /**
  * @brief Gets the dot product from two vectors.
  * @param vector_1 Vector selected as multiplicand.
  * @param vector_2 Vector selected as multiplier.
  * @returns Dot product result.
  */
  float operator*(const Vec4& vector_1, const Vec4& vector_2);

  /**
  * @brief Gets the angle (radians) between between two 3D vectors .
  * @param vector_1 first vector.
  * @param vector_2 second vector.
  * @returns Angle in radians.
  */
  float Vec4Angle(const Vec4& vector_1, const Vec4& vector_2);


  /*******************************************
  ********         4x4 MATRICES        *******
  *******************************************/
  /*****************************
  *****        INITS        ****
  *****************************/
  /**
  * @brief 4 Dimensions identity matrix.
  *
  *      1  0  0  0
  *      0  1  0  0
  *      0  0  1  0
  *      0  0  0  1
  *
  * @returns Identity Mat4.
  */
  Mat4 Mat4Identity();

  /**
  * @brief 4 Dimensions matrix initialized using vectors as columns.
  *
  *      v1.x  v2.x  v3.x  v4.x
  *      v1.y  v2.y  v3.y  v4.y
  *      v1.z  v2.z  v3.z  v4.z
  *      v1.w  v2.w  v3.w  v4.w
  *
  * @returns Initialized Mat4.
  */
  Mat4 Mat4InitByColumns(const Vec4& column_1,
                         const Vec4& column_2,
                         const Vec4& column_3,
                         const Vec4& column_4);

  /**
  * @brief 4 Dimensions matrix initialized element by element.
  *
  *      e0   e4   e8   e12
  *      e1   e5   e9   e13
  *      e2   e6   e10  e14
  *      e3   e7   e11  e15
  *
  * @returns Initialized Mat4.
  */
  Mat4 Mat4InitByElements(const float element_0, const float element_1,
                          const float element_2, const float element_3,
                          const float element_4, const float element_5,
                          const float element_6, const float element_7,
                          const float element_8, const float element_9,
                          const float element_10, const float element_11,
                          const float element_12, const float element_13,
                          const float element_14, const float element_15);

  /**
  * @brief 4 Dimensions translated matrix.
  *
  *      1  0  0  0
  *      0  1  0  0
  *      0  0  1  0
  *      tx ty tz 0
  *
  * @param trans_x translation applied to X coordinate.
  * @param trans_y translation applied to Y coordinate.
  * @param trans_z translation applied to Z coordinate.
  * @returns Translated Mat4.
  */
  Mat4 Mat4SetTranslation(const float trans_x,
                          const float trans_y,
                          const float trans_z);

  /**
  * @brief 4 Dimensions scaled matrix.
  *
  *      sx  0   0   0
  *      0   sy  0   0
  *      0   0   sz  0
  *      0   0   0   1
  *
  * @returns Scaled Mat4.
  */
  Mat4 Mat4SetScale(const float scale_x,
                    const float scale_y,
                    const float scale_z);


  /**
  * @brief 4 Dimensions matrix rotated around X axis.
  *
  *     1	   0	   0	  0
  *	    0	  cos	 -sin 	0
  *	    0	  sin	  cos	  0
  *	    0    0	   0  	1
  *
  * @returns X rotated Mat4.
  */
  Mat4 Mat4SetRotationX(const float radians);

  /**
  * @brief 4 Dimensions matrix rotated around Y axis.
  *
  *	  cos   0	  sin	  0
  *	   0    1	   0	  0
  *	 -sin   0 	cos	  0
  *    0    0	   0	  1
  *
  * @returns Y rotated Mat4.
  */
  Mat4 Mat4SetRotationY(const float radians);

  /**
  * @brief 4 Dimensions matrix rotated around Z axis.
  *
  *	  cos  -sin	   0	  0
  *	  sin   cos	   0	  0
  *	   0     0 	   1	  0
  *    0     0	   0	  1
  *
  * @returns Y rotated Mat4.
  */
  Mat4 Mat4SetRotationZ(const float radians);

  /**
  * @brief Transform a 3D object to create a 2D projection. Useful to draw in 2D
  *
  *	   1    0	   0	  0
  *	   0    1	   0	  0
  *	   0    0 	 1	  1
  *    0    0	   0    0
  *
  * @returns ProjectedMat4
  */
  Mat4 Mat4SetProjection3Dto2D();

  /*****************************
  *****      OPERATIONS     ****
  *****************************/
  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat4 Mat4MultiplyByScalar(const Mat4& matrix, const float scalar);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat4 operator*(const Mat4& matrix, const float scalar);

  /**
  * @brief Multiply all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat4 operator*(const float scalar, const Mat4& matrix);

  /**
  * @brief Multiply the matrix elements by a scalar number. (m = m * scalar)
  * @param matrix Matrix we want to multiply.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  void operator*=(Mat4& matrix, const float scalar);

  /**
  * @brief Divide all the elements of the matrix by a scalar number.
  * @param matrix Matrix we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  Mat4 operator/(const Mat4& matrix, const float scalar);

  /**
  * @brief Divide the matrix elements by a scalar number. (m = m / scalar)
  * @param matrix Matrix we want to divide.
  * @param scalar Number chosen for the operation.
  * @returns Matrix Result of the operation.
  */
  void operator/=(const Mat4& matrix, const float scalar);

  /**
  * @brief Calculate the determinant of a matrix.
  * @param matrix Matrix we want to multiply.
  * @returns Matrix determinant.
  */
  float Mat4Determinant(const Mat4& matrix);

  /**
  * @brief Calculates the cofactor value of a matrix 4x4 element.
  * @param matrix Matrix we want to use.
  * @param row Row of the matrix element we want to calculate its cofactor.
  * @param col Column of the Mat4 element.
  * @returns Element cofactor value.
  */
  float Mat4ElementCofactor(const Mat4& matrix,
                            const unsigned short int row,
                            const unsigned short int col);

  /**
  * @brief Calculates the matrix of cofactors.
  * @param matrix Matrix we want to calculate its cofactors.
  * @returns Mat4 of cofactors.
  */
  Mat4 Mat4Cofactors(const Mat4& matrix);

  /**
  * @brief Calculates the transpose of a matrix.
  * @param matrix Matrix we want to calculate its transpose.
  * @returns Transpose of a Mat4.
  */
  Mat4 Mat4Transpose(const Mat4& matrix);

  /**
  * @brief Calculates the adjugate of a matrix.
  * @param matrix Matrix we want to calculate its adjugate.
  * @returns Adjugate of a Mat4.
  */
  Mat4 Mat4Adjoint(const Mat4& matrix);

  /**
  * @brief Calculates the inverse of a matrix.
  * @param matrix Matrix we want to calculate its inverse
  * @returns inverse of a Mat4.
  */
  Mat4 Mat4Inverse(const Mat4& matrix);

  /**
  * @brief Adds two matrices.
  * @param matrix_1 Matrix selected as first addend.
  * @param matrix_2 Matrix selected as second addend.
  * @returns Addition result.
  */
  Mat4 Mat4Addition(const Mat4& matrix_1 , const Mat4& matrix_2);

  /**
  * @brief Adds two matrices.
  * @param matrix_1 Matrix selected as first addend.
  * @param matrix_2 Matrix selected as second addend.
  * @returns Addition result.
  */
  Mat4 operator+(const Mat4& matrix_1 , const Mat4& matrix_2);

  /**
  * @brief Adds two matrices. (m1 = m1 + m2)
  * @param matrix_1 Matrix selected as first addend.
  * @param matrix_2 Matrix selected as second addend.
  * @returns Addition result.
  */
  void operator+=(Mat4& matrix_1 , const Mat4& matrix_2);

  /**
  * @brief Substract two matrices.
  * @param matrix_1 Matrix selected as first minuend.
  * @param matrix_2 Matrix selected as second subtrahend.
  * @returns Operation result.
  */
  Mat4 Mat4Substract(const Mat4& matrix_1 , const Mat4& matrix_2);

  /**
  * @brief Substract two matrices.
  * @param matrix_1 Matrix selected as first minuend.
  * @param matrix_2 Matrix selected as second subtrahend.
  * @returns Operation result.
  */
  Mat4 operator-(const Mat4& matrix_1 , const Mat4& matrix_2);

  /**
  * @brief Substract two matrices. (m1 = m1 - m2)
  * @param matrix_1 Matrix selected as first minuend.
  * @param matrix_2 Matrix selected as second subtrahend.
  * @returns Operation result.
  */
  void operator-=(Mat4& matrix_1 , const Mat4& matrix_2);

  /*******************************************
  *****     3D VECTORS && MATRICES     *******
  *******************************************/
  /**
  * @brief Multiply a Mat4 and a Vec4. Useful to transform a point or vector.
  * @param vector Vector to transform.
  * @param matrix Matrix applied to the vector.
  * @returns Multiplication result, Vec3 transformed.
  */
  Vec4 Vec4Mat4Multiply(const Vec4& vector, const Mat4& matrix);

  /**
  * @brief Multiply a Mat4 and a Vec4. Useful to transform a point or vector.
  * @param vector Vector to transform.
  * @param matrix Matrix applied to the vector.
  * @returns Multiplication result, Vec3 transformed.
  */
  Vec4 operator*(const Vec4& vector, const Mat4& matrix);

  /**
  * @brief Multiply a Mat4 and a Vec4. Useful to transform a point or vector.
  * @param vector Vector to transform.
  * @param matrix Matrix applied to the vector.
  * @returns Multiplication result, Vec3 transformed.
  */
  Vec4 operator*(const Mat4& matrix, const Vec4& vector);

  /**
  * @brief Multiply a Mat4 and a Vec4. (vec = vec * mat).
  * @param vector Vector to transform.
  * @param matrix Matrix applied to the vector.
  * @returns Multiplication result, Vec3 transformed.
  */
  void operator*=(Vec4& vector, const Mat4& matrix);

  /**
  * @brief Multiply two matrices.
  *
  * In case that we are using them to acumulate transformations...
  * matrix_1 will be APPLIED FIRST, and matrix_2 AFTER FIRST.
  *
  * @param matrix_1 matrix we want to multiply. (FIRST TRANSFORMATION).
  * @param matrix_2 matrix we want to multiply.
  * @returns Multiplication result, Mat4 transformed.
  */
  Mat4 Mat4Multiply(const Mat4& matrix_1, const Mat4& matrix_2);

  /**
  * @brief Multiply two matrices.
  *
  * In case that we are using them to acumulate transformations...
  * matrix_1 will be APPLIED FIRST, and matrix_2 AFTER FIRST.
  *
  * @param matrix_1 matrix we want to multiply. (FIRST TRANSFORMATION).
  * @param matrix_2 matrix we want to multiply.
  * @returns Multiplication result, Mat4 transformed.
  */
  Mat4 operator*(const Mat4& matrix_1, const Mat4& matrix_2);

  /**
  * @brief Multiply two matrices. m1 = m1 * m2
  *
  * In case that we are using them to acumulate transformations...
  * matrix_1 will be APPLIED FIRST, and matrix_2 AFTER FIRST.
  *
  * @param matrix_1 matrix we want to multiply. (FIRST TRANSFORMATION).
  * @param matrix_2 matrix we want to multiply.
  * @returns Multiplication result, Mat4 transformed.
  */
  void operator*=(Mat4& matrix_1, const Mat4& matrix_2);

  /**
  * @brief Applies or adds a triple axis rotation to a matrix.
  *
  * matrix transformations will be first, and then the rotation is added.
  * order of the rotations will be:  X -> Y -> Z.
  *
  * @param matrix Matrix we want to apply the rotation.
  * @param radians_x Radians we want to rotate around the X axis.
  * @param radians_y Radians we want to rotate around the Y axis.
  * @param radians_z Radians we want to rotate around the Z axis.
  * @returns Mat4 with the rotation transformation applied.
  */
  Mat4 Mat4Rotate(const Mat4& matrix,
                  const float radians_x,
                  const float radians_y,
                  const float radians_z );

  /**
  * @brief Applies or adds a rotation around the X axis.
  *
  * matrix transformations will be first, and then the rotation is added.
  *
  * @param matrix Matrix we want to apply the rotation.
  * @param radians Radians we want to rotate around the X axis.
  * @returns Mat4 with the rotation transformation applied.
  */
  Mat4 Mat4RotateX(const Mat4& matrix, const float radians);

  /**
  * @brief Applies or adds a rotation around the Y axis.
  *
  * matrix transformations will be first, and then the rotation is added.
  *
  * @param matrix Matrix we want to apply the rotation.
  * @param radians Radians we want to rotate around the Y axis.
  * @returns Mat4 with the rotation transformation applied.
  */
  Mat4 Mat4RotateY(const Mat4& matrix, const float radians);

  /**
  * @brief Applies or adds a rotation around the Z axis.
  *
  * matrix transformations will be first, and then the rotation is added.
  *
  * @param matrix Matrix we want to apply the rotation.
  * @param radians Radians we want to rotate around the Z axis.
  * @returns Mat4 with the rotation transformation applied.
  */
  Mat4 Mat4RotateZ(const Mat4& matrix, const float radians);

  /**
  * @brief Applies or adds a translation to a matrix .
  *
  * matrix transformations will be first, and then the translation is added.
  *
  * @param matrix Matrix we want to apply the translation.
  * @param trans_x translation applied to X coordinate.
  * @param trans_y translation applied to Y coordinate.
  * @param trans_z translation applied to Z coordinate.
  * @returns Mat4 with the translation transformation applied.
  */
  Mat4 Mat4Translate(const Mat4& matrix,
                     const float trans_x,
                     const float trans_y,
                     const float trans_z );

  /**
  * @brief Applies or adds a scale to a matrix .
  *
  * matrix transformations will be first, and then the scale is added.
  *
  * @param matrix matrix we want to apply the scale.
  * @param scale_x Scale applied to X coordinate.
  * @param scale_y Scale applied to Y coordinate.
  * @param scale_z Scale applied to Z coordinate.
  * @returns Mat4 with the scale transformation applied.
  */
  Mat4 Mat4Scale(const Mat4& matrix,
                 const float scale_x,
                 const float scale_y,
                 const float scale_z );

  /**
  * @brief Applies or adds an uniform scale to a matrix.
  *
  * matrix transformations will be first, and then the scale is added.
  *
  * @param matrix matrix we want to apply the scale.
  * @param scale Scale applied to all the coordinates.
  * @returns Mat4 with the scale transformation applied.
  */
  Mat4 Mat4UniformScale(const Mat4& matrix, const float scale);

  /**
  * @brief Applies or adds a 3D to 2D projection to a matrix ..
  *
  * This projection is used to transform a 3D point to its 2D projection, so
  * we can paint it simulating a 3D in a 2D surface.
  * After that is necessary to homogenize the vectors.
  *
  * @param matrix Mat4 we want to apply the projection.
  * @returns Mat4 with the projection applied.
  */
  Mat4 Mat4ProjectTo2D(const Mat4& matrix);



  /*******************************************
  *****            DRAW FIGURES          *****
  *******************************************/

  /**
  * @brief Draws an extruded shape based on some Vec2 vertex.
  *
  * Creates and saves the figure vertex in temporal Vec4, deleting them after
  * drawing the extruded shape.
  *
  * @param vertex Vec2 array of vertex that will be used as the base.
  * @param num_vertex length of the vertex array.
  * @param transform Mat4 transformation we want to apply to the figure.
  * @param height size of the figure.
  */
  void Vec2DrawExtrudedShape(Vec2* vertex,
                             const unsigned short int num_vertex,
                             const Mat4& transform,
                             const float height = 1.0f);

  /**
  * @brief Draws a surface revolution figure based on some Vec2 vertex.
  *
  * Creates and saves the figure vertex in temporal Vec4, deleting them after
  * drawing the figure. Revolution is applied around Y axis.
  *
  * @param vertex Vec2 array of vertex that will be used as the base.
  * @param num_vertex length of the vertex array.
  * @param transform Mat4 transformation we want to apply to the figure.
  * @param divisions number of portions or divisions applied.
  */
  void Vec2DrawSurfaceRevolution(Vec2* vertex,
                                const unsigned short int num_vertex,
                                const Mat4& transform,
                                const unsigned short int divisions = 10);


  /**
  * @brief Draws an extruded shape based on some Vec3 vertex.
  *
  * Creates and saves the figure vertex in temporal Vec4, deleting them after
  * drawing the extruded shape.
  *
  * @param vertex Vec3 array of vertex that will be used as the base.
  * @param num_vertex length of the vertex array.
  * @param transform Mat4 transformation we want to apply to the figure.
  * @param height size of the figure.
  */
  void Vec3DrawExtrudedShape(Vec3* vertex,
                             const unsigned short int num_vertex,
                             const Mat4& transform,
                             const float height = 1.0f);


  /**
  * @brief Draws a surface revolution figure based on some Vec3 vertex.
  *
  * Creates and saves the figure vertex in temporal Vec4, deleting them after
  * drawing the figure. Revolution is applied around Y axis.
  *
  * @param vertex Vec3 array of vertex that will be used as the base.
  * @param num_vertex length of the vertex array.
  * @param transform Mat4 transformation we want to apply to the figure.
  * @param divisions number of portions or divisions applied.
  */
  void Vec3DrawSurfaceRevolution(Vec3* vertex,
                                const unsigned short int num_vertex,
                                const Mat4& transform,
                                const unsigned short int divisions = 10);

  /**
  * @brief Draws an extruded shape based on some Vec4 vertex.
  *
  * Creates and saves the figure vertex in temporal Vec4, deleting them after
  * drawing the extruded shape.
  *
  * @param vertex Vec4 array of vertex that will be used as the base.
  * @param num_vertex length of the vertex array.
  * @param transform Mat4 transformation we want to apply to the figure.
  * @param height size of the figure.
  */
  void Vec4DrawExtrudedShape(Vec4* vertex,
                             const unsigned short int num_vertex,
                             const Mat4& transform,
                             const float height = 1.0f);


  /**
  * @brief Draws a surface revolution figure based on some Vec4 vertex.
  *
  * Creates and saves the figure vertex in temporal Vec4, deleting them after
  * drawing the figure. Revolution is applied around Y axis.
  *
  * @param vertex Vec4 array of vertex that will be used as the base.
  * @param num_vertex length of the vertex array.
  * @param transform Mat4 transformation we want to apply to the figure.
  * @param divisions number of portions or divisions applied.
  */
  void Vec4DrawSurfaceRevolution(Vec4* vertex,
                                const unsigned short int num_vertex,
                                const Mat4& transform,
                                const unsigned short int divisions = 10);

  /*****************************
  *****        PRINT        ****
  *****************************/
  void PrintMat2(const Mat2& matrix);
  void PrintMat3(const Mat3& matrix);
  void PrintMat4(const Mat4& matrix);
  void PrintVec2(const Vec2& vector);
  void PrintVec3(const Vec3& vector);
  void PrintVec4(const Vec4& vector);


};

#endif

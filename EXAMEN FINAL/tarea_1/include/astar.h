/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __ASTAR_H__
#define __ASTAR_H__ 1

#include "path.h"
#include "jmath.h"
#include "cost_map.h"

class AStar {
public:

/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/
  AStar();
  ~AStar();

	bool generatePath(const JMATH::Vec2 origin, 
                    const JMATH::Vec2 destination, 
                    CostMap* map, 
                    Path* path);

  /**
  * @brief Gets all the useful node successors, nonwalkable won't be get.
  *
  * Only useful neighbors will be saved in the list. For Example:
  * if total = 3,  only neighbor_list[0, 1 and 2] will be saved.
  * to precalculate g costs. G will be set with 3 values depending on their cost:
  * 1 - Horizontal cost.    2 - Vertical cost.   3 - Diagonal cost.
  *
  * @param node Current node to find its neighbors.
  * @param map CostMap where the node belongs.
  * @param neighbor_list Array where we will save the useful neighbors.
  * @return Number of useful node saved in the neighbor pointer.
  */
  int getUsefulNodeSuccessors(CellInfo* node, 
                              CostMap* map, 
                              CellInfo neighbor_list[8]);
	
	bool preProcess(CostMap* map);

	bool generatePath(const JMATH::Vec2 origin, 
                    const JMATH::Vec2 destination, 
                    Path* path);
  
};

#endif
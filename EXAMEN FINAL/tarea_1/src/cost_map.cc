/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "cost_map.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "gamemanager.h"


/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

CostMap::CostMap() {
  grid_size_ = { 0, 0 };
  cell_size_ = { 0.0f, 0.0f };
}

CostMap::~CostMap() {
  cost_map_vector_.clear();
}

bool CostMap::load(const char* cost_map_filename) {

  if (strlen(cost_map_filename) <= 5) {
    printf(" ERROR: Filename \"%s\" is too short.\n", cost_map_filename);
    return false;
  }

  const char* file_extension = cost_map_filename + strlen(cost_map_filename) - 4;

  if (!strncmp(file_extension, ".txt", 4)) {
    return parseText(cost_map_filename);
  }
  
  if (!strncmp(file_extension, ".png", 4) ||
           !strncmp(file_extension, ".bmp", 4)) {
    return parseImage(cost_map_filename);
  }
}

bool CostMap::parseText(const char* filename) {

  auto& game = Game::instance();
  std::ifstream file(filename);

  if (file.is_open()) {

    grid_size_ = { 0, 0 };
    cell_size_ = { 0.0f, 0.0f };

    std::string text((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    int map_text_length = 0;
    for (unsigned int i = 0; i < text.length(); i++) {
      printf("%c", text.c_str()[i]);
      if (text.c_str()[i] == '*' || text.c_str()[i] == ' ') {
        map_text_length += 1;
      }
      else {
        grid_size_.y += 1;
      }
    }

    grid_size_.x = map_text_length / grid_size_.y;
    printf(" grid size = %d x %d", grid_size_.x, grid_size_.y);
    cell_size_ = { (float)game.window_width / (float)grid_size_.x,
                   (float)game.window_height / (float)grid_size_.y };

    cost_map_vector_.resize(map_text_length);
    JMATH::Vec2 initial_position = { cell_size_.x * 0.5f, cell_size_.y * 0.5f };

    int vector_index = 0;
    for (unsigned int i = 0; i < text.length(); i++) { 
      if (text.c_str()[i] == '*') {
        cost_map_vector_[vector_index].is_walkable = false;
        vector_index += 1;
      }
      else if (text.c_str()[i] == ' ') {
        cost_map_vector_[vector_index].is_walkable = true;
        vector_index += 1;
      }
      cost_map_vector_[i].position = { initial_position.x + cell_size_.x * (i % grid_size_.x),
                                     initial_position.y + cell_size_.y * (i / grid_size_.x) };
    }

    file.close();
    return true;
  }
  
  printf(" ERROR: File \"%s\" doesnt exists.\n", filename); 
  return false; 

}

bool CostMap::parseImage(const char* filename) {

  auto& game = Game::instance();
  Sprite cost_map_image;
  unsigned char color[4];

  cost_map_image.init(filename);
  grid_size_ = { cost_map_image.width(), cost_map_image.height() };
  cost_map_vector_.resize(grid_size_.x * grid_size_.y);

  printf(" grid size = %d x %d", grid_size_.x, grid_size_.y);
  cell_size_ = { (float)game.window_width / (float)grid_size_.x,
                  (float)game.window_height / (float)grid_size_.y };

  int index = 0;
  JMATH::Vec2 initial_position = { cell_size_.x * 0.5f, cell_size_.y * 0.5f };
  for (int i = 0; i < grid_size_.y; i++) {
    for (int j = 0; j < grid_size_.x; j++) {
      ESAT::SpriteGetPixel(cost_map_image.handle(), j, i, color);
      if (color[0] < 128) {
        cost_map_vector_[index].is_walkable = false;
      }
      else {
        cost_map_vector_[index].is_walkable = true;
      }
      cost_map_vector_[index].position = { initial_position.x + cell_size_.x * (index % grid_size_.x),
                                         initial_position.y + cell_size_.y * (index / grid_size_.x) };
      index++;
    }
  }

  return true;
}

void CostMap::draw() {

  auto& game = Game::instance();

  for (int i = 0; i < grid_size_.y * grid_size_.x; i++) {

    if (cost_map_vector_[i].is_walkable) {
      DrawRect(cost_map_vector_[i].position, 255, 255, 0, 40, { cell_size_.x, cell_size_.y });
    }
    else {
      DrawRect(cost_map_vector_[i].position, 0, 0, 0, 40, { cell_size_.x, cell_size_.y });
    }
  }
}

JMATH::Vec2i CostMap::getIndicesByPosition(const JMATH::Vec2 position) {
  return { (int)(position.x / cell_size_.x) , (int)(position.y / cell_size_.y) };
}

int CostMap::getMapVectorIndexByPosition(const JMATH::Vec2 position) {
  JMATH::Vec2i indices = getIndicesByPosition(position);
  return (indices.x + grid_size_.x * indices.y);
}

JMATH::Vec2 CostMap::getCellCenterPositionByIndex(const JMATH::Vec2i index) {
  return { index.x * cell_size_.x + cell_size_.x * 0.5f,
           index.y * cell_size_.y + cell_size_.y * 0.5f, };
}



/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "path.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include "gamemanager.h"


/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

Path::Path() {
  current_point_ = 0;
}


Path::~Path() {
  path_vector_.clear();
}


void Path::draw() {
  for (int i = 0; i < path_vector_.size(); ++i) {
    DrawRect(path_vector_[i], 0, 255, 0, 255, Game::instance().cost_map_.cell_size_);
  }
}

JMATH::Vec2 Path::getNextPoint() {
  if (current_point_ >= path_vector_.size()) {
    return path_vector_[current_point_ + 1];
  }
  return { -999999.9f, -999999.9f };
}


int Path::length() {
  return path_vector_.size();
}

bool Path::addPoint(const JMATH::Vec2 position) {
  path_vector_.push_back(position);
  return true;
}




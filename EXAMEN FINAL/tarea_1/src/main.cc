#include "ESAT_SDK/window.h"
#include "ESAT_SDK/input.h"
#include "ESAT_SDK/draw.h"
#include "ESAT_SDK/time.h"
#include "gamemanager.h"
#include "jmath.h"
#include "astar.h"
#include <string>
#include <time.h>


/**
* @brief Render a text.
* @param text Text to render.
* @param position Vector where the text will be rendered.
* @param size Size of the text.
* @param r Red color component of the text.
* @param g Green color component of the text.
* @param b Blue color component of the text.
*/
void DrawText(const char* text, JMATH::Vec2 position, float size = 30, unsigned char r = 0, unsigned char g = 0, unsigned char b = 0) {
  ESAT::DrawSetStrokeColor(r, g, b, 255);
  ESAT::DrawSetFillColor(r, g, b, 255);
  ESAT::DrawSetTextSize(size);
  ESAT::DrawSetTextBlur(0.0f);
  ESAT::DrawText(position.x, position.y, text);
}


/// @brief Input function of the application.
void Input() {
  Game::instance().end_program = !ESAT::WindowIsOpened() || 
                                 ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
  Game::instance().input_mouse_button_up = ESAT::MouseButtonUp(0);
}

/// @brief Update function of the application.
void Update(double delta_time) {
  auto& game = Game::instance();

  if (game.input_mouse_button_up && !game.origin_set) {
    int index = 0;
    index = game.cost_map_.getMapVectorIndexByPosition({ (float)ESAT::MousePositionX(),
                                                       (float)ESAT::MousePositionY() });
    game.origin = game.cost_map_.cost_map_vector_[index].position;
    game.origin_set = true;
    game.input_mouse_button_up = false;
    game.path_.path_vector_.clear();
  }

  if (game.input_mouse_button_up && !game.goal_set) {
    int index = 0;
    index = game.cost_map_.getMapVectorIndexByPosition({ (float)ESAT::MousePositionX(),
      (float)ESAT::MousePositionY() });
    game.goal = game.cost_map_.cost_map_vector_[index].position;
    game.goal_set = true;
    game.input_mouse_button_up = false;
  }

  if (game.goal_set && game.origin_set) {
    game.astar_.generatePath(game.origin, game.goal, &game.cost_map_, &game.path_);
    game.goal_set = false;
    game.origin_set = false;
  }

}

/// @brief Render of the scene.
void DrawScene() {
  auto& game = Game::instance();
  game.background_image_.draw();
  game.cost_map_.draw();
  game.path_.draw();
}


/// @brief Render function of the application.
void Draw() {
  auto& game = Game::instance();
  ESAT::DrawBegin();
  ESAT::DrawClear(60, 60, 60);

  DrawScene();

  ESAT::DrawEnd();
  ESAT::WindowFrame();
}

/// @brief Init function of the application.
void Init() {
  auto& game = Game::instance();

  game.window_width = 1200;
  game.window_height = 880;
  ESAT::WindowInit(game.window_width, game.window_height);
  ESAT::WindowSetMouseVisibility(true);
  ESAT::DrawSetTextFont("../data/fonts/DigitFont.TTF");
  //ESAT::DrawSetTextBlur(true);
  /* TESTEO CON MAPA DE COSTES POR TEXTO.
  game.cost_map_.load("./../data/map_12_cost.txt");
  game.background_image_.init("./../data/map_12_terrain.png");
  */
  /* TESTEOS CON MAPA DE COSTES POR IMAGEN.
  game.background_image_.init("./../data/map_03_60x44_bw.png");
  game.cost_map_.load("./../data/map_03_960x704_bw.png");
  game.cost_map_.load("./../data/map_03_60x44_bw.png");
  game.background_image_.init("./../data/map_03_960x704_color.png");
  game.background_image_.init("./../data/map_02_terrain.png");
  game.cost_map_.load("./../data/map_02_cost.png");
  */
  game.background_image_.init("./../data/map_03_960x704_layoutAB.png");
  game.cost_map_.load("./../data/map_03_120x88_bw.png");
  game.background_image_.init();
  game.background_image_.set_scale((float)game.window_width / (float)game.background_image_.width(),
                                   (float)game.window_height / (float)game.background_image_.height());

}

/// @brief Start function of the application.
void Start() {
  auto& game = Game::instance();
  srand(time(NULL));
  game.time_step = 16.6;

  double current_time = ESAT::Time(); 

  while (!game.end_program) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= game.time_step) {

      Update(game.time_step);

      current_time += game.time_step;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }

}

/// @brief Finish function of the application.
void Finish() {
  ESAT::WindowDestroy();
}

/// @brief Main function of the application.
int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica6_PathFinding
*  @brief IA PathFinding AStar
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include <stdio.h>
#include "sprite.h"

#include "ESAT_SDK/sprite.h"

/****************************************
***    CONSTRUCTORS AND DESTRUCTOR    ***
****************************************/
Sprite::Sprite() {
  handle_ = nullptr;
  ESAT::SpriteTransformInit(&transform_);
  init_from_file_ = false;
}

Sprite::Sprite(const Sprite& copy) {
  handle_ = copy.handle_;
  transform_ = copy.transform_;
}

Sprite::~Sprite(){
  if(handle_ && init_from_file_){
  ESAT::SpriteRelease(handle_);
  }
}

void Sprite::init(const char* filename,
                  Origin origin,
                  const float posX,
                  const float posY,
                  const float scaleX,
                  const float scaleY,
                  const float angle) {

  handle_ = ESAT::SpriteFromFile(filename);
  set_origin(origin);
  set_position(posX, posY);
  set_scale(scaleX, scaleY);
  transform_.angle = angle;
  init_from_file_ = true;
}

void Sprite::init(ESAT::SpriteHandle handle,
                  Origin origin,
                  const float posX,
                  const float posY,
                  const float scaleX,
                  const float scaleY,
                  const float angle) {

  handle_ = handle;
  set_origin(origin);
  set_position(posX, posY);
  set_scale(scaleX, scaleY);
  transform_.angle = angle;

}

void Sprite::init(Origin origin,
                  const float posX,
                  const float posY,
                  const float scaleX,
                  const float scaleY,
                  const float angle) {

  set_origin(origin);
  set_position(posX, posY);
  set_scale(scaleX, scaleY);
  transform_.angle = angle;

}


/****************************************
***        SETTERS AND GETTERS        ***
****************************************/
ESAT::SpriteHandle Sprite::handle() {
	return handle_;
}
ESAT::SpriteTransform Sprite::transform() {
	return transform_;
}

int Sprite::width() {
  return (ESAT::SpriteWidth(handle_) * transform_.scale_x);
}
int Sprite::height() {
  return (ESAT::SpriteHeight(handle_) * transform_.scale_y);
}

void Sprite::set_origin(Origin origin) {
  switch(origin){
    case kOrigin_UpperLeft:{
      set_origin(0.0f,0.0f);
    }break;
    case kOrigin_UpperRight:{
      set_origin(width(),0.0f);
    }break;
    case kOrigin_LowerLeft:{
      set_origin(0.0f,height());
    }break;
    case kOrigin_LowerRight:{
      set_origin(width(),height());
    }break;
    case kOrigin_Center:{
      set_origin(width()/2.0f,height()/2.0f);
    }break;
    case kOrigin_Up:{
      set_origin(width()/2,0.0f);
    }break;
    case kOrigin_Down:{
      set_origin(width()/2.0f,height());
    }break;
    case kOrigin_Left:{
      set_origin(0.0f,height()/2.0f);
    }break;
    case kOrigin_Right:{
      set_origin(width(),height()/2.0f);
    }break;
  }
}

void Sprite::set_position(const float x, const float y) {
  transform_.x = x;
  transform_.y = y;
}
void Sprite::set_origin(const float x, const float y) {
  transform_.sprite_origin_x = x;
  transform_.sprite_origin_y = y;
}
void Sprite::set_scale(const float x, const float y) {
  transform_.scale_x = x;
  transform_.scale_y = y;
}
void Sprite::rotate(const float angle){
  transform_.angle += angle;
  if (transform_.angle > 360.0f){
    transform_.angle -= 360.0f;
  }
  else if (transform_.angle < 0.0f){
    transform_.angle += 360.0f;
  }
}

/****************************************
***             SPRITE DRAW           ***
****************************************/
void Sprite::draw() {
  ESAT::DrawSprite(handle_, transform_);
}

void Sprite::draw(const float posX, const float posY) {
  set_position(posX,posY);
  ESAT::DrawSprite(handle_, transform_);
}

cls
IF EXIST .vs rmdir /s /q .vs
IF EXIST obj rmdir /s /q obj
IF EXIST ..\bin\debug rmdir /s /q ..\bin\debug
IF EXIST ..\bin\release rmdir /s /q ..\bin\release
IF EXIST 3IA_AStar.vcxproj del /F 3IA_AStar.vcxproj 
IF EXIST 3IA_AStar.vcxproj.filters del /F 3IA_AStar.vcxproj.filters 
IF EXIST 3IA_AStar.vcxproj.user del /F 3IA_AStar.vcxproj.user 
IF EXIST 3IA_AStar.sln del /F 3IA_AStar.sln 
IF EXIST 3IA_AStar.VC.db del /F 3IA_AStar.VC.db 

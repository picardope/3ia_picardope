#include "ESAT/window.h"
#include "ESAT/input.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "EDK3/scoped_ptr.h"

bool g_end_program = false;
double g_time_step = 16.6;

void Input() {
  g_end_program = !ESAT::WindowIsOpened() || 
                  ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
}

void Update(double delta_time) {
  // Aplicariamos dicho delta time al step o update de la escena principal.
}

void Draw() {
  ESAT::DrawBegin();
  ESAT::DrawClear(59, 50, 50);

  ESAT::DrawEnd();
  ESAT::WindowFrame();
}


void Init() {
  ESAT::WindowInit(800, 600);
  ESAT::WindowSetMouseVisibility(true);
}

void Start() {

  double current_time = ESAT::Time(); 

  while (!g_end_program) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= g_time_step) {

      Update(g_time_step);

      current_time += g_time_step;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }


}

void Finish() {
  ESAT::WindowDestroy();
}

int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

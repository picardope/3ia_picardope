-- GENIe solution to build the project.
-- Julio Marcelo Picardo <picardope@esat-alumni.com>
-- Ivan Sancho <sanchomu@esat-alumni.com>

PROJ_DIR = path.getabsolute("./../")

solution "IA_Practica1_MainLoop"
  configurations {
    "Debug",
    "Release"
  }
  platforms { "x32", "x64" }

project "MainLoop"
  kind "ConsoleApp"
  language "C++"

  includedirs {
  	path.join(PROJ_DIR, "./include/"),
  	path.join(PROJ_DIR, "./src/"),
  	path.join(PROJ_DIR, "./extern/ESAT/include"),
  }

  files {
  	path.join(PROJ_DIR, "./include/*.h"),
  	path.join(PROJ_DIR, "./src/*.cc"),
  	path.join(PROJ_DIR, "./extern/ESAT/include/ESAT/*.h"),
  	path.join(PROJ_DIR, "./extern/ESAT/include/EDK3/dev/*.h"),
  	path.join(PROJ_DIR, "./extern/ESAT/include/EDK3/*.h"),
  }

  configuration "Debug"
    targetdir "../compiled/debug/"
    flags { "Symbols" }
    links {
      "opengl32",
      "../bin/ESAT_d",
    }
    defines {
      "_DEBUG"
    }

  configuration "Release"
    targetdir "../compiled/release/"
    flags { "OptimizeSize" }
    links {
      "opengl32",
      "../bin/ESAT",
    }
    defines {
      "NDEBUG"
    }

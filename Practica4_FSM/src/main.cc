#include "ESAT/window.h"
#include "ESAT/input.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "gamemanager.h"
#include "agent.h"
#include "soldier.h"
#include <string>
#include <time.h>


void DrawText(const char* text, Vec2 position, float size = 30, unsigned char r = 0, unsigned char g = 0, unsigned char b = 0) {
  ESAT::DrawSetStrokeColor(r, g, b, 255);
  ESAT::DrawSetFillColor(r, g, b, 255);
  ESAT::DrawSetTextSize(size);
  ESAT::DrawSetTextBlur(0.0f);
  ESAT::DrawText(position.x, position.y, text);
}


void Input() {
  Game::instance().end_program_ = !ESAT::WindowIsOpened() || 
                                   ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
}

void Update(double delta_time) {
  auto& game = Game::instance();
  for (unsigned int i = 0; i < game.soldier_.size(); ++i) {
    game.soldier_[i].update(delta_time);
  }
}

void Draw() {
  auto& game = Game::instance();
  ESAT::DrawBegin();
  ESAT::DrawClear(60, 60, 60);

  for (unsigned int i = 0; i < game.soldier_.size(); ++i) {
    game.soldier_[i].draw();
  }

  ESAT::DrawEnd();
  ESAT::WindowFrame();
}


void Init() {
  auto& game = Game::instance();

  game.window_width_ = 1200;
  game.window_height_ = 978;
  ESAT::WindowInit(game.window_width_, game.window_height_);
  ESAT::WindowSetMouseVisibility(true);
  ESAT::DrawSetTextFont("../data/fonts/DigitFont.TTF");
  ESAT::DrawSetTextBlur(true);

  game.big_sprite_handle_ = ESAT::SpriteFromFile("../data/images/marcianito01.png");
  game.medium_sprite_handle_ = ESAT::SpriteFromFile("../data/images/axis_medic.png");
  game.small_sprite_handle_ = ESAT::SpriteFromFile("../data/images/axis_soldier.png");
  game.soldier_[0].sprite_ = game.big_sprite_handle_;
  game.soldier_[1].sprite_ = game.medium_sprite_handle_;
  game.soldier_[2].sprite_ = game.small_sprite_handle_;
  game.soldier_[3].sprite_ = game.small_sprite_handle_;
  game.soldier_[4].sprite_ = game.medium_sprite_handle_;

}

void Start() {
  auto& game = Game::instance();
  srand(time(NULL));
  game.time_step_ = 16.6;

  double current_time = ESAT::Time(); 

  while (!game.end_program_) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= game.time_step_) {

      Update(game.time_step_);

      current_time += game.time_step_;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }

}

void Finish() {
  ESAT::WindowDestroy();
}

int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

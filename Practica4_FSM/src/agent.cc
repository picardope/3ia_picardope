/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica4_FSM
*  @brief IA agent class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "agent.h"


Agent::Agent() {
  id_ = 0;
  alive_ = false;
  speed_ = 0.0f;
  velocity_ = { 0.0f, 0.0f };
  position_ = { 0.0f, 0.0f };
}

Agent::~Agent() {}

Agent::Agent(const Agent& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
}
Agent& Agent::operator=(const Agent& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
  return *this;
}
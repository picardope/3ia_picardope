/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica4_FSM
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "soldier.h"
#include "ESAT/Time.h"
#include "ESAT/draw.h"




Soldier::Soldier() : Agent() {
  movement_ = kSoldierMovement_Determ;
  state_ = kSoldierState_Init;
  type_ = kSoldierType_Small;
  patron_ = kSoldierPatron_Right;
  
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = 0.0f;
  last_path_position_ = { 0.0f, 0.0f };
  last_path_index_ = 0;
  delta_ = 0.0;
  frame_chronometer_ = 0;
  speed_factor_ = 0.0f;
  sprite_ = nullptr;
  transform_ = {};
  collision_radius_ = 0.0f;
  movement_timer_ = 0.0f;
  chase_objetive_ = nullptr;
}

Soldier::Soldier(const Soldier& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
}
Soldier& Soldier::operator=(const Soldier& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
  return *this;
}

Soldier::~Soldier() {
  if (sprite_) {
    ESAT::SpriteRelease(sprite_);
  }
}

void Soldier::init() {
  switch (type_) {
    case kSoldierType_Big:
      speed_factor_ = 0.75f;
    break;
    case kSoldierType_Medium:
      speed_factor_ = 1.0f;
    break;
    case kSoldierType_Small:
      speed_factor_ = 1.25;
    break;
  }
  ESAT::SpriteTransformInit(&transform_);
  transform_.sprite_origin_x = ESAT::SpriteWidth(sprite_) * 0.5f;
  transform_.sprite_origin_y = ESAT::SpriteHeight(sprite_) * 0.5f;
  transform_.x = position_.x;
  transform_.y = position_.y;
  transform_.scale_x = 2.0f;
  transform_.scale_y = 2.0f;
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = 0.1f;
  velocity_ = { 1.0f, 0.0f };
  frame_chronometer_ = 400;
}

void Soldier::update(const double delta_time) {
  delta_ = delta_time;
  transform_.x = position_.x;
  transform_.y = position_.y;
  mindUpdate();
  bodyUpdate();
  checkScreenLimits();
}

void Soldier::draw() {
  ESAT::DrawSprite(sprite_, transform_);
  drawCollisionRadius();
}

void Soldier::mindUpdate() {
  switch (state_) {
    case kSoldierState_Init:
      init();
      state_ = kSoldierState_Working;
      switch (type_) {
        case kSoldierType_Big:
        velocity_ = { 1.0f, 0.0f };
        movement_ = kSoldierMovement_Determ;
        break;
        case kSoldierType_Medium:
        movement_ = kSoldierMovement_Random;
        break;
        case kSoldierType_Small:
        movement_ = kSoldierMovement_Patron;
        break;
      }
    break;
    case kSoldierState_Working:
      workingUpdate();
    break;
    case kSoldierState_Chasing:
      chasingUpdate();
    break;
    case kSoldierState_Fleeing:
      fleeingUpdate();
    break;
    case kSoldierState_Resting:
      restingUpdate();
    break;
  }
  
}


void Soldier::fleeingUpdate() {

  auto& game = Game::instance();

  // Compruebo el estado cada cierto tiempo para evitar bucles de huida descanso
  // cuando me persiguen.
  if (ESAT::Time() - fleeing_sensor_timer_ > kFleeingSensorTime) {
    fleeing_sensor_timer_ = ESAT::Time();

    // Compruebo siempre si tengo enemigos muy cerca, huyendo de ellos en primer lugar.
    if (type_ == kSoldierType_Small) {
      for (unsigned int i = 0; i < game.soldier_.size(); i++) {
        if (game.soldier_[i].type_ != kSoldierType_Small &&
            getDistanceToPoint(game.soldier_[i].position_) < kD2) {
          velocity_ = Vec2Normalize({ position_.x - game.soldier_[i].position_.x, position_.y - game.soldier_[i].position_.y });
          return;
        }
      }
    }
    else if (type_ == kSoldierType_Medium) {
      for (unsigned int i = 0; i < game.soldier_.size(); i++) {
        if (game.soldier_[i].type_ == kSoldierType_Big &&
            getDistanceToPoint(game.soldier_[i].position_) < kD2) {
          velocity_ = Vec2Normalize({ position_.x - game.soldier_[i].position_.x, position_.y - game.soldier_[i].position_.y });
          return;
        }
      }
    }

    // Compruebo si tengo que seguir huyendo.
    if (type_ == kSoldierType_Small) {
      for (unsigned int i = 0; i < game.soldier_.size(); i++) {
        if (game.soldier_[i].type_ != kSoldierType_Small && 
            getDistanceToPoint(game.soldier_[i].position_) < kD3) {
          return;
        }
      }
    }
    else if (type_ == kSoldierType_Medium) {
      for (unsigned int i = 0; i < game.soldier_.size(); i++) {
        if (game.soldier_[i].type_ == kSoldierType_Big &&
            getDistanceToPoint(game.soldier_[i].position_) < kD3) {
          return;
        }
      }
    }

    // En caso de de no tener que huir de nadie, descanso.
    state_ = kSoldierState_Resting;
    resting_timer_ = ESAT::Time();
    movement_ = kSoldierMovement_None;
  }
}

void Soldier::chasingUpdate() {

  auto& game = Game::instance();

  // Compruebo si tengo que huir de alguien.
  if (type_ == kSoldierType_Medium) {
    for (unsigned int i = 0; i < game.soldier_.size(); i++) {
      if (game.soldier_[i].type_ == kSoldierType_Big &&
          getDistanceToPoint(game.soldier_[i].position_) < kD2) {
        velocity_ = Vec2Normalize({ position_.x - game.soldier_[i].position_.x, position_.y - game.soldier_[i].position_.y });
        state_ = kSoldierState_Fleeing;
        movement_ = kSoldierMovement_Determ;
      }
    }
    // Si no tengo que huir de nadie, compruebo la distancia a mi presa.
    if (getDistanceToPoint(chase_objetive_->position_) < kD3) {
      lookAt(chase_objetive_->position_);
      return;
    }
  }
  else if (type_ == kSoldierType_Medium) {
    if (getDistanceToPoint(chase_objetive_->position_) < kD3) {
      lookAt(chase_objetive_->position_);
      return;
    }
  }

  // En caso de de no tener que huir de nadie ni perseguir, descanso.
  state_ = kSoldierState_Resting;
  resting_timer_ = ESAT::Time();
  movement_ = kSoldierMovement_None;

}

void Soldier::restingUpdate() {

  auto& game = Game::instance();

  if (ESAT::Time() - resting_timer_ > kRestingTime) {
    state_ = kSoldierState_Working;
    switch (type_) {
      case kSoldierType_Big:
        velocity_ = { 1.0f, 0.0f };
        movement_ = kSoldierMovement_Determ;
      break;
      case kSoldierType_Medium:
        movement_ = kSoldierMovement_Random;
      break;
      case kSoldierType_Small:
        movement_ = kSoldierMovement_Patron;
      break;
    }
  }

  // Compruebo si tengo que huir de alguien, en cuyo caso termina la funcion.
  if (type_ == kSoldierType_Small) {
    for (unsigned int i = 0; i < game.soldier_.size(); i++) {
      if (game.soldier_[i].type_ != kSoldierType_Small &&
          getDistanceToPoint(game.soldier_[i].position_) < kD2) {
        velocity_ = Vec2Normalize({ position_.x - game.soldier_[i].position_.x, position_.y - game.soldier_[i].position_.y });
        state_ = kSoldierState_Fleeing;
        movement_ = kSoldierMovement_Determ;
      }
    }
  }
  else if (type_ == kSoldierType_Medium) {
    for (unsigned int i = 0; i < game.soldier_.size(); i++) {
      if (game.soldier_[i].type_ == kSoldierType_Big &&
          getDistanceToPoint(game.soldier_[i].position_) < kD2) {
        velocity_ = Vec2Normalize({ position_.x - game.soldier_[i].position_.x, position_.y - game.soldier_[i].position_.y });
        state_ = kSoldierState_Fleeing;
        movement_ = kSoldierMovement_Determ;
        return;
      }
    }
  }

}

void Soldier::workingUpdate() {

  auto& game = Game::instance();

  // Compruebo si tengo que huir de alguien, en cuyo caso termina la funcion.
  if (type_ == kSoldierType_Small) {
    for (unsigned int i = 0; i < game.soldier_.size(); i++) {
      if (game.soldier_[i].type_ != kSoldierType_Small &&
          getDistanceToPoint(game.soldier_[i].position_) < kD2) {
        velocity_ = Vec2Normalize({ position_.x - game.soldier_[i].position_.x, position_.y - game.soldier_[i].position_.y });
        state_ = kSoldierState_Fleeing;
        movement_ = kSoldierMovement_Determ;
      }
    }
  }
  else if (type_ == kSoldierType_Medium) {
    for (unsigned int i = 0; i < game.soldier_.size(); i++) {
      if (game.soldier_[i].type_ == kSoldierType_Big &&
          getDistanceToPoint(game.soldier_[i].position_) < kD2) {
        velocity_ = Vec2Normalize({ position_.x - game.soldier_[i].position_.x, position_.y - game.soldier_[i].position_.y });
        state_ = kSoldierState_Fleeing;
        movement_ = kSoldierMovement_Determ;
        return;
      }
    }
  }

  // Compruebo si tengo alguna presa cerca para cazarla.
  if (type_ == kSoldierType_Big) {
    for (unsigned int i = 0; i < game.soldier_.size(); i++) {
      if (game.soldier_[i].type_ != kSoldierType_Big &&
          getDistanceToPoint(game.soldier_[i].position_) < kD1) {
        chase_objetive_ = &game.soldier_[i];
        state_ = kSoldierState_Chasing;
        movement_ = kSoldierMovement_Determ;
      }
    }
  }
  else if (type_ == kSoldierType_Medium) {
    for (unsigned int i = 0; i < game.soldier_.size(); i++) {
      if (game.soldier_[i].type_ == kSoldierType_Small &&
          getDistanceToPoint(game.soldier_[i].position_) < kD1) {
        chase_objetive_ = &game.soldier_[i];
        state_ = kSoldierState_Chasing;
        movement_ = kSoldierMovement_Determ;
        return;
      }
    }
  }

}


void Soldier::bodyUpdate() {

  switch (movement_) {
    case kSoldierMovement_Determ:
    moveDeterm();
    break;
    case kSoldierMovement_Random:
    moveRandom();
    break;
    case kSoldierMovement_Patron:
    movePatron();
    break;
  }

}


void Soldier::checkScreenLimits() {
  auto& game = Game::instance();
  if (position_.x < 0.0f) {
    position_.x = (float)game.window_width_;
  }
  if (position_.y < 0.0f) {
    position_.y = (float)game.window_height_;
  }
  if (position_.x > (float)game.window_width_) {
    position_.x = 0.0f;
  }
  if (position_.y > (float)game.window_height_) {
    position_.y = 0.0f;
  }
}

void Soldier::drawCollisionRadius() {
  float vertices[100];
  float portion = k2Pi / 50.0f;
  for (unsigned int i = 0; i < 50; ++i) { 
    vertices[i * 2] = position_.x + cos(portion * i) * kD1;   
    vertices[i * 2 + 1] = position_.y + sin(portion * i) * kD1;
  }
    
  ESAT::DrawSetStrokeColor(0, 0, 0, 255);   
  ESAT::DrawSetFillColor(255, 255, 0, 50);
  ESAT::DrawSolidPath(vertices, 50, true);

  for (unsigned int i = 0; i < 50; ++i) {
    vertices[i * 2] = position_.x + cos(portion * i) * kD2;
    vertices[i * 2 + 1] = position_.y + sin(portion * i) * kD2;
  }

  ESAT::DrawSetStrokeColor(0, 0, 0, 255);
  ESAT::DrawSetFillColor(255, 255, 0, 20);
  ESAT::DrawSolidPath(vertices, 50, true);

  for (unsigned int i = 0; i < 50; ++i) {
    vertices[i * 2] = position_.x + cos(portion * i) * kD3;
    vertices[i * 2 + 1] = position_.y + sin(portion * i) * kD3;
  }

  ESAT::DrawSetStrokeColor(0, 0, 0, 255);
  ESAT::DrawSetFillColor(255, 255, 0, 5);
  ESAT::DrawSolidPath(vertices, 50, true);
}

void Soldier::moveDeterm() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_ * speed_factor_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ * speed_factor_ };
}

void Soldier::lookAt(Vec2 destiny) {
  velocity_ = { destiny.x - position_.x,destiny.y - position_.y };
  velocity_ = Vec2Normalize(velocity_);
}


bool Soldier::isNextToDestiny(Vec2 destiny) {
  if (position_.x < destiny.x + 3.0f &&
      position_.x > destiny.x - 3.0f &&
      position_.y < destiny.y + 3.0f &&
      position_.y > destiny.y - 3.0f) {
    return true;
  }
  return false;
}

float Soldier::getDistanceToPoint(const Vec2 point) {
  Vec2 distance = { point.x - position_.x, point.y - position_.y };
  float result = Vec2Magnitude(distance);
  if (result < 0.0f) {
    result *= -1.0f;
  }
  return result;
}


void Soldier::moveRandom() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_ * speed_factor_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ * speed_factor_ };
  if (ESAT::Time() - movement_timer_ > kRandomMovementTimeToChange) {
    movement_timer_ = ESAT::Time();
    velocity_ = { (float)(rand() % 30) - 15.0f, (float)(rand() % 30) - 15.0f };
    velocity_ = Vec2Normalize(velocity_);
  }
}

void Soldier::movePatron() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_ * speed_factor_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ * speed_factor_ };
  if (ESAT::Time() - movement_timer_ > kPatronMovementTimeToChange) {
    movement_timer_ = ESAT::Time();
    switch (patron_) {
      case kSoldierPatron_Left:
        patron_ = kSoldierPatron_Up;
        velocity_ = { 0.0f, 1.0f };
      break;
      case kSoldierPatron_Up:
        patron_ = kSoldierPatron_Right;
        velocity_ = { 1.0f, 0.0f };
      break;
      case kSoldierPatron_Right:
        patron_ = kSoldierPatron_Down;
        velocity_ = { 0.0f, -1.0f };
      break;
      case kSoldierPatron_Down:
        patron_ = kSoldierPatron_Left;
        velocity_ = { -1.0f, 0.0f };
      break;
    }
  }
}


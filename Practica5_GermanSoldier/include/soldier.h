/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica5_GermanSoldier
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_SOLDIER_H__
#define __IA_SOLDIER_H__ 1

#include "agent.h"

enum SoldierState {
  kSoldierState_None,
  kSoldierState_Init,
  kSoldierState_Patrol,
  kSoldierState_Attacking,
  kSoldierState_Scanning,
  kSoldierState_Dead,
  kSoldierState_CheckingSound,
};

enum SoldierMovement {
  kSoldierMovement_None,
  kSoldierMovement_Determ,
  kSoldierMovement_Patron,
};


enum SoldierPatron {
  kSoldierPatron_Left,
  kSoldierPatron_Up,
  kSoldierPatron_Down,
  kSoldierPatron_Right,
  kSoldierPatron_Idle
};

const float kScanningSensorTime = 3000.0f;
const float kTimeToArriveAtSoundPoint = 3000.0f;
const float kPatronMovementTimeToChange = 2000.0f;
const Vec2 kInitStateSquarePosition = { 50.0f, 125.0f };
const Vec2 kPatrolStateSquarePosition = { 230.0f, 125.0f };
const Vec2 kDeadStateSquarePosition = { 605.0f, 438.0f };
const Vec2 kScanningStateSquarePosition = { 603.0f, 124.0f };
const Vec2 kCheckingSoundStateSquarePosition = { 386.0f, 200.0f };
const Vec2 kAttackingStateSquarePosition = { 386.0f, 47.0f };

class Soldier : public Agent {
public:


  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  Soldier();
  ~Soldier();
  Soldier(const Soldier& copy);
  Soldier& operator=(const Soldier& copy);

  /*******************************************************************************
  ***                            Soldier methods                               ***
  *******************************************************************************/

  virtual void bodyUpdate() override;
  virtual void mindUpdate() override;
  virtual void update(const double delta_time) override;

  /// @brief Init method of the class.
  void init();

  /// @brief Mind update applied to the attacking soldier state.
  void attackingUpdate();
  /// @brief Mind update applied to the dead soldier state.
  void deadUpdate();
  /// @brief Mind update applied to the searching soldier state.
  void checkingSoundUpdate();
  /// @brief Mind update applied to the scanning soldier state.
  void scanningUpdate();
  /// @brief Mind update applied to the patrol soldier state.
  void patrolUpdate();
  /// @brief Checks if the soldier is still alive or not..
  void checkHealth();

  /// @brief Body's Determinist move method.
  void moveDeterm();
  /// @brief Body's Patron move method.
  void movePatron();


  SoldierState state_;
  SoldierMovement movement_;
  SoldierPatron patron_;
  int health_;
  double delta_;
  double cheking_sound_arrive_to_point_timer_;
  double scanning_timer_;
  double movement_timer_;
  bool enemy_detected_;
  bool suspicious_sound_heared_;

};


#endif
/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica5_GermanSoldier
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/


#include "gamemanager.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "soldier.h"


float Vec2Magnitude(const Vec2& vector) {

  float magnitude;

  magnitude = pow(vector.x, 2) + pow(vector.y, 2);
  magnitude = sqrtf(magnitude);

  return magnitude;
}

Vec2 Vec2Normalize(const Vec2& vector) {

  Vec2 normalized;

  float inverse_magnitude = 1.0f / Vec2Magnitude(vector);

  normalized = { vector.x * inverse_magnitude, vector.y * inverse_magnitude };

  return normalized;
}



/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

/*******************************************************************************
***                       Constructor and destructor                         ***
*******************************************************************************/

Game& Game::instance() {
  static Game* singleton = new Game();
  return *singleton;
}

Game::Game() {
  end_program_ = false;
  time_step_ = 16.6;
  window_width_ = 1200;
  window_height_ = 978;
  background_ = 0;
  square_position_ = { 0.0f, 0.0f };
  soldier_.resize(1);
}



Game::~Game() {
  if (background_) {
    ESAT::SpriteRelease(background_);
  }
}





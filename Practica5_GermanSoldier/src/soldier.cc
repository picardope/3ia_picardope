/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica5_GermanSoldier
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "soldier.h"
#include "ESAT/Time.h"
#include "ESAT/draw.h"




Soldier::Soldier() : Agent() {
  movement_ = kSoldierMovement_Determ;
  state_ = kSoldierState_None;
  patron_ = kSoldierPatron_Right;
  
  health_ = 100;
  speed_ = 0.0f;
  delta_ = 0.0;
  scanning_timer_ = 0.0;
  cheking_sound_arrive_to_point_timer_ = 0.0;
  movement_timer_ = 0.0;
  enemy_detected_ = false;
  suspicious_sound_heared_ = false;
}

Soldier::Soldier(const Soldier& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
}
Soldier& Soldier::operator=(const Soldier& copy) {
  id_ = copy.id_;
  alive_ = copy.alive_;
  speed_ = copy.speed_;
  velocity_ = copy.velocity_;
  position_ = copy.position_;
  return *this;
}

Soldier::~Soldier() {}

void Soldier::init() {
  movement_ = kSoldierMovement_Determ;
  state_ = kSoldierState_None;
  patron_ = kSoldierPatron_Right;
  speed_ = 0.0f;
  delta_ = 0.0;
  scanning_timer_ = 0.0;
  cheking_sound_arrive_to_point_timer_ = 0.0;
  movement_timer_ = 0.0;
  health_ = 100;
  ESAT::Sleep(1000);
  enemy_detected_ = false;
  suspicious_sound_heared_ = false;
  state_ = kSoldierState_Patrol;
  Game::instance().square_position_ = kPatrolStateSquarePosition;
  checkHealth();
}

void Soldier::attackingUpdate() {
  // Continuara atacando hasta que deje de detectar al enemigo, bien por muerte,
  // suya, por muerte del enemigo, porque este escape, etc.
  if (!enemy_detected_) {
    Game::instance().square_position_ = kScanningStateSquarePosition;
    state_ = kSoldierState_Scanning;
    scanning_timer_ = ESAT::Time();
  }
  checkHealth();
}

void Soldier::deadUpdate() {
  // Permanecera muerto hasta que se vuelva a pulsar el boton de restart.
}

void Soldier::checkingSoundUpdate() {
  auto& game = Game::instance();
  // Ira hasta la posicion donde haya escuchado el sonido, una vez alcanzado, 
  // pasara a escanear. En caso de escuchar un nuevo sonido, se dirigira hasta este.
  // Se utilizara un timer para simbolizar la llegada al punto. En caso de
  //  detectar un enemigo durante el chequeo, se pasara automaticamente a atacar.
  if (suspicious_sound_heared_) {
    cheking_sound_arrive_to_point_timer_ = ESAT::Time();
  }
  if (ESAT::Time() - cheking_sound_arrive_to_point_timer_ >= kTimeToArriveAtSoundPoint) {
    state_ = kSoldierState_Scanning;
    game.square_position_ = kScanningStateSquarePosition;
    scanning_timer_ = ESAT::Time();
  }
  if (enemy_detected_) {
    state_ = kSoldierState_Attacking;
    game.square_position_ = kAttackingStateSquarePosition;
  }
  checkHealth();
}

void Soldier::scanningUpdate() {

  // En caso de escuchar un nuevo sonido pasaremos a chequear dicha posicion.
  // Comprobaremos que el tiempo de escaneo se ha cumplido, y pasaremos a patrullar,
  // En caso de detectar un enemigo pasaremos a atacarlo.
  auto& game = Game::instance();
  if (suspicious_sound_heared_) {
    state_ = kSoldierState_CheckingSound;
    game.square_position_ = kCheckingSoundStateSquarePosition;
    cheking_sound_arrive_to_point_timer_ = ESAT::Time();
    suspicious_sound_heared_ = false;
  }
  if (ESAT::Time() - scanning_timer_ >= kScanningSensorTime) {
    state_ = kSoldierState_Patrol;
    game.square_position_ = kPatrolStateSquarePosition;
  }
  if (enemy_detected_) {
    state_ = kSoldierState_Attacking;
    game.square_position_ = kAttackingStateSquarePosition;
  }
  checkHealth();
}

void Soldier::patrolUpdate() {
  // Realizara su patrulla y si oye algun sonido sospechoso o tiene un enemigo
  // a la vista, cambiara de estado, siendo este ultimo el prioritario.
  auto& game = Game::instance();

  if (suspicious_sound_heared_) {
    state_ = kSoldierState_CheckingSound;
    game.square_position_ = kCheckingSoundStateSquarePosition;
    cheking_sound_arrive_to_point_timer_ = ESAT::Time();
    suspicious_sound_heared_ = false;
  }
  if (enemy_detected_) {
    state_ = kSoldierState_Attacking;
    game.square_position_ = kAttackingStateSquarePosition;
  }
  checkHealth();
}

void Soldier::checkHealth() {
  if (health_ <= 0) {
    state_ = kSoldierState_Dead;
    Game::instance().square_position_ = kDeadStateSquarePosition;
  }
}

void Soldier::update(const double delta_time) {
  delta_ = delta_time;
  mindUpdate();
  bodyUpdate();
}


void Soldier::mindUpdate() {
  auto& game = Game::instance();
  switch (state_) {
    case kSoldierState_None:
      state_ = kSoldierState_Init;
      game.square_position_ = kInitStateSquarePosition;
    break;
    case kSoldierState_Init:
      init();
    break;
    case kSoldierState_Patrol:
      patrolUpdate();
    break;
    case kSoldierState_Attacking:
      attackingUpdate();
    break;
    case kSoldierState_Scanning:
      scanningUpdate();
    break;
    case kSoldierState_Dead:
      deadUpdate();
    break;
    case kSoldierState_CheckingSound:
      checkingSoundUpdate();
    break;
  }
    
}





void Soldier::bodyUpdate() {

  switch (movement_) {
    case kSoldierMovement_Determ:
    moveDeterm();
    break;
    case kSoldierMovement_Patron:
    movePatron();
    break;
  }

}


void Soldier::moveDeterm() {
  // Movimiento determinista, en este caso moverse hacia delante.
}



void Soldier::movePatron() {
  // Por poner un ejemplo del patron.
  moveDeterm();
  if (ESAT::Time() - movement_timer_ > kPatronMovementTimeToChange) {
    movement_timer_ = ESAT::Time();
    switch (patron_) {
      case kSoldierPatron_Left:
        patron_ = kSoldierPatron_Up;
        velocity_ = { 0.0f, 1.0f };
      break;
      case kSoldierPatron_Up:
        patron_ = kSoldierPatron_Right;
        velocity_ = { 1.0f, 0.0f };
      break;
      case kSoldierPatron_Right:
        patron_ = kSoldierPatron_Down;
        velocity_ = { 0.0f, -1.0f };
      break;
      case kSoldierPatron_Down:
        patron_ = kSoldierPatron_Idle;
        velocity_ = { 0.0f, 0.0f };
      break;
      case kSoldierPatron_Idle:
      patron_ = kSoldierPatron_Left;
      velocity_ = { -1.0f, 0.0f };
      break;
    }
  }
}


#include "ESAT/window.h"
#include "ESAT/input.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "ESAT_extra/imgui.h"
#include "gamemanager.h"
#include "agent.h"
#include "soldier.h"
#include <string>
#include <time.h>

/// @brief Remder function to draw a square over the current state.
void DrawSquare() {
  Vec2 size = { 96.0f, 77.0f };
  auto& game = Game::instance();
  float square[10] = {
    game.square_position_.x, game.square_position_.y,
    game.square_position_.x + size.x, game.square_position_.y,
    game.square_position_.x + size.x, game.square_position_.y + size.y,
    game.square_position_.x, game.square_position_.y + size.y,
    game.square_position_.x, game.square_position_.y
  };
  ESAT::DrawSetFillColor(255, 0, 0, 100);
  ESAT::DrawSolidPath(square, 5);
}

/// @brief Input function of the application.
void Input() {
  Game::instance().end_program_ = !ESAT::WindowIsOpened() || 
                                   ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
}

/// @brief Update function of the application.
void Update(double delta_time) {
  auto& game = Game::instance();
  game.soldier_[0].update(delta_time);
}

/// @brief Render function of the application.
void Draw() {
  auto& game = Game::instance();
  ESAT::DrawBegin();
  ESAT::DrawClear(60, 60, 60);

  ESAT::DrawSprite(game.background_, 0.0f, 0.0f);
  DrawSquare();


  ESAT::DrawEnd();

  ImGui::Begin("Soldier conditions to change between states");

  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Spacing();

  ImGui::SliderInt("Soldier's health", &game.soldier_[0].health_, 0, 100);
  ImGui::Checkbox("Enemy detected by the soldier", &game.soldier_[0].enemy_detected_);
  ImGui::Checkbox("Suspicious Sound Heared", &game.soldier_[0].suspicious_sound_heared_);

  ImGui::Spacing();
  ImGui::Separator();
  ImGui::Spacing();

  if (ImGui::Button("Restart game")) {
    game.square_position_ = kInitStateSquarePosition;
    game.soldier_[0].health_ = 100;
    game.soldier_[0].state_ = kSoldierState_None;
  }

  ImGui::End();

  ImGui::Render();

  ESAT::WindowFrame();
}

/// @brief Init function of the application.
void Init() {
  auto& game = Game::instance();

  game.window_width_ = 800;
  game.window_height_ = 600;
  ESAT::WindowInit(game.window_width_, game.window_height_);
  ESAT::WindowSetMouseVisibility(true);

  game.background_ = ESAT::SpriteFromFile("../data/images/background.jpg");

}

/// @brief Start function of the application.
void Start() {
  auto& game = Game::instance();
  srand(time(NULL));
  game.time_step_ = 16.6;

  double current_time = ESAT::Time(); 

  while (!game.end_program_) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= game.time_step_) {

      Update(game.time_step_);

      current_time += game.time_step_;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }

}

/// @brief Last function of the application.
void Finish() {
  ESAT::WindowDestroy();
}

int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

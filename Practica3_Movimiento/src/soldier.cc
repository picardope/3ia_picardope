/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica3_Movimiento
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "soldier.h"
#include "ESAT/Time.h"


Soldier::Soldier() : Agent() {
  objetive_ = kSoldierObjetive_MoveForward;
  health_ = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = 0.0f;
  last_path_position_ = { 0.0f, 0.0f };
  last_path_index_ = 0;
  size_ = { 0.0f, 0.0f };
  delta_ = 0.0;
  frame_chronometer_ = 0;
  is_the_special_one_ = false;
}

Soldier::~Soldier() {

}

void Soldier::init() {
  objetive_ = kSoldierObjetive_MoveForward;
  health_ = 0;
  color[0] = 200; color[1] = 200; color[2] = 0;
  destiny_ = { 0.0f, 0.0f };
  speed_ = 0.2f;
  velocity_ = { 1.0f, 0.0f };
  last_path_position_ = { Game::instance().path_node[0] };
  last_path_index_ = 0;
  size_ = { 15.0f, 15.0f };
  position_ = { Game::instance().window_width * 0.5f, Game::instance().window_height * 0.5f };
  is_the_special_one_ = false;
  frame_chronometer_ = 400;
}

void Soldier::update(const double delta_time) {
  delta_ = delta_time;
  mindUpdate();
  bodyUpdate();
}

void Soldier::mindUpdate() {
  frame_chronometer_ += 1;
  if (frame_chronometer_ >= 400) {
    if (objetive_ == kSoldierObjetive_FollowPath) {
      last_path_position_ = position_;
    }
    if (is_the_special_one_) {
      switchToNextObjetive();
    }
    if (objetive_ == kSoldierObjetive_FollowPath) {
      destiny_ = last_path_position_;
      lookAt(last_path_position_);
    }
    if (objetive_ == kSoldierObjetive_MoveRandomly) {
      destiny_ = { position_.x + (rand() % 300) - 150, position_.y + (rand() % 300) - 150 };
      if (destiny_.x < 0.0f) { destiny_.x += 150; }
      if (destiny_.y < 0.0f) { destiny_.y += 150; }
      if (destiny_.x >= Game::instance().window_width) { destiny_.x -= 150; }
      if (destiny_.x >= Game::instance().window_height) { destiny_.y -= 150; }
      lookAt(destiny_);
    }
    if (objetive_ == kSoldierObjetive_Patrol) {
      velocity_.x = 0; velocity_.y = 1;
    }
    if (objetive_ == kSoldierObjetive_MoveForward) {
      velocity_.x = 1; velocity_.y = 0;
    }
    frame_chronometer_ = 0;
  }

  switch (objetive_) {
  case kSoldierObjetive_MoveForward: {}break;
  case kSoldierObjetive_FollowPath: {
    if (isNextToDestiny(destiny_)) {
      last_path_index_ += 1;
      if (last_path_index_ >= 4) {
        last_path_index_ = 0;
      }
      destiny_ = Game::instance().path_node[last_path_index_];
      lookAt(destiny_);
    }
  }break;
  case kSoldierObjetive_Patrol: {
    float speed_direction = sin(ESAT::Time() * 0.002);
    velocity_.x = 0.0f;
    if ((speed_direction < 0.0f && velocity_.y > 0.0f) ||
        (speed_direction > 0.0f && velocity_.y < 0.0f)) {
      velocity_.y *= -1;
    }
  }break;
  case kSoldierObjetive_MoveRandomly: {
    if (isNextToDestiny(destiny_)) {
      destiny_ = { position_.x + (rand() % 300) - 150, position_.y + (rand() % 300) - 150 };
      if (destiny_.x < 0.0f) { destiny_.x += 150; }
      if (destiny_.y < 0.0f) { destiny_.y += 150; }
      if (destiny_.x >= Game::instance().window_width) { destiny_.x -= 150; }
      if (destiny_.x >= Game::instance().window_height) { destiny_.y -= 150; }
      lookAt(destiny_);
    }
  }break;
  }
  
}

void Soldier::bodyUpdate() {
  moveForward();
}


void Soldier::moveForward() {
  position_ = { position_.x + velocity_.x * ((float)delta_ * 0.001f) * speed_,
                position_.y + velocity_.y * ((float)delta_ * 0.001f) * speed_ };
}
void Soldier::lookAt(Vec2 destiny) {
  velocity_ = { destiny.x - position_.x,destiny.y - position_.y };
  velocity_ = Vec2Normalize(velocity_);
}


bool Soldier::isNextToDestiny(Vec2 destiny) {
  if (position_.x < destiny.x + 3.0f &&
      position_.x > destiny.x - 3.0f &&
      position_.y < destiny.y + 3.0f &&
      position_.y > destiny.y - 3.0f) {
    return true;
  }
  return false;
}

void Soldier::switchToNextObjetive() {
  
    switch (objetive_) {
    case kSoldierObjetive_MoveForward: {
      objetive_ = kSoldierObjetive_FollowPath;
      color[0] = 0; color[1] = 200; color[2] = 0;
    }break;
    case kSoldierObjetive_FollowPath: {
      objetive_ = kSoldierObjetive_MoveRandomly;
      color[0] = 200; color[1] = 0; color[2] = 0;

    }break;
    case kSoldierObjetive_MoveRandomly: {
      objetive_ = kSoldierObjetive_Patrol;
      color[0] = 0; color[1] = 0; color[2] = 200;
    }break;
    case kSoldierObjetive_Patrol: {
      objetive_ = kSoldierObjetive_MoveForward;
      color[0] = 200; color[1] = 200; color[2] = 0;
    }break;
    }
}
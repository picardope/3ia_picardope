/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica3_Movimiento
*  @brief IA agent class
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#include "agent.h"

Agent::Agent() {
  id_ = 0;
  alive_ = false;
  speed_ = 0.0f;
  velocity_ = { 0.0f, 0.0f };
  position_ = { 0.0f, 0.0f };
}

Agent::~Agent() {}
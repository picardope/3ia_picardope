#include "ESAT/window.h"
#include "ESAT/input.h"
#include "ESAT/draw.h"
#include "ESAT/time.h"
#include "gamemanager.h"
#include "agent.h"
#include "soldier.h"
#include <string>
#include <time.h>



/**
* @brief Render a square.
* @param position Vector where the center of the square will be rendered.
* @param r Red color component of the square.
* @param g Green color component of the square.
* @param b Blue color component of the square.
* @param size Side length of the square.
*/
void DrawSquare(Vec2 position, unsigned char r, unsigned char g, unsigned char b, float size = 20.0f) {
  float vertices[10] = { position.x - size, position.y - size,     
                         position.x - size, position.y + size,     
                         position.x + size, position.y + size,     
                         position.x + size, position.y - size,     
                         position.x - size, position.y - size };    
  ESAT::DrawSetStrokeColor(0, 0, 0, 255);   
  ESAT::DrawSetFillColor(r, g, b, 255);   
  ESAT::DrawSolidPath(vertices, 5, true);
}

/**
* @brief Render a rectangle.
* @param position Vector where the center of the rectangle will be rendered.
* @param r Red color component of the rectangle.
* @param g Green color component of the rectangle.
* @param b Blue color component of the rectangle.
* @param size Vector with the width and height of the rectangle.
*/
void DrawRect(Vec2 position, unsigned char r, unsigned char g, unsigned char b, Vec2 size = { 20.0f, 20.0f }) {
  float vertices[10] = { position.x - size.x, position.y - size.y,
                         position.x - size.x, position.y + size.y,
                         position.x + size.x, position.y + size.y,
                         position.x + size.x, position.y - size.y,
                         position.x - size.x, position.y - size.y };
  ESAT::DrawSetStrokeColor(255, 255, 255, 255);
  ESAT::DrawSetFillColor(r, g, b, 255);
  ESAT::DrawSolidPath(vertices, 5, true);
}

/**
* @brief Render a text.
* @param text Text to render.
* @param position Vector where the text will be rendered.
* @param size Size of the text.
* @param r Red color component of the text.
* @param g Green color component of the text.
* @param b Blue color component of the text.
*/
void DrawText(const char* text, Vec2 position, float size = 30, unsigned char r = 0, unsigned char g = 0, unsigned char b = 0) {
  ESAT::DrawSetStrokeColor(r, g, b, 255);
  ESAT::DrawSetFillColor(r, g, b, 255);
  ESAT::DrawSetTextSize(size);
  ESAT::DrawSetTextBlur(0.0f);
  ESAT::DrawText(position.x, position.y, text);
}

/// @brief Render of the scene.
void DrawScene() {
  auto& game = Game::instance();
  DrawRect({ game.window_width * 0.25f, game.window_height * 0.25f },
    255, 0, 0, { game.window_width * 0.25f, game.window_height * 0.25f });
  DrawText(" Random ", { game.window_width * 0.17f, game.window_height * 0.25f }, 50);
  DrawRect({ game.window_width * 0.75f, game.window_height * 0.25f },
    255, 255, 0, { game.window_width * 0.25f, game.window_height * 0.25f });
  DrawText(" Determinista ", { game.window_width * 0.65f, game.window_height * 0.25f }, 50);
  DrawRect({ game.window_width * 0.25f, game.window_height * 0.75f },
    0, 0, 255, { game.window_width * 0.25f, game.window_height * 0.25f });
  DrawText(" Patrulla ", { game.window_width * 0.15f, game.window_height * 0.75f }, 50);
  DrawRect({ game.window_width * 0.75f, game.window_height * 0.75f },
    0, 255, 0, { game.window_width * 0.25f, game.window_height * 0.25f });
  DrawText(" Camino ", { game.window_width * 0.70f, game.window_height * 0.75f }, 50);
  DrawText("Practica 3 - Movimiento", { 50.0f,50.0f }, 40);
}

/// @brief Render of the path points.
void DrawPathPoints() {
  for (int i = 0; i < 4; i++) {
    DrawSquare(Game::instance().path_node[i], 255, 255, 255, 10);
  } 
}

/**
* @brief Check the limits of the screen.
* @param soldier soldier to check its position.
*/
void CheckSoldierScreenLimits(Soldier* soldier) {
  auto& game = Game::instance();
  if (soldier->position_.x + soldier->size_.x < -1.0f) {
    soldier->position_.x = game.window_width + soldier->size_.x;
  }
  if (soldier->position_.y + soldier->size_.y < -1.0f) {
    soldier->position_.y = game.window_height + soldier->size_.y;
  }
  if (soldier->position_.x - soldier->size_.x - 1 > game.window_width) {
    soldier->position_.x = 0.0f - soldier->size_.x;
  }
  if (soldier->position_.y - soldier->size_.y - 1 > game.window_height) {
    soldier->position_.y = 0.0f - soldier->size_.y;
  }
}

/// @brief Renders a soldier.
void DrawSoldier(Soldier* soldier) {
  DrawSquare(soldier->position_, soldier->color[0], soldier->color[1], soldier->color[2], soldier->size_.x);
}

/// @brief Input function of the application.
void Input() {
  Game::instance().end_program = !ESAT::WindowIsOpened() || 
                                 ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape);
}

/// @brief Update function of the application.
void Update(double delta_time) {
  auto& game = Game::instance();
  game.soldier->update(delta_time);
  CheckSoldierScreenLimits(game.soldier);
  game.determ_soldier->update(delta_time);
  CheckSoldierScreenLimits(game.determ_soldier);
  game.random_soldier->update(delta_time);
  CheckSoldierScreenLimits(game.random_soldier);
  game.patrol_soldier->update(delta_time);
  CheckSoldierScreenLimits(game.patrol_soldier);
  game.rastreator_soldier->update(delta_time);
  CheckSoldierScreenLimits(game.rastreator_soldier);
}

/// @brief Render function of the application.
void Draw() {
  auto& game = Game::instance();
  ESAT::DrawBegin();
  ESAT::DrawClear(60, 60, 60);

  DrawScene();
  DrawPathPoints();
  DrawSoldier(game.soldier);
  DrawSoldier(game.determ_soldier);
  DrawSoldier(game.patrol_soldier);
  DrawSoldier(game.random_soldier);
  DrawSoldier(game.rastreator_soldier);

  ESAT::DrawEnd();
  ESAT::WindowFrame();
}

/// @brief Init function of the application.
void Init() {
  auto& game = Game::instance();

  game.window_width = 1200;
  game.window_height = 978;
  ESAT::WindowInit(game.window_width, game.window_height);
  ESAT::WindowSetMouseVisibility(true);
  ESAT::DrawSetTextFont("../data/fonts/DigitFont.TTF");
  ESAT::DrawSetTextBlur(true);

  game.path_node[0] = { game.window_width * 0.40f, game.window_height * 0.40f };
  game.path_node[1] = { game.window_width * 0.60f, game.window_height * 0.40f };
  game.path_node[2] = { game.window_width * 0.60f, game.window_height * 0.60f };
  game.path_node[3] = { game.window_width * 0.40f, game.window_height * 0.60f };

  game.soldier->init();
  game.soldier->is_the_special_one_ = true;
  game.soldier->size_ = { 30.0f, 30.0f };

  game.random_soldier->init();
  game.random_soldier->objetive_ = kSoldierObjetive_MoveRandomly;
  game.random_soldier->position_ = { game.window_width * 0.2f, game.window_height * 0.8f };
  game.random_soldier->color[0] = 200; game.random_soldier->color[1] = 0; game.random_soldier->color[2] = 0;
  game.determ_soldier->init();
  game.determ_soldier->objetive_ = kSoldierObjetive_MoveForward;
  game.determ_soldier->position_ = { game.window_width * 0.2f, game.window_height * 0.2f };
  game.determ_soldier->color[0] = 200; game.determ_soldier->color[1] = 200; game.determ_soldier->color[2] = 0;
  game.rastreator_soldier->init();
  game.rastreator_soldier->objetive_ = kSoldierObjetive_FollowPath;
  game.rastreator_soldier->position_ = { game.window_width * 0.8f, game.window_height * 0.2f };
  game.rastreator_soldier->color[0] = 0; game.rastreator_soldier->color[1] = 200; game.rastreator_soldier->color[2] = 0;
  game.patrol_soldier->init();
  game.patrol_soldier->objetive_ = kSoldierObjetive_Patrol;
  game.patrol_soldier->position_ = { game.window_width * 0.8f, game.window_height * 0.8f };
  game.patrol_soldier->color[0] = 0; game.patrol_soldier->color[1] = 0; game.patrol_soldier->color[2] = 200;
}

/// @brief Start function of the application.
void Start() {
  auto& game = Game::instance();
  srand(time(NULL));
  game.time_step = 16.6;

  double current_time = ESAT::Time(); 

  while (!game.end_program) {

    Input();
    
    double accumulated_time = ESAT::Time() - current_time;
    
    while (accumulated_time >= game.time_step) {

      Update(game.time_step);

      current_time += game.time_step;

      accumulated_time = ESAT::Time() - current_time;
    }

    Draw();
  }

}

/// @brief Finish function of the application.
void Finish() {
  ESAT::WindowDestroy();
}

/// @brief Main function of the application.
int ESAT::main(int argc, char** argv) {

  Init();

  Start();
	
  Finish();

  return 0;
}

/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica3_Movimiento
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __IA_SOLDIER_H__
#define __IA_SOLDIER_H__ 1

#include "agent.h"

enum SoldierObjetive {
  kSoldierObjetive_MoveForward,
  kSoldierObjetive_FollowPath,
  kSoldierObjetive_MoveRandomly,
  kSoldierObjetive_Patrol
};

class Soldier : public Agent {
public:

  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  Soldier();
  ~Soldier();

  /*******************************************************************************
  ***                            Soldier methods                               ***
  *******************************************************************************/

  virtual void bodyUpdate() override;
  virtual void mindUpdate() override;
  virtual void update(const double delta_time) override;
  /// @brief Init method of the class.
  void init();

  /// @brief Change objetive of the movement the agent.
  void switchToNextObjetive();

  /// @brief Moves the agent.
  void moveForward();

  /**
  * @brief Modifies the agent velocity vector.
  * @param destiny Vector director of the velocity.
  */
  void lookAt(Vec2 destiny);

  /**
  * @brief Checks the distance between the agent and the destiny point.
  * @param destiny Vector to calculate its distance.
  * @returns True if the destiny is very near.
  */
  bool isNextToDestiny(Vec2 destiny);


  SoldierObjetive objetive_;
  int health_;
  Vec2 destiny_;
  unsigned char color[3];
  Vec2 last_path_position_;
  unsigned int last_path_index_;
  Vec2 size_;
  double delta_;
  int frame_chronometer_;
  bool is_the_special_one_;



  /*******************************************************************************
  ***                         Private Copy Constructor                         ***
  *******************************************************************************/
private:
  Soldier(const Soldier& copy);
  Soldier& operator=(const Soldier& copy);

};


#endif
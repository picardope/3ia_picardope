/** Copyright Julio Picardo. 2016-17, all rights reserveds.
*
*  @project Practica3_Movimiento
*  @brief IA soldier class (child of agent)
*  @authors Julio Marcelo Picardo <picardope@esat-alumni.com>
*
*/

#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__ 1

class Soldier;

struct Vec2 {
  float x, y;
};

const unsigned short int kNumPathNodes = 4;

/**
* @brief Calculates the magnitude of a vector.
* @param vector Vector to calculate its Magnitude.
* @returns Magnitude of the vector.
*/
float Vec2Magnitude(const Vec2& vector);

/**
* @brief Normalizes a vector.
* @param vector Vector to normalize.
* @returns Vector normalized.
*/
Vec2 Vec2Normalize(const Vec2& vector);

/*******************************************************************************
********************************************************************************
***                                                                          ***
***                              GAME MANAGER                                ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

class Game {

public:

  /// @brief Singleton instance.
  static Game& instance();

  /*******************************************************************************
  ***                        Constructor and destructor                        ***
  *******************************************************************************/
  /// @brief Constructor of the class.
  Game();
  /// @brief Destructor of the class.
  ~Game();

  // INPUT
  bool end_program;


  double time_step;
  int window_width;
  int window_height;
  Vec2 path_node[kNumPathNodes];


  Soldier* soldier;
  Soldier* determ_soldier;
  Soldier* random_soldier;
  Soldier* rastreator_soldier;
  Soldier* patrol_soldier;


   


/*******************************************************************************
***                         Private Copy Constructor                         ***
*******************************************************************************/
 private:

  Game(const Game& copy);
  Game& operator=(const Game& copy);

};


#endif
